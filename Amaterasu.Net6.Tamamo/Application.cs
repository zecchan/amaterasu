﻿using Amaterasu.Database;
using Amaterasu.Tamamo;

namespace Amaterasu.Net6.Tamamo
{
    public abstract class Application : IApplication
    {
        public abstract IApplicationExtensionManager ExtensionManager { get; }

        public abstract IDatabaseManager DatabaseManager { get; }
    }
}