﻿using Amaterasu.Tamamo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tamamo
{
    public interface IApplicationExtension
    {
        string Id { get; }

        string Name { get; }

        Version Version { get; }

        /// <summary>
        /// Gets menu items exported by this extension
        /// </summary>
        /// <returns>A collection of menu items</returns>
        IEnumerable<ApplicationMenuItem> GetMenuItems();
    }
}
