﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Amaterasu.Tamamo
{
    public static class ComponentTagParser
    {
        public static Dictionary<string, string> Parse(string? tag)
        {
            var res = new Dictionary<string, string>();
            if (tag != null)
            {
                var spl = tag.Split(new char[] { ';' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                foreach (var s in spl)
                {
                    var spl2 = s.Split(new char[] { ':' }, 2, StringSplitOptions.TrimEntries);
                    if (spl2.Length == 2)
                    {
                        res[spl2[0]] = spl2[1];
                    }
                    else
                    {
                        res[spl2[0]] = "";
                    }
                }
            }
            return res;
        }

        public static Dictionary<string, string> Parse(FrameworkElement element)
        {
            return Parse(element.Tag?.ToString());
        }

        public static Dictionary<string, string> Parse(System.Windows.Forms.Control control)
        {
            return Parse(control.Tag?.ToString());
        }
    }
}
