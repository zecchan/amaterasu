﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tamamo.Models
{
    public class SupportedApplicationExtension
    {
        public string Id { get; set; } = string.Empty;

        public Version MinVersion { get; set; } = new Version(1, 0, 0);

        public Version? MaxVersion { get; set; }
    }
}
