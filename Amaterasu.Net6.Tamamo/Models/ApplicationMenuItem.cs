﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tamamo.Models
{
    public class ApplicationMenuItem
    {
        public string Id { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        public string? ParentId { get; set; }

        public string WindowId { get; set; } = string.Empty;

        public bool ShowModal { get; set; } = false;
    }
}
