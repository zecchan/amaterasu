﻿using Amaterasu.Tamamo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tamamo
{
    public interface IApplicationExtensionManager
    {
        /// <summary>
        /// Get a list of supported application extensions
        /// </summary>
        IReadOnlyList<SupportedApplicationExtension> SupportedApplicationExtensions { get; }

        /// <summary>
        /// Registers an extension
        /// </summary>
        void Register(IApplicationExtension extension);

        /// <summary>
        /// Registers extensions from a dll file
        /// </summary>
        void RegisterFromFile(string dllPath);

        /// <summary>
        /// Registers extensions from dlls inside a directory
        /// </summary>
        void RegisterFromDirectory(string path, bool includeSubfolders = false);

        /// <summary>
        /// Get an extension with a certain type
        /// </summary>
        /// <returns>Extension instance when found, otherwise null</returns>
        T? GetExtension<T>() where T : IApplicationExtension;
    }
}
