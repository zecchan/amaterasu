﻿using Amaterasu.Database;
using Amaterasu.Database.MongoDB;
using Amaterasu.Database.MySQL;
using Amaterasu.Database.SQLServer;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.Databases
{
    [TestClass]
    public class DatabaseManagerTests
    {
        [TestMethod]
        public void LoadFromConfig_ValidConfig_Success()
        {
            var cfg = CreateConfig();

            var dbm = new DatabaseManager();
            dbm.LoadFromConfig(cfg.GetSection("Databases"));
            Assert.AreNotEqual(dbm.Databases.Count, 0);
            Assert.IsTrue(dbm.Databases.ContainsKey("mongo"));
        }
        [TestMethod]
        public void LoadFromConfig_ValidConfigWithAlias_Success()
        {
            var cfg = CreateConfigAliased();

            var dbm = new DatabaseManager();
            dbm.LoadFromConfig(cfg.GetSection("Databases"));
            Assert.AreNotEqual(dbm.Databases.Count, 0);
            Assert.IsTrue(dbm.Databases.ContainsKey("mongo"));
        }

        private IConfiguration CreateConfigAliased(string? path = null)
        {
            var jsonConfig = @"{
    ""Databases"": {
      ""mongo"": {
        ""engine"": ""Mongo"",
        ""cstring"": ""mongodb://127.0.0.1:27017/"",
        ""db"": ""otomax""
      }
    }
}";
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonConfig));
            ms.Seek(0, SeekOrigin.Begin);
            return new ConfigurationBuilder().AddJsonStream(ms).Build();
        }

        private IConfiguration CreateConfig(string? path = null)
        {
            var jsonConfig = @"{
    ""Databases"": {
      ""mongo"": {
        ""Engine"": ""Mongo"",
        ""ConnectionString"": ""mongodb://127.0.0.1:27017/"",
        ""Name"": ""otomax""
      }
    }
}";
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonConfig));
            ms.Seek(0, SeekOrigin.Begin);
            return new ConfigurationBuilder().AddJsonStream(ms).Build();
        }

        [TestInitialize]
        public void Initialize()
        {
            var mongo = new MongoDatabaseContext();
            var sqlServ = new SQLServerDatabaseContext();
            var mySql = new MySQLDatabaseContext();
        }
    }
}
