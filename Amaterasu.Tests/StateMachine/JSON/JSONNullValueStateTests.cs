﻿using Amaterasu.Logic.StateMachine;
using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONNullValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidTokens_Success()
        {
            var obj = new JSONNullValueState();
            var value = "null";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.IsNull(res.Value);
        }

        [TestMethod]
        public async Task Accept_InvalidTokens_ThrowInvalidTokenException()
        {
            var obj = new JSONNullValueState();
            var value = "nulla";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokens2_ThrowInvalidTokenException()
        {
            var obj = new JSONNullValueState();
            var value = "nuul";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }
    }
}
