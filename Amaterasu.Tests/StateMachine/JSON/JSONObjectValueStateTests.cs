﻿using Amaterasu.Logic.StateMachine;
using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONObjectValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidTokens_Success()
        {
            var obj = new JSONObjectValueState();
            var value = "{\r\n  \"aho\": true,\r\n  \"baka\": 123,\r\n \"asu\": []\r\n}";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(3, (res.Value as IDictionary)?.Count);
        }

        [TestMethod]
        public async Task Accept_ValidTokensEmptyObject_Success()
        {
            var obj = new JSONObjectValueState();
            var value = "{}";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(0, (res.Value as IDictionary)?.Count);
        }

        [TestMethod]
        public async Task Accept_InvalidTokensUnclosedObject_ThrowException()
        {
            var obj = new JSONObjectValueState();
            var value = "{\"aho\": 123";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }
            Assert.ThrowsException<Exception>(() =>
            {
                obj.GetResult();
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokensInvalidValue_ThrowInvalidTokenException()
        {
            var obj = new JSONObjectValueState();
            var value = "{aho : 123}";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokensDoubleComma_ThrowInvalidTokenException()
        {
            var obj = new JSONObjectValueState();
            var value = "{\r\n  \"aho\": true,,\r\n  \"baka\": 123,\r\n \"asu\": []\r\n}";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }
    }
}
