﻿using Amaterasu.Logic.StateMachine;
using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONNumberValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidTokens_Success()
        {
            var obj = new JSONNumberValueState();
            var value = "-2981.3734E-19";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(res.Value, -2981.3734E-19);
        }

        [TestMethod]
        public async Task Accept_InvalidTokens_ThrowInvalidTokenException()
        {
            var obj = new JSONNumberValueState();
            var value = "34891..248";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokens2_ThrowInvalidTokenException()
        {
            var obj = new JSONNumberValueState();
            var value = "-2-24";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokens3_ThrowInvalidTokenException()
        {
            var obj = new JSONNumberValueState();
            var value = "-0.3512.23";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }
    }
}
