﻿using Amaterasu.Logic.StateMachine;
using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONArrayValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidTokens_Success()
        {
            var obj = new JSONArrayValueState();
            var value = "[\r\n  true,\r\n  \"baka\",\r\n  12.58381e-10,\r\n  null\r\n]";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(4, (res.Value as IList)?.Count);
        }

        [TestMethod]
        public async Task Accept_ValidTokensEmptyArray_Success()
        {
            var obj = new JSONArrayValueState();
            var value = "[]";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(0, (res.Value as IList)?.Count);
        }

        [TestMethod]
        public async Task Accept_InvalidTokensUnclosedArray_ThrowException()
        {
            var obj = new JSONArrayValueState();
            var value = "[true";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }
            Assert.ThrowsException<Exception>(() =>
            {
                obj.GetResult();
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokensInvalidValue_ThrowInvalidTokenException()
        {
            var obj = new JSONArrayValueState();
            var value = "[true,\"Baka]";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokensDoubleComma_ThrowInvalidTokenException()
        {
            var obj = new JSONArrayValueState();
            var value = "[true,,false]";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }
    }
}
