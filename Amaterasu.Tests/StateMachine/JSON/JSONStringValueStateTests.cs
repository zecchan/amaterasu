﻿using Amaterasu.Logic.StateMachine;
using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONStringValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidTokens_Success()
        {
            var obj = new JSONStringValueState();
            var value = "\"true\"";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(res.Value, "true");
        }

        [TestMethod]
        public async Task Accept_InvalidTokens_ThrowInvalidTokenException()
        {
            var obj = new JSONStringValueState();
            var value = "\"asdki38hjhjk";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            Assert.ThrowsException<Exception>(() =>
            {
                obj.GetResult();
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokens2_ThrowInvalidTokenException()
        {
            var obj = new JSONStringValueState();
            var value = "fl\"ase";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }
    }
}
