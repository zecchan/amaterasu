﻿using Amaterasu.Logic.StateMachine;
using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONBooleanValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidTokens_Success()
        {
            var obj = new JSONBooleanValueState();
            var value = "true";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.IsTrue(res.Value is bool b && b);
        }

        [TestMethod]
        public async Task Accept_InvalidTokens_ThrowInvalidTokenException()
        {
            var obj = new JSONBooleanValueState();
            var value = "trues";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }

        [TestMethod]
        public async Task Accept_InvalidTokens2_ThrowInvalidTokenException()
        {
            var obj = new JSONBooleanValueState();
            var value = "flase";

            Assert.ThrowsException<InvalidTokenException>(() =>
            {
                for (var i = 0; i < value.Length; i++)
                {
                    var token = value[i];
                    obj.Accept(token);
                }
            });
        }
    }
}