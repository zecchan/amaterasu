﻿using Amaterasu.Logic.StateMachines.JSON;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Tests.StateMachine.JSON
{
    [TestClass]
    public class JSONValueStateTests
    {
        [TestMethod]
        public async Task Accept_ValidStringTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "\"Baka\\r\\nLoli\\f\\tAsweda\"";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(res.Value, "Baka\r\nLoli\f\tAsweda");
        }

        [TestMethod]
        public async Task Accept_ValidNumberTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "-582736.4726E+3";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(res.Value, -582736.4726E+3);
        }

        [TestMethod]
        public async Task Accept_ValidObjectTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "{\"lat\": -582736.4726E+3, \"lng\": -3424, \"name\": \"Babushka\", \"married\": true, \"date\": null, \"children\": [123,true,null,\"empty\"]}";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(6, (res.Value as IDictionary)?.Count);
        }

        [TestMethod]
        public async Task Accept_ValidArrayTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "[-582736.4726E+3,true,\"Baka\"]";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.AreEqual(3, (res.Value as IList)?.Count);
        }

        [TestMethod]
        public async Task Accept_ValidBooleanTrueTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "true";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.IsTrue(res.Value is bool b && b);
        }

        [TestMethod]
        public async Task Accept_ValidBooleanFalseTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "false";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.IsTrue(res.Value is bool b && !b);
        }

        [TestMethod]
        public async Task Accept_ValidNullTokens_Success()
        {
            var obj = new JSONValueState();
            var value = "null";

            for (var i = 0; i < value.Length; i++)
            {
                var token = value[i];
                obj.Accept(token);
            }

            var res = obj.GetResult();
            Assert.IsNotNull(res);
            Assert.IsNull(res.Value);
        }
    }
}
