# Amaterasu Data Package
## Document version
v0.3 - 2021-11-10  
Initial draft version

## What is ADP?
ADP is yet another archive format that designed to be secure yet fast archive file.

## ADP structure
### Header
- Magic Number  
The first 4 bytes of ADP archives is the string 'ADP1' encoded in ASCII.
- Archive Flag  
The next 2 bytes are the archive flags.
- Block Size  
The next 1 byte is the block size in KiB. Default is 4 KiB.
- Table Offset  
The next 4 or 8 bytes (depends on BigArchive archive flag) is the address of the beginning of table data.
- Recovery Offset  
The next 4 or 8 bytes (depends on BigArchive archive flag) is the address of the beginning of recovery data. This might not be present (depends on Recovery archive flag)
- Metadata Offset
The next 4 or 8 bytes (depends on BigArchive archive flag) is the address of the beginning of metadata section. This might not be present (depends on Metadata archive flag)
### Payload
After the header, the main payload is written. The size depends on what file are contained.
### Table
After the payload, the table is written. It contains all information about the payload and other data.
### Recovery
After the table, the recovery data is written. It is not always present.
### Metadata
After the table and recovery, the metadata is written. It is not always present.

## Archive flags
- BigArchive (1)  
Defines that all address is written in long instead of int.
- Recovery (2)  
Defines that this archive has a recovery section.
- Metadata (4)  
Defines that this archive has a metadata section.
- Compressed (8)  
Defines that this archive is compressed.
- Encrypted (16)  
Defines that the payload is encrypted.
- Encrypted Table (32)  
Defines that the table is encrypted.
- Encrypted Recovery (32)  
Defines that the recovery section is encrypted.
- Encrypted Metadata (32)  
Defines that the metadata section is encrypted.