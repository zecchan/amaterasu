﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Amaterasu.IO
{
    public class SubStream : Stream
    {
        /// <summary>
        /// Creates a substream from a stream.
        /// </summary>
        public SubStream(Stream underlyingStream, long offset, long length)
        {
            UnderlyingStream = underlyingStream;
            _length = length;
            UnderlyingStreamOffset = offset;
        }

        /// <summary>
        ///  Creates a substream from a stream from its current position.
        /// </summary>
        public SubStream(Stream underlyingStream, long length)
        {
            UnderlyingStream = underlyingStream;
            _length = length;
            UnderlyingStreamOffset = UnderlyingStream.Position;
        }

        public Stream UnderlyingStream { get; }

        public override bool CanRead => UnderlyingStream.CanRead;

        public override bool CanSeek => UnderlyingStream.CanSeek;

        public override bool CanWrite => UnderlyingStream.CanWrite;

        public long UnderlyingStreamOffset { get; }

        long _length = 0;
        public override long Length { get => _length; }

        public override long Position { get => UnderlyingStream.Position - UnderlyingStreamOffset; set => UnderlyingStream.Position = value + UnderlyingStreamOffset; }

        public override void Flush()
        {
            UnderlyingStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return UnderlyingStream.Read(buffer, offset, count);
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return UnderlyingStream.Seek(
                origin == SeekOrigin.Begin ? offset + UnderlyingStreamOffset
                : origin == SeekOrigin.Current ? offset + Position
                : origin == SeekOrigin.End ? offset + Length + offset
                : UnderlyingStreamOffset
                , SeekOrigin.Begin); 
        }

        public override void SetLength(long value)
        {
            if (value < 0)  
                throw new ArgumentOutOfRangeException(nameof(value));
            UnderlyingStream.SetLength(UnderlyingStream.Length + value - Length);
            _length = value;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            UnderlyingStream.Write(buffer, offset, count);
        }
    }
}
