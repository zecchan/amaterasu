﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.IO
{
    public static class PathHelper
    {
        /// <summary>
        /// Gets a relative path of a resource based on a base path
        /// </summary>
        /// <param name="basePath">The base path, must be a full path.</param>
        /// <param name="path">The resource path, must be a full path.</param>
        /// <returns>The relative path of the resource, null if no relative path</returns>
        public static string GetRelativePath(string basePath, string path, bool ignoreCase = true)
        {
            var bPaths = basePath.Split(new char[] { '/', '\\' }, StringSplitOptions.None).Select(x => x.Trim()).ToList();
            var pbPaths = ignoreCase ? bPaths.Select(x => x.ToLower()).ToList() : bPaths;
            var rPaths = path.Split(new char[] { '/', '\\' }, StringSplitOptions.None).Select(x => x.Trim()).ToList();
            var prPaths = ignoreCase ? rPaths.Select(x => x.ToLower()).ToList() : rPaths;

            int longestSamePath = -1;
            for(var i = 0; i < Math.Min(prPaths.Count, pbPaths.Count); i++)
            {
                if (prPaths[i] == pbPaths[i])
                {
                    longestSamePath = i + 1;
                }
                else
                {
                    break;
                }
            }
            string relPath = null;
            if (longestSamePath == bPaths.Count && rPaths.Count > longestSamePath)
            {
                var relPaths = new string[rPaths.Count - longestSamePath];
                Array.Copy(rPaths.ToArray(), longestSamePath, relPaths, 0, relPaths.Length);
                relPath = string.Join("/", relPaths);
            }
            return relPath;
        }
    }
}
