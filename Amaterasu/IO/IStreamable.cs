﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Amaterasu
{
    /// <summary>
    /// Interface to write data to stream.
    /// </summary>
    public interface IStreamWriteable
    {
        /// <summary>
        /// When implemented, write data to stream based on current instance data.
        /// </summary>
        /// <param name="stream">The stream to write to</param>
        void WriteTo(Stream stream);
    }

    /// <summary>
    /// Interface to read data from stream.
    /// </summary>
    public interface IStreamReadable
    {
        /// <summary>
        /// When implemented, read data from stream and update the current instance data.
        /// </summary>
        /// <param name="stream">The stream to read from</param>
        void ReadFrom(Stream stream);
    }

    /// <summary>
    /// Interface to read and write data to a stream.
    /// </summary>
    public interface IStreamable : IStreamWriteable, IStreamReadable
    {
    }

    public abstract class StreamableVersionedObject : IStreamable
    {
        /// <summary>
        /// Gets the magic key of this object
        /// </summary>
        protected abstract byte[] MagicKey { get; }

        protected abstract void Read(Stream stream);
        protected abstract void Write(Stream stream);

        public void ReadFrom(Stream stream)
        {
            if (!CheckMagicKey(stream, false))
                throw new InvalidDataException("Invalid data stream");
            Read(stream);
        }

        public void WriteTo(Stream stream)
        {
            if (MagicKey != null && MagicKey.Length > 0)
                stream.Write(MagicKey, 0, MagicKey.Length);
            Write(stream);
        }

        public bool CheckMagicKey(Stream stream, bool peekOnly = true)
        {
            if (MagicKey == null || MagicKey.Length == 0) return true;
            var lastPos = stream.Position;
            try
            {
                var buf = new byte[MagicKey.Length];
                stream.Read(buf, 0, MagicKey.Length);

                for (var i = 0; i < MagicKey.Length; i++)
                    if (buf[i] != MagicKey[i]) return false;

                return true;
            }
            finally
            {
                try
                {
                    if (peekOnly)
                        stream.Position = lastPos;
                }
                catch { }
            }
        }
    }
}
