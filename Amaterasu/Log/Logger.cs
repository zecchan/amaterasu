﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Amaterasu.Log
{
    public class Logger : IDisposable
    {
        /// <summary>
        /// Indicates that the log can only be accessed by this logger exclusively.
        /// </summary>
        public bool Exclusive { get; }

        /// <summary>
        /// Indicates that any exception when writing logs are ignored.
        /// </summary>
        public bool SafeMode { get; }

        /// <summary>
        /// Get the stream object used by the <see cref="Logger"/> to write log entries.
        /// </summary>
        public Stream LogFileStream { get; private set; }

        /// <summary>
        /// Gets the path to the log file.
        /// </summary>
        public string LogFilePath { get; }

        /// <summary>
        /// Gets or sets the time stamp format.
        /// </summary>
        public string TimeStampFormat { get; set; } = "yyyy-MM-dd HH:mm:ss";

        /// <summary>
        /// Gets or sets the log format.
        /// </summary>
        public string LogFormat { get; set; } = "[{timestamp} - {level}] {message}";

        /// <summary>
        /// Creates a new instance of the <see cref="Logger"/> class
        /// </summary>
        /// <param name="logFilePath">The path to log file</param>
        /// <param name="safeMode">When set to true, ignores all exceptions.</param>
        /// <param name="exclusive">When set to true, this logger will exclusively access the log file</param>
        public Logger(string logFilePath, bool safeMode = true, bool exclusive = false)
        {
            var dir = Path.GetDirectoryName(logFilePath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            SafeMode = safeMode;
            Exclusive = exclusive;
            if (Exclusive)
            {
                LogFileStream = new FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                LogFileStream.Seek(0, SeekOrigin.End);
            }
            LogFilePath = logFilePath;
        }

        /// <summary>
        /// Writes a new log entry.
        /// </summary>
        /// <param name="level">The log level</param>
        /// <param name="message">The log message</param>
        public void WriteLog(LogLevel level, string message)
        {
            var ts = DateTime.Now.ToString(TimeStampFormat);
            var fmt = TimeStampFormat;
            fmt = fmt.Replace("{timestamp}", ts);
            fmt = fmt.Replace("{level}", level.ToString());
            fmt = fmt.Replace("{message}", message);
            WriteLine(fmt);
        }

        /// <summary>
        /// Writes a new log entry based on an exception.
        /// </summary>
        /// <param name="exception">The exception to process</param>
        /// <param name="writeStackTrace">When sets to true, also write the stack trace.</param>
        /// <param name="includeInnerExceptions">When sets to true, also write inner exceptions.</param>
        /// <param name="level">The log level, defaults to <see cref="LogLevel.Error"/></param>
        public void WriteLog(Exception exception, bool writeStackTrace = true, bool includeInnerExceptions = true, LogLevel level = LogLevel.Error)
        {
            var msg = exception.Message;
            if (writeStackTrace)
            {
                msg += Environment.NewLine + "Stack Trace:" + Environment.NewLine + exception.StackTrace;
            }
            if (includeInnerExceptions)
            {
                var count = 0;
                var innerEx = exception.InnerException;
                while(innerEx != null)
                {
                    msg += Environment.NewLine + "Inner Exception #" + count + ": " + innerEx.Message;
                    if (writeStackTrace)
                    {
                        msg += Environment.NewLine + "Stack Trace:" + Environment.NewLine + innerEx.StackTrace;
                    }
                    count++; 
                    innerEx = innerEx.InnerException;
                }
            }
            WriteLog(level, msg);
        }

        private void WriteLine(string line)
        {
            if (SafeMode)
            {
                try
                {
                    DoWriteLine(line);
                }
                catch { }
            }
            else
            {
                DoWriteLine(line);
            }
        }

        private void DoWriteLine(string line)
        {
            if (Exclusive)
            {
                var buffer = Encoding.UTF8.GetBytes(line + Environment.NewLine);
                LogFileStream.Write(buffer, 0, buffer.Length);
            }
            else
            {
                File.AppendAllText(LogFilePath, line + Environment.NewLine);
            }
        }

        public void Dispose()
        {
            if (Exclusive && LogFileStream != null)
            {
                LogFileStream.Flush();
                LogFileStream.Dispose();
            }
        }
    }

    public enum LogLevel
    {
        Verbose,
        Debug,

        Information,

        Warning,
        FatalWarning,

        Error,
        Fatal,
    }
}
