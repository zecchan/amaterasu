﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Amaterasu.OAuth
{
    public class Google
    {
        public string ClientID { get; set; }

        public string ClientSecret { get; set; }

        public string RedirectURI { get; private set; }

        public string ResponseType { get; set; } = "code";

        public string Scope { get; set; } = "email profile openid";

        public string SuccessMessage { get; set; }

        /// <summary>
        /// Timeout in seconds
        /// </summary>
        public int Timeout { get; set; } = 60;

        public GoogleOAuthGetTokenMethod GetTokenMethod { get; set; } = GoogleOAuthGetTokenMethod.Direct;

        public string GetTokenProxyUrl { get; set; }

        public void StartOAuth()
        {
            var rand = new Random();
            var port = rand.Next(49215, 65536);
            var retries = 0;
            while (retries < 100)
            {
                try
                {
                    var httpListener = new HttpListener();
                    httpListener.Prefixes.Add("http://localhost:" + port + "/");
                    RedirectURI = "http://localhost:" + port;
                    httpListener.Start();
                    httpListener.BeginGetContext(HandleLocalCallback, httpListener);
                    break;
                }
                catch
                {
                    retries++;
                }
            }

            var uri = "https://accounts.google.com/o/oauth2/v2/auth?";
            var param = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(Scope))
                param.Add("scope", Scope);
            if (!string.IsNullOrEmpty(ResponseType))
                param.Add("response_type", ResponseType);
            if (!string.IsNullOrEmpty(RedirectURI))
                param.Add("redirect_uri", RedirectURI);
            if (!string.IsNullOrEmpty(ClientID))
                param.Add("client_id", ClientID);

            uri += string.Join("&", param.Select(x => x.Key + "=" + HttpUtility.UrlEncode(x.Value)));

            var psi = new ProcessStartInfo()
            {
                UseShellExecute = true,
                FileName = uri,
            };
            Process.Start(psi);
        }

        private async void HandleLocalCallback(IAsyncResult result)
        {
            try
            {
                HttpListener listener = (HttpListener)result.AsyncState;
                // Call EndGetContext to complete the asynchronous operation.
                HttpListenerContext context = listener.EndGetContext(result);
                HttpListenerRequest request = context.Request;
                // Obtain a response object.
                HttpListenerResponse response = context.Response;

                Dictionary<string, string[]> queries = new Dictionary<string, string[]>();
                foreach (var qs in request.QueryString)
                {
                    if (qs is string q)
                    {
                        var val = request.QueryString.GetValues(q);
                        queries.Add(q, val);
                    }
                }

                string responseString = SuccessMessage ?? "Success";
                if (queries.ContainsKey("error") || !queries.ContainsKey("code"))
                {
                    responseString = "Error: " + String.Join(", ", queries["error"]);
                }

                if (queries.ContainsKey("code") && queries["code"]?.Length > 0)
                {
                    OAuthCodeReceived?.Invoke(this, queries["code"][0]);

                    try
                    {
                        if (GetTokenMethod == GoogleOAuthGetTokenMethod.Direct)
                        {
                            if (!await GetToken(queries["code"][0]))
                            {
                                throw new Exception("Failed to authorize");
                            }
                        }
                        else
                        {
                            if (!await GetTokenViaProxy(queries["code"][0]))
                            {
                                throw new Exception("Failed to authorize");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        responseString = "Error: " + ex.Message;
                    }
                }

                byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                // Get a response stream and write the response to it.
                response.ContentLength64 = buffer.Length;
                System.IO.Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                // You must close the output stream.
                output.Close();
            }
            catch { }
        }

        private async Task<bool> GetToken(string code)
        {
            HttpClient clnt = new HttpClient();
            var req = new HttpRequestMessage(HttpMethod.Post, "https://oauth2.googleapis.com/token");
            req.Content = new FormUrlEncodedContent(new Dictionary<string, string>
            {
                { "code", code },
                { "client_id", ClientID },
                { "client_secret", ClientSecret },
                { "redirect_uri", "http://localhost" },
                { "grant_type", "authorization_code" },
            });
            var res = await clnt.SendAsync(req);
            return false;
        }
        private async Task<bool> GetTokenViaProxy(string code)
        {
            try
            {
                HttpClient clnt = new HttpClient();
                var req = new HttpRequestMessage(HttpMethod.Post, GetTokenProxyUrl);
                req.Content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "code", code },
                    { "redirect_uri", RedirectURI },
                });
                var res = await clnt.SendAsync(req);
                var content = await res.Content.ReadAsStringAsync();
                if (res.IsSuccessStatusCode)
                {
                    OAuthCompleted?.Invoke(this, content);
                    return true;
                }
            }
            catch { }
            return false;
        }

        public event EventHandler<string> OAuthCodeReceived;
        public event EventHandler<string> OAuthCompleted;
    }

    public enum GoogleOAuthGetTokenMethod
    {
        Direct,
        Proxy
    }
}
