﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public abstract class RepeatingCapturingState<TToken, TResult> : RepeatingState<TToken>, ICapturingState<TToken, TResult>
    {
        public abstract TResult? GetResult();
    }
}
