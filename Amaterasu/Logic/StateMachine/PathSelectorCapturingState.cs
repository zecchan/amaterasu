﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public class PathSelectorCapturingState<TToken, TResult> : ICapturingState<TToken, TResult>
    {
        public bool CanExit { get => CurrentState?.CanExit ?? false; }

        public virtual string Name => "Capturing Path Selector";

        public List<ICapturingState<TToken, TResult>> States { get; } = new List<ICapturingState<TToken, TResult>>();

        public ICapturingState<TToken, TResult>? CurrentState { get; protected set; }

        public void Accept(TToken token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token?.ToString());
            if (CurrentState == null)
            {
                CurrentState = States.First(x => x.CanAccept(token));
            }
            CurrentState.Accept(token);
        }

        public bool CanAccept(TToken token)
        {
            if (CurrentState != null) return CurrentState.CanAccept(token);
            return States.Any(x => x.CanAccept(token));
        }

        public TResult? GetResult()
        {
            if (CurrentState != null)
            {
                if (!CurrentState.CanExit) throw new Exception();
                return CurrentState.GetResult();
            }
            return default;
        }

        public void Reset()
        {
            CurrentState = null;
            States.ForEach(x => x.Reset());
        }
    }
}
