﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public class PatternState<TToken> : IState<TToken>
    {
        public virtual string Name => "Pattern";

        public bool CanExit { get; protected set; }

        public List<TToken[]> Patterns { get; } = new List<TToken[]>();

        protected List<TToken> Buffer { get; } = new List<TToken>();

        public void Accept(TToken token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token?.ToString());
            Buffer.Add(token);
            CanExit = IsBufferMatchPattern(Buffer) == true;
        }

        public bool CanAccept(TToken token)
        {
            var bufClone = Buffer.ToList();
            bufClone.Add(token);
            return IsBufferMatchPattern(bufClone) != false;
        }

        protected bool? IsBufferMatchPattern(IEnumerable<TToken> buffer)
        {
            if (buffer.Count() == 0) return false;
            var firstToken = buffer.First();
            var matchedPatterns = Patterns.Where(x => x.Length > 0 && (x[0]?.Equals(firstToken) ?? false)).ToList();
            if (matchedPatterns.Count == 0) return false;
            for(var i = 1; i < buffer.Count(); i++)
            {
                matchedPatterns = matchedPatterns.Where(x => x.Length > i && (x[i]?.Equals(buffer.ElementAt(i)) ?? false)).ToList();
                if (matchedPatterns.Count == 0) return false;
            }
            if (matchedPatterns.Count != 1) return null;
            var matched = matchedPatterns.First();
            if (matched.Length != buffer.Count()) return null;
            return true;
        }

        public void Reset()
        {
            CanExit = false;
            Buffer.Clear();
        }
    }
}
