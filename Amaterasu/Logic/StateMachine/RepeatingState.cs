﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public abstract class RepeatingState<TToken> : IState<TToken>
    {
        public virtual string Name => "Repeating";

        public virtual bool CanExit { get; protected set; }

        protected List<TToken> Buffer { get; } = new List<TToken>();

        public virtual void Accept(TToken token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token?.ToString());
            Buffer.Add(token);
            CanExit = CheckBufferValid(Buffer.ToList());
        }

        public abstract bool CanAccept(TToken token);

        protected virtual bool CheckBufferValid(List<TToken> buffer)
        {
            return buffer.Count > 0;
        }

        public virtual void Reset()
        {
            Buffer.Clear();
            CanExit = false;
        }
    }
}
