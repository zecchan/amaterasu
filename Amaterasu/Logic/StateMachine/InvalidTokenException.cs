﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public class InvalidTokenException : Exception
    {
        public InvalidTokenException(string stateName, string? tokenValue): base($"Invalid token value {(tokenValue != null ? "'" + tokenValue + "'" : "")} in {stateName}")
        { 

        }

        public InvalidTokenException(string message) : base(message) { }
    }
}
