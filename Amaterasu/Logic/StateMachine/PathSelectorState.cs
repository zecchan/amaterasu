﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public class PathSelectorState<TToken> : IState<TToken>
    {
        public bool CanExit { get => CurrentState?.CanExit ?? false; }

        public virtual string Name => "Path Selector";

        public List<IState<TToken>> States { get; } = new List<IState<TToken>>();

        public IState<TToken>? CurrentState { get; protected set; }

        public void Accept(TToken token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token?.ToString());
            if (CurrentState == null)
            {
                CurrentState = States.First(x => x.CanAccept(token));
            }
            CurrentState.Accept(token);
        }

        public bool CanAccept(TToken token)
        {
            if (CurrentState != null) return CurrentState.CanAccept(token);
            return States.Any(x => x.CanAccept(token));
        }

        public void Reset()
        {
            CurrentState = null;
            States.ForEach(x => x.Reset());
        }
    }
}
