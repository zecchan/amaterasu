﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public interface IState<TToken>
    {
        /// <summary>
        /// Gets the name of this state
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Checks whether <see cref="IState{TToken}"/> can accept a token
        /// </summary>
        /// <param name="token">Token to check</param>
        /// <returns>True if valid, false otherwise.</returns>
        bool CanAccept(TToken token);

        /// <summary>
        /// Give token to <see cref="IState{TToken}"/>
        /// </summary>
        /// <param name="token">Token to give</param>
        void Accept(TToken token);

        /// <summary>
        /// Gets whether state can be exited
        /// </summary>
        bool CanExit { get; }

        /// <summary>
        /// Reset <see cref="IState{TToken}"/> parameters
        /// </summary>
        void Reset();
    }
}
