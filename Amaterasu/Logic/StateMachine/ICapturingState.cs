﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public interface ICapturingState<TToken, TResult>: IState<TToken>
    {
        /// <summary>
        /// Gets the result of this state
        /// </summary>
        /// <returns></returns>
        TResult? GetResult();
    }
}
