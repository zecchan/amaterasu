﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachine
{
    public abstract class PatternCapturingState<TToken, TResult> : PatternState<TToken>, ICapturingState<TToken, TResult>
    {
        public abstract TResult? GetResult();
    }
}
