﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONNullValueState : PatternCapturingState<char, JSONToken>
    {
        public override string Name => "null";

        public JSONNullValueState()
        {
            Patterns.Add("null".ToCharArray());
        }

        public override JSONToken? GetResult()
        {
            if (!CanExit) throw new Exception();
            return new JSONToken();
        }
    }
}
