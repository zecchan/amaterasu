﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONObjectValueState : ICapturingState<char, JSONToken>
    {
        const string Whitespaces = " \t\r\n";
        bool entered = false;
        bool wantsString = false;
        bool afterValue = false;
        bool wantsValue = false;
        bool wantsColon = false;
        bool inString = false;
        private bool inValue;

        public string Name => "object";

        public bool CanExit { get; protected set; }

        protected JSONValueState ValueState { get; set; }

        protected JSONStringValueState StringValueState { get; } = new JSONStringValueState();

        protected Dictionary<string, JSONToken> ResultBuffer { get; } = new Dictionary<string, JSONToken>();

        protected string? PropName { get; set; }

        public void Accept(char token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token.ToString());
            if (!entered)
            {
                wantsString = true;
                StringValueState.Reset();
                entered = true;
                return;
            }
            if (wantsString)
            {
                if (StringValueState.CanAccept(token))
                {
                    StringValueState.Accept(token);
                    inString = true;
                    wantsString = false;
                }
                else if (token == '}')
                {
                    CanExit = true;
                }
                return;
            }
            if (inString)
            {
                if (StringValueState.CanAccept(token))
                {
                    StringValueState.Accept(token);
                    return;
                }
                else if (!StringValueState.CanExit)
                {
                    throw new InvalidTokenException(StringValueState.Name, token.ToString());
                }

                var pname = StringValueState.GetResult()?.Value?.ToString();
                if (string.IsNullOrEmpty(pname))
                    throw new InvalidTokenException("Property name cannot be empty or null");
                PropName = pname;
                inString = false;
                wantsColon = true;
                StringValueState.Reset();
                if (token == ':')
                {
                    wantsColon = false;
                    wantsValue = true;
                    ValueState = new JSONValueState();
                }
                return;
            }
            if (wantsColon)
            {
                if (token == ':')
                {
                    wantsColon = false;
                    wantsValue = true;
                    ValueState = new JSONValueState();
                }
            }
            if (wantsValue)
            {
                if (ValueState.CanAccept(token))
                {
                    ValueState.Accept(token);
                    inValue = true;
                    wantsValue = false;
                }
                return;
            }
            if (inValue)
            {
                if (ValueState.CanAccept(token))
                {
                    ValueState.Accept(token);
                    return;
                }
                else if (!ValueState.CanExit)
                {
                    throw new InvalidTokenException(ValueState.CurrentState?.Name ?? Name, token.ToString());
                }

                var pval = ValueState.GetResult();
                if (pval == null || PropName == null)
                    throw new InvalidTokenException("Value is invalid");
                ResultBuffer[PropName] = pval;
                inValue = false;
                afterValue = true;
                ValueState.Reset();
                if (token == '}')
                {
                    CanExit = true;
                }
                if (token == ',')
                {
                    afterValue = false;
                    wantsString = true;
                }
                return;
            }
            if (afterValue)
            {
                if (token == '}')
                {
                    CanExit = true;
                }
                if (token == ',')
                {
                    afterValue = false;
                    wantsString = true;
                }
                return;
            }
        }

        public bool CanAccept(char token)
        {
            if (CanExit) return false;
            if (!entered)
            {
                return token == '{';
            }
            if (wantsString)
            {
                return StringValueState.CanAccept(token)
                    || Whitespaces.Contains(token) || token == '}';
            }
            if (inString)
            {
                return StringValueState.CanAccept(token)
                    || (StringValueState.CanExit && (token == ':' || Whitespaces.Contains(token)));
            }
            if (wantsColon)
            {
                return token == ':' || Whitespaces.Contains(token);
            }
            if (wantsValue)
            {
                return ValueState.CanAccept(token)
                    || Whitespaces.Contains(token);
            }
            if (inValue)
            {
                return ValueState.CanAccept(token)
                    || (ValueState.CanExit && (token == ',' || token == '}' || Whitespaces.Contains(token)));
            }
            if (afterValue)
            {
                return token == ',' || token == '}' || Whitespaces.Contains(token);
            }
            return false;
        }

        public JSONToken? GetResult()
        {
            if (!CanExit) throw new Exception();
            return new JSONToken(ResultBuffer);
        }

        public void Reset()
        {
            entered = false;
            wantsString = false;
            wantsValue = false;
            wantsColon = false;
            afterValue = false;
            inString = false;
            inValue = false;
            CanExit = false;
            PropName = null;
            ValueState = null;
            ResultBuffer.Clear();
            StringValueState.Reset();
        }
    }
}
