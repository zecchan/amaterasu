﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONValueState : PathSelectorCapturingState<char, JSONToken>
    {
        public JSONValueState()
        {
            States.Add(new JSONStringValueState());
            States.Add(new JSONNumberValueState());
            States.Add(new JSONObjectValueState());
            States.Add(new JSONArrayValueState());
            States.Add(new JSONBooleanValueState());
            States.Add(new JSONNullValueState());
        }
    }
}
