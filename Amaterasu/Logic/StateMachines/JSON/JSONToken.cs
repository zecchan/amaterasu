﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONToken
    {
        public object? Value { get; }

        public JSONTokenType TokenType { get; }

        public JSONToken()
        {
            TokenType = JSONTokenType.Null;
        }

        public JSONToken(bool value)
        {
            Value = value;
            TokenType = JSONTokenType.Boolean;
        }

        public JSONToken(string value)
        {
            Value = value;
            TokenType = JSONTokenType.String;
        }

        public JSONToken(double value)
        {
            Value = value;
            TokenType = JSONTokenType.Number;
        }

        public JSONToken(IEnumerable<JSONToken> value)
        {
            Value = value;
            TokenType = JSONTokenType.Array;
        }

        public JSONToken(Dictionary<string, JSONToken> value)
        {
            Value = value;
            TokenType = JSONTokenType.Object;
        }
    }

    public enum JSONTokenType
    {
        Null,

        String,
        Number,
        Object,
        Array,
        Boolean,
    }
}
