﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONStringValueState : ICapturingState<char, JSONToken>
    {
        public string Name => "string";

        public bool CanExit { get; protected set; }

        protected List<char> Buffer { get; } = new List<char>();
        protected static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }

        public void Accept(char token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token.ToString());
            if (token == '\"' && !entered)
            {
                entered = true;
                return;
            }

            if (wantsUnicodeHex)
            {
                var validChars = "1234567890abcdefABCDEF";
                if (!validChars.Contains(token)) throw new InvalidTokenException("Invalid hex value: " + token);
                unicodeHex += token;
                if (unicodeHex.Length == 4)
                {
                    try
                    {
                        var buf = StringToByteArray(unicodeHex);
                        Buffer.Add(Encoding.UTF8.GetString(buf)[0]);
                        wantsUnicodeHex = false;
                        unicodeHex = "";
                    }
                    catch
                    {
                        throw new InvalidTokenException("Invalid unicode character \\u" + unicodeHex);
                    }
                }
            }
            else if (afterEscape)
            {
                var validChars = new[] { '"', '\\', '/', 'b', 'f', 'n', 'r', 't', 'u' };
                if (!validChars.Contains(token)) throw new InvalidTokenException("Invalid escape character \\" + token);

                else if (token == 'b') Buffer.Add('\b');
                else if (token == 'f') Buffer.Add('\f');
                else if (token == 'n') Buffer.Add('\n');
                else if (token == 'r') Buffer.Add('\r');
                else if (token == 't') Buffer.Add('\t');
                else if (token == 'u') { wantsUnicodeHex = true; unicodeHex = ""; }
                else Buffer.Add(token);
                afterEscape = false;
            }
            else if (token == '\\')
            {
                afterEscape = true;
            }
            else if (token == '"')
            {
                CanExit = true;
            }
            else
            {
                Buffer.Add(token);
            }
        }

        bool entered = false;
        bool afterEscape = false;
        bool wantsUnicodeHex = false;
        string unicodeHex = "";

        public bool CanAccept(char token)
        {
            if (!entered)
            {
                return token == '\"';
            }
            return !CanExit;
        }

        public JSONToken? GetResult()
        {
            if (!CanExit) throw new Exception();
            return new JSONToken(new string(Buffer.ToArray()));
        }

        public void Reset()
        {
            CanExit = false;
            afterEscape = false;
            wantsUnicodeHex = false;
            entered = false;
            unicodeHex = "";
            Buffer.Clear();
        }
    }
}
