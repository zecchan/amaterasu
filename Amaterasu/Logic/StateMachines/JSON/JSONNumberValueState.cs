﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONNumberValueState : ICapturingState<char, JSONToken>
    {
        public string Name => "number";

        public bool CanExit { get; protected set; }

        protected List<char> Buffer { get; } = new List<char>();

        public void Accept(char token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token.ToString());
            if (!entered)
            {
                entered = true;
                Buffer.Add(token);
                afterDigit = CanExit = IsDigit(token);
            }
            else
            {
                if (afterComma) afterComma = false;
                if (afterExponent) afterExponent = false;

                if (token == '.')
                {
                    afterComma = true;
                    inFraction = true;
                    hasComma = true;
                }
                if (token == 'e' || token == 'E')
                {
                    afterExponent = true;
                    hasExponent = true;
                }

                CanExit = afterDigit = IsDigit(token);
                Buffer.Add(token);
            }
        }

        bool entered = false;
        bool inFraction = false;
        bool afterDigit = false;
        bool afterExponent = false;
        bool afterComma = false;
        bool hasExponent = false;
        bool hasComma = false;

        protected bool IsDigit(char token)
        {
            return "0123456789".Contains(token);
        }

        public bool CanAccept(char token)
        {
            if (!entered)
            {
                return token == '-' || IsDigit(token);
            }
            else
            {
                if (afterExponent)
                {
                    return IsDigit(token) || token == '+' || token == '-';
                }
                if (!inFraction)
                {
                    return IsDigit(token) || afterDigit && !hasComma && token == '.';
                }
                else
                {
                    return IsDigit(token) || afterDigit && !hasExponent && (token == 'e' || token == 'E');
                }
            }
        }

        public JSONToken? GetResult()
        {
            if (!CanExit) throw new Exception();
            return new JSONToken(double.Parse(new string(Buffer.ToArray())));
        }

        public void Reset()
        {
            entered = false;
            inFraction = false;
            afterDigit = false;
            afterExponent = false;
            afterComma = false;
            hasExponent = false;
            hasComma = false;
            Buffer.Clear();
        }
    }
}
