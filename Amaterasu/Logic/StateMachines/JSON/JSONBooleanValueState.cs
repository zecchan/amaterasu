﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONBooleanValueState : PatternCapturingState<char, JSONToken>
    {
        public override string Name => "boolean";

        public JSONBooleanValueState()
        {
            Patterns.Add("true".ToCharArray());
            Patterns.Add("false".ToCharArray());
        }

        public override JSONToken? GetResult()
        {
            if (!CanExit) throw new Exception();
            var str = new string(Buffer.ToArray());
            return new JSONToken(str == "true");
        }
    }
}
