﻿using Amaterasu.Logic.StateMachine;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Logic.StateMachines.JSON
{
    public class JSONArrayValueState : ICapturingState<char, JSONToken>
    {
        const string Whitespaces = " \t\r\n";
        bool entered = false;
        bool enteredValueState = false;
        bool canAcceptValue = false;

        public string Name => "array";

        public bool CanExit { get; protected set; }

        protected JSONValueState ValueState { get; set; }

        protected List<JSONToken> ResultBuffer { get; } = new List<JSONToken>();

        public void Accept(char token)
        {
            if (!CanAccept(token)) throw new InvalidTokenException(Name, token.ToString());
            if (!entered)
            {
                canAcceptValue = true;
                entered = true;
                ValueState = new JSONValueState();
                return;
            }
            else
            {
                if (enteredValueState)
                {
                    if (Whitespaces.Contains(token) || token == ']' || token == ',')
                    {
                        if (!ValueState.CanExit) throw new InvalidTokenException(ValueState.CurrentState?.Name ?? Name, token.ToString());
                        var res = ValueState.GetResult();
                        if (res == null) throw new Exception("Failed to get value");
                        ResultBuffer.Add(res);
                        enteredValueState = false;
                        canAcceptValue = false;

                        if (token == ']')
                        {
                            CanExit = true;
                        }
                        else if (token == ',')
                        {
                            canAcceptValue = true;
                            ValueState.Reset();
                        }
                    }
                    else
                    {
                        ValueState.Accept(token);
                    }
                }
                else
                {
                    if (canAcceptValue && ValueState.CanAccept(token))
                    {
                        ValueState.Accept(token);
                        enteredValueState = true;
                    }
                    else
                    {
                        if (token == ']')
                        {
                            CanExit = true;
                        }
                        else if (token == ',')
                        {
                            canAcceptValue = true;
                        }
                    }
                }
            }
        }

        public bool CanAccept(char token)
        {
            if (CanExit) return false;
            if (!entered)
            {
                return token == '[';
            }

            if (enteredValueState)
            {
                return ValueState.CanAccept(token) || Whitespaces.Contains(token) || token == ']' || token == ',';
            }
            else
            {
                if (canAcceptValue && ValueState.CanAccept(token))
                {
                    return true;
                }
                else
                {
                    return Whitespaces.Contains(token) || token == ']' || (!canAcceptValue && ResultBuffer.Count > 0 && token == ',');
                }
            }
        }

        public JSONToken? GetResult()
        {
            if (!CanExit) throw new Exception();
            return new JSONToken(ResultBuffer.ToArray());
        }

        public void Reset()
        {
            entered = false;
            enteredValueState = false;
            canAcceptValue = false;
            CanExit = false;
            ValueState = null;
            ResultBuffer.Clear();
        }
    }
}
