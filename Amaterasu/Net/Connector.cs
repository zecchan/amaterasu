﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Amaterasu.Net
{
    public class Connector
    {
        /// <summary>
        /// Gets the connector version.
        /// </summary>
        public static Version Version { get; } = new Version(1, 0);

        /// <summary>
        /// Gets the current connector type.
        /// </summary>
        public ConnectorType ConnectorType { get; }

        /// <summary>
        /// Gets the current connection state.
        /// </summary>
        public ConnectorConnectionState ConnectionState { get; private set; } = ConnectorConnectionState.Closed;

        /// <summary>
        /// Gets the current end point.
        /// </summary>
        public IPEndPoint LocalEndPoint { get; private set; }

        /// <summary>
        /// Gets the outer node end point. This is your public end point on the internet.
        /// </summary>
        public IPEndPoint OuterNodeEndPoint { get; private set; }

        /// <summary>
        /// Gets the current end point.
        /// </summary>
        public IPEndPoint RemoteEndPoint { get; private set; }

        int _timeout = 1000;
        /// <summary>
        /// Gets or sets the connection timeout.
        /// </summary>
        public int Timeout { get => _timeout; set => _timeout = Math.Max(100, value); }

        public Connector(ConnectorType connectorType)
        {
            ConnectorType = connectorType;
        }

        int _poolRefreshRate = 1000;
        /// <summary>
        /// Gets or sets the connection refresh rate while in the pool. This will make sure your internet end point won't change.
        /// </summary>
        public int PoolRefreshRate { get => _poolRefreshRate; set => _poolRefreshRate = Math.Max(100, value); }

        TcpClient _hubClient = null;

        Uri _hubUri = null;
        int _hubPort = 0;
        public void Handshake(string hubUri)
        {
            Close();
            var rand = new Random();
            _hubUri = new Uri(hubUri);
            _hubPort = rand.Next(20000, 30000);
            LocalEndPoint = new IPEndPoint(IPAddress.Any, _hubPort);
            _hubClient = new TcpClient();
            _hubClient.Connect(_hubUri.Host, 80);
            
            var payload = "GET " + _hubUri.AbsolutePath + " HTTP/1.1\r\n" +
                "Host: " + _hubUri.Host + "\r\n" +
                "Connection: keep-alive\r\n" +
                "User-Agent: AmaterasuConnector/" + Version.ToString() + "\r\n" +
                "\r\n";
            _hubClient.Client.Send(Encoding.UTF8.GetBytes(payload));

            // wait until server send data back
            var start = DateTime.Now;
            while (_hubClient.Available <= 0)
            {
                Thread.Sleep(100);
                var delta = DateTime.Now - start;
                if (delta.TotalMilliseconds >= Timeout)
                    throw new Exception("Connection to pool is timed out");
            }

            var buf = new byte[_hubClient.Available];
            var i = _hubClient.Client.Receive(buf);
            var response1 = Encoding.UTF8.GetString(buf, 0, i);

            // check the response, must be a valid hub response
            ConnectionState = ConnectorConnectionState.Active;
        }

        public void Close()
        {
            try
            {

            }
            catch
            {

            }
            finally
            {
                ConnectionState = ConnectorConnectionState.Closed;
            }
        }
    }

    public enum ConnectorType
    {
        TCP,
        UDP,
    }

    public enum ConnectorConnectionState
    {
        Closed,
        Active,
        InPool,
        Connected
    }
}
