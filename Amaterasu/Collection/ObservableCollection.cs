﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Collection
{
    public class ObservableCollection<T> : ICollection<T>, IObservable<ObservableCollection<T>>, IObserver<T> where T : IObservable<T>
    {
        private Dictionary<T, IDisposable> unsubscribers { get; } = new Dictionary<T, IDisposable>();
        protected List<T> collection { get; } = new List<T>();
        protected List<IObserver<ObservableCollection<T>>> observers { get; } = new List<IObserver<ObservableCollection<T>>>();

        public int Count => collection.Count;

        public bool IsReadOnly => false;

        public void Add(T item)
        {
            if (!collection.Contains(item))
            {
                collection.Add(item);
                Subscribe(item);
                NotifySubscribers();
            }
        }

        public void Clear()
        {
            collection.Clear();
            UnsubscribeAll();
            NotifySubscribers();
        }

        public bool Contains(T item)
        {
            return collection.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            collection.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return collection.ToArray().ToList().GetEnumerator();
        }

        public bool Remove(T item)
        {
            if (collection.Contains(item))
            {
                var res = collection.Remove(item);
                if (res)
                {
                    Unsubscribe(item);
                    NotifySubscribers();
                    return true;
                }
            }
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return collection.ToArray().ToList().GetEnumerator();
        }

        private class Unsubscriber<TObservable>: IDisposable
        {
            private List<IObserver<TObservable>> _observers;
            private IObserver<TObservable> _observer;

            public Unsubscriber(List<IObserver<TObservable>> observers, IObserver<TObservable> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }

            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }

        public IDisposable Subscribe(IObserver<ObservableCollection<T>> observer)
        {
            if (!observers.Contains(observer))
            {
                observers.Add(observer);
                observer.OnNext(this);
            }
            return new Unsubscriber<ObservableCollection<T>>(observers, observer);
        }
        private void NotifySubscribers()
        {
            foreach (var observer in observers)
                observer.OnNext(this);
        }

        private void Subscribe(T child)
        {
            unsubscribers.Add(child, child.Subscribe(this));
        }
        private void Unsubscribe(T child)
        {
            if (unsubscribers.ContainsKey(child))
            {
                unsubscribers[child].Dispose();
                unsubscribers.Remove(child);
            }
        }
        private void UnsubscribeAll()
        {
            foreach(var cs in unsubscribers)
                cs.Value.Dispose();
            unsubscribers.Clear();
        }

        public void OnCompleted()
        {
            // NYI: How to know who send the OnComplete signal?
        }

        public void OnError(Exception error)
        {
        }

        public void OnNext(T value)
        {
            NotifySubscribers();
        }
    }
}
