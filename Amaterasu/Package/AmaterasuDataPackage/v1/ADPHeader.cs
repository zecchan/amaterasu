﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Package.AmaterasuDataPackage.v1
{
    public class ADPHeader
    {
        /// <summary>
        /// The magic number of the archive
        /// </summary>
        public byte[] MagicNumber { get; } = Encoding.ASCII.GetBytes("ADP1");

        public ADPArchiveFlag Flag { get; set; } = ADPArchiveFlag.BigArchive;

        public byte BlockSize { get; set; } = 4;

        public long TableOffset { get; set; }

        public long? RecoveryOffset { get; set; }

        public long? MetadataOffset { get; set; }

        // ====== Informational ======

        /// <summary>
        /// Gets the address size.
        /// </summary>
        public int AddressSize { get => Flag.HasFlag(ADPArchiveFlag.BigArchive) ? sizeof(long) : sizeof(int); }

        /// <summary>
        /// Gets the table offset address.
        /// </summary>
        public int TableOffsetAddress { get => MagicNumber.Length + 3; }

        /// <summary>
        /// Gets the start of the payload.
        /// </summary>
        public long PayloadOffset
        {
            get
            {
                var addr = MagicNumber.Length + 3 + AddressSize; // 3 is from the flag and blocksize followed by table offset
                if (Flag.HasFlag(ADPArchiveFlag.Recovery)) addr += AddressSize;
                if (Flag.HasFlag(ADPArchiveFlag.Metadata)) addr += AddressSize;
                return addr;
            }
        }
    }
}
