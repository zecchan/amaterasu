﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Package.AmaterasuDataPackage.v1
{
    public enum ADPArchiveFlag: ushort
    {
        /// <summary>
        /// All address is written in ulong instead of uint.
        /// </summary>
        BigArchive = 1,

        /// <summary>
        /// This archive has recovery partition.
        /// </summary>
        Recovery = 2,

        /// <summary>
        /// This archive has metadata partition.
        /// </summary>
        Metadata = 4,

        /// <summary>
        /// The payload of this archive is compressed.
        /// </summary>
        Compressed = 8,

        /// <summary>
        /// The payload of this archive is encrypted.
        /// </summary>
        Encrypted = 16,

        /// <summary>
        /// The table of this archive is encrypted.
        /// </summary>
        EncryptedTable = 32,

        /// <summary>
        /// The recovery partition of this archive is encrypted.
        /// </summary>
        EncryptedRecovery = 64,

        /// <summary>
        /// The metadata partition of this archive is encrypted.
        /// </summary>
        EncryptedMetadata = 128
    }
}
