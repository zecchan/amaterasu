﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Amaterasu.Package.AmaterasuDataPackage.v1
{
    public class ADPArchive
    {
        public ADPHeader Header { get; } = new ADPHeader();

        public void WriteAddress(long address, Stream stream)
        {
            if (Header.Flag.HasFlag(ADPArchiveFlag.BigArchive))
                stream.Write(address);
            else
                stream.Write((int)address);
        }

        /// <summary>
        /// Writes archive data starting from the current position of supplied stream.
        /// </summary>
        /// <param name="stream">The stream to write to</param>
        public void WriteTo(Stream stream)
        {
            // Section: Header
            stream.Write(Header.MagicNumber, 0, Header.MagicNumber.Length);
            stream.Write((ushort)Header.Flag);
            stream.Write(Header.BlockSize);

            stream.Seek(Header.PayloadOffset, SeekOrigin.Begin);
            // Section: Payload

            // Section: Table

            // Section: Recovery
            if (Header.Flag.HasFlag(ADPArchiveFlag.Recovery))
            {
            }

            // Section: Metadata
            if (Header.Flag.HasFlag(ADPArchiveFlag.Metadata))
            {
            }

            // Write offset for table, recovery and metadata
            stream.Seek(Header.TableOffsetAddress, SeekOrigin.Begin);
            WriteAddress(Header.TableOffset, stream);
            if (Header.Flag.HasFlag(ADPArchiveFlag.Recovery))
                WriteAddress(Header.RecoveryOffset.Value, stream);
            if (Header.Flag.HasFlag(ADPArchiveFlag.Metadata))
                WriteAddress(Header.MetadataOffset.Value, stream);
        }
    }
}
