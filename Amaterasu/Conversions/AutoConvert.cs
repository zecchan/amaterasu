﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Conversions
{
    public static class AutoConvert
    {
        public static object? ConvertToType<T>(this T obj, Type type)
        {
            var sourceType = typeof(T);

            if (sourceType == type) return obj;

            if (obj == null)
            {
                return null;
            }

            if (type == typeof(string))
            {
                return obj?.ToString();
            }

            if (type.IsNumericType())
            {
                var strVal = obj?.ToString();
                decimal d;
                if (decimal.TryParse(strVal, out d))
                {
                    switch(Type.GetTypeCode(type))
                    {
                        case TypeCode.Byte: return (byte)d;
                        case TypeCode.SByte: return (byte)d;
                        case TypeCode.UInt16: return (byte)d;
                        case TypeCode.UInt32: return (byte)d;
                        case TypeCode.UInt64: return (byte)d;
                        case TypeCode.Int16: return (byte)d;
                        case TypeCode.Int32: return (byte)d;
                        case TypeCode.Int64: return (byte)d;
                        case TypeCode.Single: return (byte)d;
                        case TypeCode.Double: return (byte)d;
                        case TypeCode.Decimal: return d;
                    }
                }
            }

            if (type == typeof(DateTime))
            {
                var strVal = obj?.ToString();
                DateTime d;
                if (DateTime.TryParse(strVal, out d))
                {
                    return d;
                }
            }

            return default;
        }

        public static bool IsNullableType(this Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }

        public static bool IsNumericType(this Type type, bool includeNullable = false)
        {
            HashSet<Type> NumericTypes = new HashSet<Type>
            {
                typeof(sbyte),
                typeof(byte),
                typeof(short),
                typeof(ushort),
                typeof(int),
                typeof(uint),
                typeof(long),
                typeof(ulong),
                typeof(float),
                typeof(double),
                typeof(decimal),
            }; 
            return NumericTypes.Contains(type) ||
              (includeNullable && NumericTypes.Contains(Nullable.GetUnderlyingType(type)));
        }
    }
}
