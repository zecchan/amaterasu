﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace Amaterasu.Cryptography
{
    public static class Hasher
    {
        /// <summary>
        /// Reads a stream from start to end and compute the hash.
        /// </summary>
        /// <returns>HashResult</returns>
        public static HashResult ComputeHash(Stream stream, HashAlgorithm algorithm)
        {
            stream.Seek(0, SeekOrigin.Begin);
            var buff = algorithm.ComputeHash(stream);
            return new HashResult(buff);
        }

        /// <summary>
        /// Reads a stream from start to end and compute the hash.
        /// </summary>
        /// <returns>HashResult</returns>
        public static HashResult ComputeHash(Stream stream, CommonHashAlgorithm algorithm)
        {
            return ComputeHash(stream, CreateHashAlgorithm(algorithm));
        }

        /// <summary>
        /// Reads a buffer and compute the hash.
        /// </summary>
        /// <returns>HashResult</returns>
        public static HashResult ComputeHash(byte[] buffer, int offset, int length, HashAlgorithm algorithm)
        {
            var buff = algorithm.ComputeHash(buffer, offset, length);
            return new HashResult(buff);
        }

        /// <summary>
        /// Reads a buffer and compute the hash.
        /// </summary>
        /// <returns>HashResult</returns>
        public static HashResult ComputeHash(byte[] buffer, int offset, int length, CommonHashAlgorithm algorithm)
        {
            return ComputeHash(buffer, offset, length, CreateHashAlgorithm(algorithm));
        }

        /// <summary>
        /// Reads whole buffer and compute the hash.
        /// </summary>
        /// <returns>HashResult</returns>
        public static HashResult ComputeHash(byte[] buffer, HashAlgorithm algorithm)
        {
            var buff = algorithm.ComputeHash(buffer);
            return new HashResult(buff);
        }

        /// <summary>
        /// Reads whole buffer and compute the hash.
        /// </summary>
        /// <returns>HashResult</returns>
        public static HashResult ComputeHash(byte[] buffer, CommonHashAlgorithm algorithm)
        {
            return ComputeHash(buffer, CreateHashAlgorithm(algorithm));
        }

        /// <summary>
        /// Creates an instance of a common hash algorithm.
        /// </summary>
        /// <returns>HashAlgorithm</returns>
        /// <exception cref="NotSupportedException"></exception>
        public static HashAlgorithm CreateHashAlgorithm(CommonHashAlgorithm hashAlgorithm)
        {
            switch (hashAlgorithm)
            {
                case CommonHashAlgorithm.MD5:
                    return MD5.Create();
                case CommonHashAlgorithm.SHA1:
                    return SHA1.Create();
                case CommonHashAlgorithm.SHA256:
                    return SHA256.Create();
                case CommonHashAlgorithm.SHA384:
                    return SHA384.Create();
                case CommonHashAlgorithm.SHA512:
                    return SHA512.Create();
                default:
                    throw new NotSupportedException("This algorithm is not supported");
            }
        }
    }

    public enum CommonHashAlgorithm
    {
        MD5,
        SHA1,
        SHA256,
        SHA384,
        SHA512,
    }

    public class HashResult
    {
        /// <summary>
        /// Gets the raw byte array hash.
        /// </summary>
        public byte[] Raw { get; }

        /// <summary>
        /// Gets the string hash.
        /// </summary>
        public string Hash { get; }

        /// <summary>
        /// Gets the hash as a number. This is insecure and should only be used for comparing hash. It takes the first 4 bytes of raw hash and covert it to int.
        /// </summary>
        public int Number { get; }

        public HashResult(byte[] raw)
        {
            if (raw == null)
                throw new ArgumentNullException(nameof(raw));
            Raw = raw;
            Hash = BitConverter.ToString(raw, 0, raw.Length).Replace("-", "").ToLower();
            if (Raw.Length > sizeof(int))
                Number = BitConverter.ToInt32(raw, 0);
        }
    }
}
