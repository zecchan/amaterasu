﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Amaterasu.Cryptography.Cryptoanalysis
{
    public class FrequencyCalculator
    {
        /// <summary>
        /// Gets or sets the number of byte in a group. Default is 1, minimum 1.
        /// </summary>
        public int ByteGroup { get => _byteGroup; set => _byteGroup = Math.Max(1, value); }
        int _byteGroup = 1;

        /// <summary>
        /// Gets or sets whether the byte group is read by sliding the pointer or reading it by block.
        /// </summary>
        public bool SlidingGroup { get; set; } = false;

        /// <summary>
        /// Calculates the frequency of byte group in a buffer.
        /// </summary>
        public FrequencyResult Calculate(byte[] buffer, int offset = 0, int length = 0)
        {
            if (buffer == null)
                throw new ArgumentNullException(nameof(buffer));
            using (var ms = new MemoryStream(buffer))
            {
                return Calculate(ms, offset, length);
            }
        }

        /// <summary>
        /// Calculates the frequency of characters in a text.
        /// </summary>
        public FrequencyResult Calculate(string text, Encoding encoding)
        {
            return Calculate(encoding.GetBytes(text));
        }

        /// <summary>
        /// Calculates the frequency of byte group in a stream.
        /// </summary>
        public FrequencyResult Calculate(Stream stream, long offset = 0, long length = 0)
        {
            if (stream == null)
                throw new ArgumentNullException(nameof(stream));

            if (!stream.CanSeek && offset != 0)
                throw new NotSupportedException("This stream cannot have an offset");
            if (offset > 0)
                stream.Seek(offset, SeekOrigin.Begin);
            if (offset < 0)
                stream.Seek(offset, SeekOrigin.End);

            offset = stream.Position;
            if (length <= 0)
                length = (stream.Length - offset) + length;
            if (length + offset > stream.Length || length + offset <= 0)
                throw new IndexOutOfRangeException("Length is out of range");

            var res = new FrequencyResult() { SlidingGroup = true };

            var group = new Queue<byte>();
            while (stream.Position < stream.Length)
            {
                if (SlidingGroup)
                {
                    var byt = stream.ReadByte();
                    if (byt < 0) break;
                    group.Enqueue((byte)byt);
                    if (group.Count < ByteGroup)
                        continue;
                    var data = group.ToArray();
                    res.GroupStreamCount++;
                    res.Increment(data);
                }
                else
                {
                    group.Clear();
                    for (var i = 0; i < ByteGroup; i++)
                    {
                        var byt = stream.ReadByte();
                        if (byt < 0) break;
                        group.Enqueue((byte)byt);
                    }
                    if (group.Count > 0)
                    {
                        var data = group.ToArray();
                        res.GroupStreamCount++;
                        res.Increment(data);
                    }
                }
            }

            return res;
        }
    }

    public class FrequencyResult
    {
        /// <summary>
        /// Gets the data frequencies.
        /// </summary>
        internal List<DataFrequency> _dataFrequencies { get; } = new List<DataFrequency>();

        /// <summary>
        /// Gets the data frequencies.
        /// </summary>
        public DataFrequency[] DataFrequencies { get => _dataFrequencies.ToArray(); }

        /// <summary>
        /// Gets the calculated number of groups in the stream.
        /// </summary>
        public int GroupStreamCount { get; internal set; }

        /// <summary>
        /// Gets the number of unique data group in the stream.
        /// </summary>
        public int GroupCount { get => _dataFrequencies.Count; }

        /// <summary>
        /// Gets whether the byte group is read by sliding the pointer or reading it by block.
        /// </summary>
        public bool SlidingGroup { get; internal set; }

        /// <summary>
        /// Gets the index of a data in DataFrequencies.
        /// </summary>
        public int IndexOf(byte[] data)
        {
            for (var w = 0; w < _dataFrequencies.Count; w++)
            {
                var df = _dataFrequencies[w];
                if (df.Data.Length != data.Length) continue;
                var same = true;
                for (var i = 0; i < df.Data.Length; i++)
                    if (df.Data[i] != data[i])
                    {
                        same = false;
                        break;
                    }
                if (same)
                    return w;
            }
            return -1;
        }

        /// <summary>
        /// Increments the frequency of data by 1.
        /// </summary>
        internal void Increment(byte[] data)
        {
            if (data == null || data.Length == 0) return;
            var idx = IndexOf(data);
            if (idx < 0)
            {
                var d = new DataFrequency(data, this);
                d.Occurences = 1;
                _dataFrequencies.Add(d);
            }
            else
            {
                var d = _dataFrequencies[idx];
                d.Occurences++;
                _dataFrequencies[idx] = d;
            }
        }
    }

    public struct DataFrequency
    {
        /// <summary>
        /// Gets the data that found.
        /// </summary>
        public byte[] Data { get; }

        /// <summary>
        /// Gets how many this data shows up in the stream.
        /// </summary>
        public int Occurences { get; internal set; }

        /// <summary>
        /// Gets the percentage of this data in the stream.
        /// </summary>
        public double Percentage { get => (double)Occurences / Result.GroupStreamCount; }

        /// <summary>
        /// Gets the frequency result that owns this data.
        /// </summary>
        public FrequencyResult Result { get; }

        public DataFrequency(byte[] data, FrequencyResult result)
        {
            Data = new byte[data.Length];
            data.CopyTo(Data, 0);
            Occurences = 0;
            Result = result;
        }
    }
}
