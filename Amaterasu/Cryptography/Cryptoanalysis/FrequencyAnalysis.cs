﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Amaterasu.Cryptography.Cryptoanalysis
{
    public class FrequencyAnalysis
    {
        /// <summary>
        /// Gets a list of baseline frequencies.
        /// </summary>
        public List<DataFrequency> BaselineFrequencies { get; } = new List<DataFrequency>();

        /// <summary>
        /// Gets or sets the number of byte in a group. Default is 1, minimum 1.
        /// </summary>
        public int ByteGroup { get => _byteGroup; set => _byteGroup = Math.Max(1, value); }
        int _byteGroup = 1;

        /// <summary>
        /// Gets or sets whether the byte group is read by sliding the pointer or reading it by block.
        /// </summary>
        public bool SlidingGroup { get; set; } = false;

        /// <summary>
        /// Gets or sets the tolerance of similar frequencies.
        /// </summary>
        public double Tolerance { get; set; } = 0.015;

        /// <summary>
        /// Generates a baseline for frequency analysis.
        /// </summary>
        public void GenerateBaseline(string text, Encoding encoding)
        {
            BaselineFrequencies.Clear();
            var fc = new FrequencyCalculator();
            fc.ByteGroup = ByteGroup;
            fc.SlidingGroup = SlidingGroup;
            var res = fc.Calculate(text, encoding);
            BaselineFrequencies.AddRange(res.DataFrequencies);
        }
        /// <summary>
        /// Generates a baseline for frequency analysis.
        /// </summary>
        public void GenerateBaseline(Stream stream, int offset = 0, int length = 0)
        {
            BaselineFrequencies.Clear();
            var fc = new FrequencyCalculator();
            fc.ByteGroup = ByteGroup;
            fc.SlidingGroup = SlidingGroup;
            var res = fc.Calculate(stream, offset, length);
            BaselineFrequencies.AddRange(res.DataFrequencies);
        }
        /// <summary>
        /// Generates a baseline for frequency analysis.
        /// </summary>
        public void GenerateBaseline(byte[] buffer, int offset = 0, int length = 0)
        {
            BaselineFrequencies.Clear();
            var fc = new FrequencyCalculator();
            fc.ByteGroup = ByteGroup;
            fc.SlidingGroup = SlidingGroup;
            var res = fc.Calculate(buffer, offset, length);
            BaselineFrequencies.AddRange(res.DataFrequencies);
        }

        /// <summary>
        /// Calculates the data frequency and compare it to baseline.
        /// </summary>
        public IEnumerable<DataFrequencyAnalysis> Calculate(string text, Encoding encoding)
        {
            var fc = new FrequencyCalculator();
            fc.ByteGroup = ByteGroup;
            fc.SlidingGroup = SlidingGroup;
            var res = fc.Calculate(text, encoding);
            return Calculate(res.DataFrequencies);
        }
        /// <summary>
        /// Calculates the data frequency and compare it to baseline.
        /// </summary>
        public IEnumerable<DataFrequencyAnalysis> Calculate(Stream stream, int offset = 0, int length = 0)
        {
            var fc = new FrequencyCalculator();
            fc.ByteGroup = ByteGroup;
            fc.SlidingGroup = SlidingGroup;
            var res = fc.Calculate(stream, offset, length);
            return Calculate(res.DataFrequencies);
        }
        /// <summary>
        /// Calculates the data frequency and compare it to baseline.
        /// </summary>
        public IEnumerable<DataFrequencyAnalysis> Calculate(byte[] buffer, int offset = 0, int length = 0)
        {
            var fc = new FrequencyCalculator();
            fc.ByteGroup = ByteGroup;
            fc.SlidingGroup = SlidingGroup;
            var res = fc.Calculate(buffer, offset, length);
            return Calculate(res.DataFrequencies);
        }

        #region Internal functions
        /// <summary>
        /// Calculates the data frequency and compare it to baseline.
        /// </summary>
        public IEnumerable<DataFrequencyAnalysis> Calculate(IEnumerable<DataFrequency> data)
        {
            var res = new List<DataFrequencyAnalysis>();
            foreach (var d in data)
            {
                var bline = BaselineFrequencies.Where(x => IsBufferEqual(x.Data, d.Data)).FirstOrDefault();
                var row = new DataFrequencyAnalysis()
                {
                    Data = d.Data,
                    Percentage = d.Percentage,
                };
                if (bline.Result != null)
                    row.ExpectedPercentage = bline.Percentage;
                var similar = BaselineFrequencies.Where(x => !IsBufferEqual(x.Data, d.Data) && IsSimilarFrequency(x, d)).Select(
                    x => new SimilarDataFrequency()
                        {
                            Data = x.Data,
                            Deviation = Math.Abs(d.Percentage - x.Percentage),
                            Percentage = x.Percentage
                        }
                    ).ToList();
                similar.Sort((a, b) => a.Percentage.CompareTo(b.Percentage));
                row.SimilarData = similar.ToArray();
                res.Add(row);
            }
            return res;
        }

        private bool IsBufferEqual(byte[] a, byte[] b)
        {
            if (a == null || b == null) return false;
            if (a.Length != b.Length) return false;
            for (var i = 0; i < a.Length; i++)
                if (a[i] != b[i]) return false;
            return true;
        }

        private bool IsSimilarFrequency(DataFrequency a, DataFrequency b)
        {
            return Math.Abs(a.Percentage - b.Percentage) <= Tolerance;
        }
        public static bool IsSimilarFrequency(double a, double b, double tolerance = 0.015)
        {
            return Math.Abs(a - b) <= tolerance;
        }
        #endregion
    }

    public struct DataFrequencyAnalysis
    {
        /// <summary>
        /// Gets the data that found.
        /// </summary>
        public byte[] Data { get; internal set; }

        /// <summary>
        /// Gets the percentage of this data in the stream.
        /// </summary>
        public double Percentage { get; internal set; }

        /// <summary>
        /// Gets the expected percentage of this data based on baseline.
        /// </summary>
        public double ExpectedPercentage { get; internal set; }

        /// <summary>
        /// Gets a list of similar data.
        /// </summary>
        public SimilarDataFrequency[] SimilarData { get; internal set; }
    }

    public struct SimilarDataFrequency
    {
        /// <summary>
        /// Gets the data that found.
        /// </summary>
        public byte[] Data { get; internal set; }

        /// <summary>
        /// Gets the percentage of this data in the stream.
        /// </summary>
        public double Percentage { get; internal set; }

        /// <summary>
        /// Gets the deviation of this similar data.
        /// </summary>
        public double Deviation { get; internal set; }
    }
}
