﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Archive
{
    public interface IAmaterasuArchiveFileSystem
    {
        /// <summary>
        /// Gets the version of archive file system
        /// </summary>
        Version Version { get; }

        Dictionary<string, IAmaterasuArchiveEntry> Entries { get; }
    }
}
