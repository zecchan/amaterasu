﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Archive
{
    public class AmaterasuArchive
    {
        public long EntriesTableOffset { get; private set; }

        public long DataOffset { get; private set; }
    }
}
