﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu
{
    public static class DateTimeExtension
    {
        public static DateTime ToUTC(this DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Local)
                return TimeZoneInfo.ConvertTimeToUtc(dateTime, TimeZoneInfo.Local);

            if (dateTime.Kind == DateTimeKind.Utc)
                return dateTime;

            throw new Exception("This DateTime value does not contains time zone information");
        }

        public static DateTime ToLocal(this DateTime dateTime)
        {
            if (dateTime.Kind == DateTimeKind.Utc)
                return TimeZoneInfo.ConvertTimeFromUtc(dateTime, TimeZoneInfo.Local);

            if (dateTime.Kind == DateTimeKind.Local)
                return dateTime;

            throw new Exception("This DateTime value does not contains time zone information");
        }

        public static DateTime AsUTC(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond, DateTimeKind.Utc);
        }
        public static DateTime AsLocal(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond, DateTimeKind.Local);
        }
    }
}
