﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu
{
    public static class StringExtension
    {
        public static string SplitEvery(this string str, string glue, uint every)
        {
            if (string.IsNullOrEmpty(str) || every == 0) return str;

            var pieces = new List<string>();
            var buffer = "";
            for(var i = 0; i < str.Length; i++)
            {
                buffer += str[i];
                if (buffer.Length == every)
                {
                    pieces.Add(buffer);
                    buffer = "";
                }
            }
            if (buffer.Length > 0) pieces.Add(buffer);
            return string.Join(glue, pieces);
        }
    }
}
