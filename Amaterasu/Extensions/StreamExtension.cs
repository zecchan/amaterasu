﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Amaterasu
{
    public static class StreamExtension
    {
        public static void Write(this Stream stream, string value)
        {
            value = value ?? "";
            var buffer = Encoding.UTF8.GetBytes(value);
            Write(stream, buffer.Length);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static string ReadString(this Stream stream)
        {
            var len = stream.ReadInt32();
            var buffer = new byte[len];
            stream.Read(buffer, 0, buffer.Length);
            return Encoding.UTF8.GetString(buffer);
        }

        public static void Write(this Stream stream, short value)
        {
            var buffer = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static short ReadInt16(this Stream stream)
        {
            var buffer = new byte[sizeof(short)];
            stream.Read(buffer, 0, buffer.Length);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            return BitConverter.ToInt16(buffer, 0);
        }

        public static void Write(this Stream stream, ushort value)
        {
            var buffer = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static ushort ReadUInt16(this Stream stream)
        {
            var buffer = new byte[sizeof(ushort)];
            stream.Read(buffer, 0, buffer.Length);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            return BitConverter.ToUInt16(buffer, 0);
        }

        public static void Write(this Stream stream, int value)
        {
            var buffer = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static int ReadInt32(this Stream stream)
        {
            var buffer = new byte[sizeof(int)];
            stream.Read(buffer, 0, buffer.Length);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            return BitConverter.ToInt32(buffer, 0);
        }

        public static void Write(this Stream stream, uint value)
        {
            var buffer = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static uint ReadUInt32(this Stream stream)
        {
            var buffer = new byte[sizeof(uint)];
            stream.Read(buffer, 0, buffer.Length);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            return BitConverter.ToUInt32(buffer, 0);
        }

        public static void Write(this Stream stream, long value)
        {
            var buffer = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static long ReadInt64(this Stream stream)
        {
            var buffer = new byte[sizeof(long)];
            stream.Read(buffer, 0, buffer.Length);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            return BitConverter.ToInt64(buffer, 0);
        }

        public static void Write(this Stream stream, ulong value)
        {
            var buffer = BitConverter.GetBytes(value);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static ulong ReadUInt64(this Stream stream)
        {
            var buffer = new byte[sizeof(ulong)];
            stream.Read(buffer, 0, buffer.Length);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(buffer);
            return BitConverter.ToUInt64(buffer, 0);
        }

        public static void Write(this Stream stream, float value)
        {
            var buffer = BitConverter.GetBytes(value);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static float ReadSingle(this Stream stream)
        {
            var buffer = new byte[sizeof(float)];
            stream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToSingle(buffer, 0);
        }

        public static void Write(this Stream stream, double value)
        {
            var buffer = BitConverter.GetBytes(value);
            stream.Write(buffer, 0, buffer.Length);
        }
        public static double ReadDouble(this Stream stream)
        {
            var buffer = new byte[sizeof(double)];
            stream.Read(buffer, 0, buffer.Length);
            return BitConverter.ToDouble(buffer, 0);
        }

        /// <summary>
        /// Writes an instance of IStreamWriteable to this stream.
        /// </summary>
        /// <param name="instance">The instance of IStreamWriteable</param>
        public static void Write(this Stream stream, IStreamWriteable instance)
        {
            if (instance == null)
                throw new ArgumentNullException(nameof(instance));
            instance.WriteTo(stream);
        }
        /// <summary>
        /// Creates a new instance of IStreamReadable and make it reads this stream.
        /// </summary>
        /// <typeparam name="T">A type that implements IStreamReadable</typeparam>
        /// <returns>A new instance of IStreamReadable</returns>
        public static T Read<T>(this Stream stream) where T : IStreamReadable
        {
            var instance = (T)Activator.CreateInstance(typeof(T));
            instance.ReadFrom(stream);
            return instance;
        }
        /// <summary>
        /// Make an instance of IStreamReadable to read this stream.
        /// </summary>
        /// <param name="instance">The instance of IStreamReadable</param>
        /// <typeparam name="T">A type that implements IStreamReadable</typeparam>
        /// <returns>A new instance of IStreamReadable</returns>
        public static void Read<T>(this Stream stream, T instance) where T : IStreamReadable
        {
            if (instance == null)
                throw new ArgumentNullException(nameof(instance));
            instance.ReadFrom(stream);
        }
    }
}
