﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Amaterasu
{
    public static class MemberInfoExtension
    {
        public static T GetCustomAttribute<T>(this MemberInfo mem, bool inherit) where T : Attribute
        {
            var at = typeof(T);
            return mem.GetCustomAttributes(inherit).Where(x => at.IsAssignableFrom(x.GetType())).FirstOrDefault() as T;
        }

        public static object GetValue(this MemberInfo mem, object obj)
        {
            if (mem is PropertyInfo pi)
                return pi.GetValue(obj);
            if (mem is FieldInfo fi)
                return fi.GetValue(obj);
            throw new InvalidOperationException();
        }
        public static void SetValue(this MemberInfo mem, object obj, object value)
        {
            if (mem is PropertyInfo pi)
            {
                pi.SetValue(obj, value);
                return;
            }
            if (mem is FieldInfo fi)
            {
                fi.SetValue(obj, value);
                return;
            }
            throw new InvalidOperationException();
        }

        public static Type GetValueType(this MemberInfo mem)
        {
            if (mem is PropertyInfo pi)
            {
                return pi.PropertyType;
            }
            if (mem is FieldInfo fi)
            {
                return fi.FieldType;
            }
            throw new InvalidOperationException();
        }

        public static bool Readable(this MemberInfo mem)
        {
            if (mem is PropertyInfo pi)
                return pi.CanRead;
            if (mem is FieldInfo fi)
                return true;
            return false;
        }
        public static bool Writeable(this MemberInfo mem)
        {
            if (mem is PropertyInfo pi)
                return pi.CanWrite;
            if (mem is FieldInfo fi)
                return true;
            return false;
        }
    }
}
