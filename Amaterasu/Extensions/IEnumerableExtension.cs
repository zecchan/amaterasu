﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu
{
    public static class IEnumerableExtension
    {
        public static IEnumerable<IEnumerable<T>> Bucket<T>(this IEnumerable<T> collection, Func<T, object> keySelector)
        {
            if (keySelector == null) throw new ArgumentNullException(nameof(keySelector));

            Dictionary<object, List<T>> buckets = new Dictionary<object, List<T>>();

            foreach (var ele in collection)
            {
                var k = keySelector.Invoke(ele);
                if (buckets.ContainsKey(k))
                {
                    buckets[k].Add(ele);
                }
                else
                {
                    buckets[k] = new List<T>();
                    buckets[k].Add(ele);
                }
            }

            return buckets.Values.ToList();
        }

        public static T PickRandom<T>(this IEnumerable<T> collection)
        {
            var cnt = collection.Count();
            var rand = new Random().Next(cnt);
            return cnt == 0 ? default : collection.ElementAt(rand);
        }

        public static T PickFirstMin<T>(this IEnumerable<T> collection, Func<T, int> valueFunc)
        {
            if (valueFunc == null) throw new ArgumentNullException(nameof(valueFunc));
            var minVal = int.MaxValue;
            T minEle = default(T);
            foreach (var ele in collection)
            {
                var val = valueFunc(ele);
                if (val < minVal)
                {
                    minEle = ele;
                    minVal = val;
                }
            }
            return minEle;
        }

        public static T PickFirstMax<T>(this IEnumerable<T> collection, Func<T, int> valueFunc)
        {
            if (valueFunc == null) throw new ArgumentNullException(nameof(valueFunc));
            var maxVal = int.MinValue;
            T maxEle = default(T);
            foreach(var ele in collection)
            {
                var val = valueFunc(ele);
                if (val > maxVal)
                {
                    maxEle = ele;
                    maxVal = val;
                }
            }
            return maxEle;
        }
    }
}
