﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu
{
    public static class TypeExtension
    {
        public static T GetCustomAttribute<T>(this Type type, bool inherit) where T: Attribute
        {
            var at = typeof(T);
            return type.GetCustomAttributes(inherit).Where(x => at.IsAssignableFrom(x.GetType())).FirstOrDefault() as T;
        }
    }
}
