﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class ParameterExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            return expression is ParameterExpression;
        }

        public IExpressionParser Clone()
        {
            return new ParameterExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as ParameterExpression;
            var res = new ExpressionNode(expression);

            var re = root.CompileAndExecute(exp);
            if (re != null) return re;

            res.Type = ExpressionOperationType.Parameter;
            res.ReturnType = exp.Type;
            res.MemberName = exp.Name;
            return res;
        }
    }
}
