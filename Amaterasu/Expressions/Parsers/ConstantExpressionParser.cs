﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class ConstantExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            if (!(expression is ConstantExpression)) return false;
            return true;
        }

        public IExpressionParser Clone()
        {
            return new ConstantExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as ConstantExpression;
            var res = new ExpressionNode(expression);
            res.Type = ExpressionOperationType.Constant;
            res.ValueType = exp.Type;
            res.ConstantValue = exp.Value;
            return res;
        }
    }
}
