﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class MethodCallExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            if (!(expression is MethodCallExpression)) return false;
            return true;
        }

        public IExpressionParser Clone()
        {
            return new MethodCallExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as MethodCallExpression;
            var res = new ExpressionNode(expression);

            var re = root.CompileAndExecute(exp);
            if (re != null) return re;

            res.Type = ExpressionOperationType.MethodCall;
            res.ReturnType = exp.Method.ReturnType;
            res.MemberName = exp.Method.Name;
            res.Accessor = root.Parse(exp.Object);
            var args = exp.Arguments.Select(x => root.Parse(x));
            res.Arguments.AddRange(args);
            return res;
        }
    }
}
