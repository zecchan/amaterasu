﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class LambdaExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            if (!(expression is LambdaExpression)) return false;
            return expression.NodeType == ExpressionType.Lambda;
        }

        public IExpressionParser Clone()
        {
            return new LambdaExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as LambdaExpression;
            var res = new ExpressionNode(expression);
            res.ReturnType = exp.ReturnType;
            res.Body = root.Parse(exp.Body);
            res.Type = ExpressionOperationType.Lambda;
            return res;
        }
    }
}
