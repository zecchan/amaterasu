﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class UnaryExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            return expression is UnaryExpression;
        }

        public IExpressionParser Clone()
        {
            return new UnaryExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as UnaryExpression;

            var re = root.CompileAndExecute(exp);
            if (re != null) return re;

            var res = new ExpressionNode(exp);
            res.Type = ExpressionOperationType.Unary;
            res.Body = root.Parse(exp.Operand);
            return res;
        }
    }
}
