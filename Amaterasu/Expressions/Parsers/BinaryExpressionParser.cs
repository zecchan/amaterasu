﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class BinaryExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            if (!(expression is BinaryExpression)) return false;
            var typ = expression.NodeType;
            return
                typ == ExpressionType.And ||
                typ == ExpressionType.AndAlso ||
                typ == ExpressionType.Or ||
                typ == ExpressionType.OrElse ||
                typ == ExpressionType.NotEqual ||
                typ == ExpressionType.Equal ||
                typ == ExpressionType.LessThan ||
                typ == ExpressionType.GreaterThan ||
                typ == ExpressionType.LessThanOrEqual ||
                typ == ExpressionType.GreaterThanOrEqual;
        }

        public IExpressionParser Clone()
        {
            return new BinaryExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as BinaryExpression;
            var res = new ExpressionNode(expression);
            res.Type = ExpressionOperationType.Binary;
            res.Left = root.Parse(exp.Left);
            res.Right = root.Parse(exp.Right);
            return res;
        }
    }
}
