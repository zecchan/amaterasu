﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Expressions.Parsers
{
    public class MemberAccessExpressionParser : IExpressionParser
    {
        public bool Accepts(Expression expression)
        {
            if (!(expression is MemberExpression)) return false;
            return true;
        }

        public IExpressionParser Clone()
        {
            return new MemberAccessExpressionParser();
        }

        public ExpressionNode Parse(Expression expression, ExpressionParser root)
        {
            var exp = expression as MemberExpression;
            var res = new ExpressionNode(expression);

            var re = root.CompileAndExecute(exp);
            if (re != null) return re;

            res.Accessor = root.Parse(exp.Expression);
            res.Type = ExpressionOperationType.Member;
            res.ReturnType = exp.Type;
            res.MemberName = exp.Member.Name;
            return res;
        }
    }
}
