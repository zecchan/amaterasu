﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Amaterasu.Expressions
{
    /// <summary>
    /// Exposes methods to perform LINQ to SQL
    /// </summary>
    public class ExpressionParser
    {
        /// <summary>
        /// Parse a lambda expression and return it as expression result
        /// </summary>
        /// <param name="expression">The expression to parse</param>
        /// <returns>Expression result</returns>
        public ExpressionNode Parse<T>(Expression<Func<T, bool>> expression)
        {
            return Parse(expression as Expression);
        }

        /// <summary>
        /// Parse an expression and return it as expression result
        /// </summary>
        /// <param name="expression">The expression to parse</param>
        /// <returns>Expression result</returns>
        public ExpressionNode Parse(Expression expression)
        {
            // so we
            var ps = FindMatching(expression);

            foreach (var p in ps)
            {
                var np = p.Clone();
                return np.Parse(expression, this);
            }
            throw new Exception("Expression of type [" + expression.GetType().Name + ":" + expression.GetType().BaseType?.Name + "=>" + expression.NodeType + "] is not supported!");
        }

        public IEnumerable<IExpressionParser> FindMatching(Expression expression)
        {
            var w = ExpressionParsers.Where(x => x.Accepts(expression)).ToList();
            w.Reverse();
            return w;
        }

        public void LoadAvailableIExpressionParsers()
        {
            var ch = typeof(IExpressionParser);
            var typs = Assembly.GetExecutingAssembly().GetTypes().Where(x => ch.IsAssignableFrom(x)).ToList();
            foreach (var t in typs)
            {
                try
                {
                    var o = Activator.CreateInstance(t) as IExpressionParser;
                    if (o != null)
                        ExpressionParsers.Add(o);
                }
                catch { }
            }
        }

        /// <summary>
        /// Gets a list of ExpressionParsers
        /// </summary>
        public List<IExpressionParser> ExpressionParsers { get; } = new List<IExpressionParser>();

        public ExpressionNode CompileAndExecute(Expression expression)
        {
            try
            {
                var reduced = false;
                // reduce until it cannot be reduced anymore
                while (expression.CanReduce)
                {
                    reduced = true;
                    expression = expression.Reduce();
                }

                try
                {
                    // execute the expression   
                    var lam = Expression.Lambda(expression);
                    var comp = lam.Compile();
                    var res = comp.DynamicInvoke();
                    return new ExpressionNode()
                    {
                        NodeType = ExpressionType.Constant,
                        Type = ExpressionOperationType.Constant,
                        ValueType = res?.GetType(),
                        ConstantValue = res
                    };
                }
                catch { }

                if (reduced)
                    return Parse(expression);
            }
            catch { }
            return null;
        }
    }

    public interface IExpressionParser
    {
        bool Accepts(Expression expression);

        ExpressionNode Parse(Expression expression, ExpressionParser root);

        /// <summary>
        /// Clone this parser. Must be implemented.
        /// </summary>
        /// <returns>Cloned parser.</returns>
        IExpressionParser Clone();
    }

    /// <summary>
    /// A node in an expresion tree
    /// </summary>
    public class ExpressionNode
    {
        /// <summary>
        /// Gets or sets the expression node
        /// </summary>
        public ExpressionType NodeType { get; set; }
        public ExpressionOperationType Type { get; set; }

        public Type ValueType { get; set; }
        public Type ReturnType { get; set; }

        public string MemberName { get; set; }
        public object ConstantValue { get; set; }

        public List<ExpressionNode> Arguments { get; } = new List<ExpressionNode>();
        public ExpressionNode Body { get; set; }
        public ExpressionNode Accessor { get; set; }
        public ExpressionNode Left { get; set; }
        public ExpressionNode Right { get; set; }

        public ExpressionNode() { }
        public ExpressionNode(ExpressionType type)
        {
            NodeType = type;
        }
        public ExpressionNode(Expression expression)
        {
            NodeType = expression.NodeType;
        }
    }

    public enum ExpressionOperationType
    {
        Lambda,
        Binary,
        Unary,
        Constant,
        MethodCall,
        Member,
        Parameter
    }

}
