﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
    public class MemberAttribute: Attribute
    {
        public string? Name { get; set; } 

        public MemberAttribute(string? name = null) {
            Name = name;
        }
    }
}
