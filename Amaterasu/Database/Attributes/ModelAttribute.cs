﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class ModelAttribute : Attribute
    {
        /// <summary>
        /// The name of the data source table or collection.
        /// </summary>
        public string SourceName { get; set; }

        /// <summary>
        /// The name of the schema.
        /// </summary>
        public string SchemaName { get; set; }

        /// <summary>
        /// Should validate before saving
        /// </summary>
        public bool Validate { get; set; }

        public ModelAttribute(string sourceName, string schemaName = null, bool validate = false)
        {
            SourceName = sourceName;
            SchemaName = schemaName;
            Validate = validate;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class PrimaryKeyAttribute : Attribute
    {

    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class IgnoreMemberAttribute : Attribute
    {

    }
}
