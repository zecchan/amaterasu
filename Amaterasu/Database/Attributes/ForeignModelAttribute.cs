﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ForeignModelAttribute: Attribute
    {
        public string LocalKey { get; set; }

        public string ForeignKey { get; set; }

        /// <summary>
        /// Automatically loads this model data
        /// </summary>
        /// <param name="foreignKey">The foreign key field</param>
        /// <param name="localKey">The id field in the foreign table</param>
        public ForeignModelAttribute(string foreignKey, string localKey = "id")
        {
            LocalKey = localKey;
            ForeignKey = foreignKey;
        }
    }
}
