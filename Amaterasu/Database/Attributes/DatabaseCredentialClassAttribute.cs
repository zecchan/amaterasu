﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Attributes
{
    /// <summary>
    /// Defines the configuration class used by this database engine
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DatabaseCredentialClassAttribute: Attribute
    {
        public Type ConfigurationClass { get; }

        public string EngineId { get; }

        public DatabaseCredentialClassAttribute(Type configurationClass, string engineId) { 
            ConfigurationClass = configurationClass;
            EngineId = engineId;
        }
    }
}
