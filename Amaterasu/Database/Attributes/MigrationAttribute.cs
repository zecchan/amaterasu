﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class MigrationAttribute: Attribute
    {
        public string? Name { get; set; }

        public string Description { get; set; }

        public string? Group { get; set; }

        public int Sequence { get; set; }

        public MigrationAttribute(int sequence, string? group = null, string? name = null, string description = "")
        {
            Name = name;
            Description = description;
            Group = group;
            Sequence = sequence;
        }
    }
}
