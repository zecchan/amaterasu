﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public class DatabaseColumnConstructor
    {
        /// <summary>
        /// Gets the name of the table
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the constructor mode
        /// </summary>
        DatabaseConstructorMode Mode { get; }
    }
}
