﻿using Amaterasu.Expressions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using Amaterasu.Database.Core;

namespace Amaterasu.Database
{
    public class QueryBuilder<T> : IQueryBuilder<T> where T : class, new()
    {
        public string Source { get; set; }
        public ExpressionParser Parser { get; set; }
        public ExpressionToSQL LINQ2SQL { get; set; }

        public QueryLimit Limit { get; } = new QueryLimit();

        public List<QueryMember> Fields { get; } = new List<QueryMember>();

        public List<QuerySort> Sorts { get; } = new List<QuerySort>();
        public ISqlDatabaseContext DatabaseContext { get; set; }
        IDatabaseContext IQueryBuilder<T>.DatabaseContext {
            get => DatabaseContext; 
            set => DatabaseContext = value as ISqlDatabaseContext; 
        }

        public QueryBuilder()
        {
            Clear();
        }

        protected ExpressionNode QueryExpressionNode
        {
            get => ExpressionNodeStack.Count > 0 ? ExpressionNodeStack.Peek().Node : null;
            set
            {
                if (ExpressionNodeStack.Count > 0)
                    ExpressionNodeStack.Pop();
                ExpressionNodeStack.Push(new ExpressionNodeStackRecord(value, ExpressionType.AndAlso));
            }
        }
        protected Stack<ExpressionNodeStackRecord> ExpressionNodeStack { get; } = new Stack<ExpressionNodeStackRecord>();

        protected virtual void SetSource()
        {
            if (Source == null)
                Source = ModelHelper.GetSourceName<T>();
        }

        protected virtual void PrepareForBuild()
        {
            SetSource();
            if (ExpressionNodeStack.Count > 1) throw new Exception("Query group is not closed, count: " + (ExpressionNodeStack.Count - 1));
        }

        protected virtual string BuildSelect()
        {
            if (Fields.Count == 0)
                return "*";
            return string.Join(", ", Fields.Select(x => LINQ2SQL.ToSelectField(x)));
        }

        public virtual string BuildLimit()
        {
            if (Limit.Take <= 0) return "";
            var lim = " LIMIT ";
            if (Limit.Skip > 0)
                lim += Limit.Skip + ", ";
            lim += Limit.Take;
            return lim;
        }

        public virtual string BuildSort()
        {
            if (Sorts.Count == 0) return "";
            var sort = " ORDER BY " + string.Join(", ", Sorts.Select(s => s.Name + " " + (s.SortDirection == QuerySortDirection.Ascending ? "ASC" : "DESC")));

            return sort;
        }

        public virtual SQLWithParameter BuildSQL(bool clear = true)
        {
            PrepareForBuild();
            var whr = LINQ2SQL.ToWhereClause(QueryExpressionNode);

            if (string.IsNullOrWhiteSpace(Source))
                throw new Exception("Source is not defined for '" + typeof(T).FullName + "', make sure it have ModelAttribute");

            var sql = "SELECT " + BuildSelect() + " FROM " + LINQ2SQL.Quote(Source) +
                (string.IsNullOrEmpty(whr?.Where) ? "" : " WHERE " + whr.Where)
                + BuildLimit() + BuildSort();

            var res = new SQLWithParameter();
            res.SQL = sql;
            if (whr?.Parameters?.Count > 0)
            {
                res.Parameters = whr.Parameters;
            }
            if (clear) Clear();

            return res;
        }

        public int Count(bool clear = true)
        {
            var of = Fields.ToArray();
            var os = Sorts.ToArray();

            // we don't want fields
            Fields.Clear();
            Fields.Add(new QueryMember()
            {
                Name = "COUNT(*)",
                As = "cnt"
            });
            // or sorts, to speed up
            Sorts.Clear();

            var sql = BuildSQL(clear);
            if (!clear)
            {
                Fields.Clear();
                Fields.AddRange(of);
                Sorts.AddRange(os);
            }

            var cnt = 0;
            DatabaseContext.ExecuteQuery(sql.SQL, (rdr) =>
            {
                if (rdr.Read())
                    cnt = rdr.GetInt32(0);
            }, sql.Parameters);

            return cnt;
        }

        public virtual T First()
        {
            Take(1);
            return this.ToList().First();
        }

        public virtual T FirstOrDefault()
        {
            Take(1);
            return this.ToList().FirstOrDefault();
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (DatabaseContext == null)
                return null;
            var sql = BuildSQL();
            return DatabaseContext.ExecuteQueryEnumerator<T>(sql.SQL, sql.Parameters);
        }

        public virtual T Last()
        {
            return this.ToList().Last();
        }

        public virtual T LastOrDefault()
        {
            return this.ToList().LastOrDefault();
        }

        public virtual IQueryBuilder<T> OrderBy(Expression<Func<T, object>> expression, QuerySortDirection direction = QuerySortDirection.Ascending)
        {
            var mem = Parser.Parse(expression);
            var cvt = LINQ2SQL.ToWhereClause(mem.Body);
            Sorts.Add(new QuerySort()
            {
                Name = cvt.Where,
                SortDirection = direction
            });
            return this;
        }

        public IQueryBuilder<T> Select(params Expression<Func<T, object>>[] expression)
        {
            Fields.Clear();
            foreach (var ex in expression)
            {
                var mem = Parser.Parse(ex);
                var cvt = LINQ2SQL.ToWhereClause(mem.Body);
                Fields.Add(new QueryMember()
                {
                    Name = cvt.Where
                });
            }
            return this;
        }

        public virtual IQueryBuilder<T> Skip(int skip)
        {
            Limit.Skip = Math.Max(0, skip);
            return this;
        }

        public virtual IQueryBuilder<T> Take(int take)
        {
            Limit.Take = Math.Max(0, take);
            return this;
        }

        public virtual IQueryBuilder<T> GroupStart()
        {
            ExpressionNodeStack.Push(new ExpressionNodeStackRecord(null));
            return this;
        }

        public virtual IQueryBuilder<T> OrGroupStart()
        {
            ExpressionNodeStack.Push(new ExpressionNodeStackRecord(null, ExpressionType.OrElse));
            return this;
        }

        public virtual IQueryBuilder<T> GroupEnd()
        {
            if (ExpressionNodeStack.Count <= 1) throw new Exception("No query group has been started");
            var pp = ExpressionNodeStack.Pop();
            var left = QueryExpressionNode;
            if (pp.Node != null)
            {
                if (left != null)
                {
                    QueryExpressionNode = new ExpressionNode()
                    {
                        Type = ExpressionOperationType.Binary,
                        NodeType = pp.NodeType,
                        Left = left,
                        Right = pp.Node
                    };
                }
                else
                {
                    QueryExpressionNode = pp.Node;
                }
            }
            return this;
        }

        public virtual IQueryBuilder<T> OrWhere(Expression<Func<T, bool>> expression)
        {
            if (expression == null) return this;
            var exn = Parser.Parse(expression).Body;
            if (QueryExpressionNode != null)
            {
                var left = QueryExpressionNode;
                QueryExpressionNode = new ExpressionNode()
                {
                    Type = ExpressionOperationType.Binary,
                    NodeType = ExpressionType.OrElse,
                    Left = left,
                    Right = exn
                };
            }
            else
                QueryExpressionNode = exn;
            return this;
        }

        public virtual IQueryBuilder<T> Where(Expression<Func<T, bool>> expression)
        {
            if (expression == null) return this;
            var exn = Parser.Parse(expression).Body;
            if (QueryExpressionNode != null)
            {
                var left = QueryExpressionNode;
                QueryExpressionNode = new ExpressionNode()
                {
                    Type = ExpressionOperationType.Binary,
                    NodeType = ExpressionType.AndAlso,
                    Left = left,
                    Right = exn
                };
            }
            else
                QueryExpressionNode = exn;
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Clear()
        {
            Fields.Clear();
            Sorts.Clear();
            Limit.Clear();
            ExpressionNodeStack.Clear();
            ExpressionNodeStack.Push(new ExpressionNodeStackRecord(null));
            QueryExpressionNode = null;
        }
    }
}
