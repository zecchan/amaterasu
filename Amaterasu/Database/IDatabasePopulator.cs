﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabasePopulator
    {
        /// <summary>
        /// Populate the database
        /// </summary>
        /// <param name="databaseContext">Database context to use</param>
        void Populate(IDatabaseContext databaseContext);

        /// <summary>
        /// Remove data populated by this migration
        /// </summary>
        /// <param name="databaseContext">Database context to use</param>
        void Depopulate(IDatabaseContext databaseContext);
    }
}
