﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Amaterasu.Database.Access
{
    public class AccessExpressionToSQL: ExpressionToSQL
    {
        public AccessExpressionToSQL()
        {
            MemberStartQuote = '[';
            MemberEndQuote = ']';

            Converters.Add(new BinaryExpressionToSQLConverter());
            Converters.Add(new ConstantExpressionToSQLConverter());
            Converters.Add(new MethodCallExpressionToSQLConverter());
            Converters.Add(new UnaryExpressionToSQLConverter());
            Converters.Add(new MemberExpressionToSQLConverter());

            var ch = typeof(IAccessExpressionToSQLConverter);
            var typs = Assembly.GetExecutingAssembly().GetTypes().Where(x => ch.IsAssignableFrom(x)).ToList();
            foreach (var t in typs)
            {
                try
                {
                    var o = Activator.CreateInstance(t) as IAccessExpressionToSQLConverter;
                    if (o != null)
                        Converters.Add(o);
                }
                catch { }
            }
        }
    }

    /// <summary>
    /// If you implement this interface, SQLServerExpressionToSQL will automatically load it
    /// </summary>
    public interface IAccessExpressionToSQLConverter : IExpressionToSQLConverter { }
}
