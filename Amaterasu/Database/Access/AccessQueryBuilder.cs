﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Access
{
    public class AccessQueryBuilder<T> : QueryBuilder<T> where T : class, new()
    {
        public override SQLWithParameter BuildSQL(bool clear = true)
        {
            PrepareForBuild();
            var whr = LINQ2SQL.ToWhereClause(QueryExpressionNode);

            var sql = "SELECT " + BuildLimit() + BuildSelect() + " FROM " + LINQ2SQL.Quote(Source) +
                (string.IsNullOrEmpty(whr?.Where) ? "" : " WHERE " + whr.Where)
                 + BuildSort();

            var res = new SQLWithParameter();
            res.SQL = sql;
            if (whr?.Parameters?.Count > 0)
            {
                res.Parameters = whr.Parameters;
            }
            if (clear) Clear();

            return res;
        }

        public override string BuildLimit()
        {
            if (Limit.Take > 0)
            {
                if (Limit.Skip > 0)
                {
                    throw new Exception("OFFSET is not supported in MS Access");
                }
                return "TOP " + Limit.Take + " ";
            }
            return "";
        }
    }
}
