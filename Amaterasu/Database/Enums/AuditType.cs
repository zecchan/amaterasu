﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public enum AuditType
    {
        Insert,
        Update,
        Delete,
    }
}
