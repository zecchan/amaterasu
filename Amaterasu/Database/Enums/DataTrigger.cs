﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Enums
{
    public enum DataTrigger
    {
        PreSave = 1,
        PostSave = 2,

        PreDelete = 4,
        PostDelete = 8,
    }
}
