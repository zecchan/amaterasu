﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Enums
{
    public enum DatabaseColumnDataType
    {
        Boolean,

        Byte,
        Short,  
        Integer,
        Long,

        Float,
        Double,
        Decimal,

        String,
        Text,

        Json,
    }
}
