﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.MySql
{
    public class MySqlQueryBuilder<T> : QueryBuilder<T> where T : class, new()
    {
        public MySqlQueryBuilder()
        {
            Parser = new Expressions.ExpressionParser();
            Parser.LoadAvailableIExpressionParsers();

            LINQ2SQL = new ExpressionToSQL();
            LINQ2SQL.LoadAvailableIExpressionToSQLConverter();
        }
    }
}
