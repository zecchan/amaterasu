﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public static class Generator
    {
        public static string GenerateId(bool includeDate = true, bool includeTime = true, int guidCount = 1)
        {
            var id = Guid.NewGuid().ToString("n");
            for(var i = 1; i < guidCount; i++)
            {
                id += Guid.NewGuid().ToString("n");
            }
            var prefix = "";
            if (includeDate)
            {
                prefix += DateTime.Now.ToString("yyyyMMdd");
            }
            if (includeTime)
            {
                prefix += DateTime.Now.ToString("hhmmss");
            }
            if (prefix != "")
            {
                prefix += "_";
            }
            return prefix + id;
        }

        public static class CharacterSets
        {
            public const string AlphaLowercase = "abcdefghijklmnopqrstuvwxyz";

            public const string AlphaUppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            public const string Alphabet = AlphaLowercase + AlphaUppercase;

            public const string Number = "0123456789";

            public const string Alphanumeric = Alphabet + Number;
        }

        public static string GenerateString(int length, string charset = CharacterSets.Alphanumeric)
        {
            var res = "";
            var rng = new Random();
            for(var i = 0; i < length; i++)
            {
                res += charset[rng.Next(charset.Length)];
            }
            return res;
        }
    }
}
