﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseAutoReconnect
    {
        bool AutoReconnect { get; set; }
    }
}
