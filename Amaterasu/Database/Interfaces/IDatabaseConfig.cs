﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseConfig
    {
        string Engine { get; set; }

        IDatabaseCredential Credential { get; set; }
    }
}
