﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    /// <summary>
    /// Exposes the OnPreSave method to database context.
    /// </summary>
    public interface IPreSave
    {
        /// <summary>
        /// This method will be executed before save (insert or update). Any exception thrown inside this method will halt the save process.
        /// </summary>
        /// <param name="db">The database context that execute the save</param>
        /// <param name="isUpdate">Indicates this operation is an update operation or not</param>
        void OnPreSave(IDatabaseContext db, bool isUpdate);
    }
}
