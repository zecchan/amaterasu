﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface ISqlQueryBuilder<T>: IQueryBuilder<T>
    {
        /// <summary>
        /// Gets or sets the parser used to parse linq expression.
        /// </summary>
        ExpressionParser Parser { get; set; }

        /// <summary>
        /// Gets or sets the expression to SQL converter
        /// </summary>
        ExpressionToSQL LINQ2SQL { get; set; }
    }
}
