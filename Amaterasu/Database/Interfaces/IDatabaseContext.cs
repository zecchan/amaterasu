﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Database
{
    /// <summary>
    /// Exposes the database context method and properties
    /// </summary>
    public interface IDatabaseContext: IDatabaseCommands
    {
        /// <summary>
        /// Gets the database credential
        /// </summary>
        IDatabaseCredential Credential { get; }

        /// <summary>
        /// Truncates source.
        /// </summary>
        /// <typeparam name="T">Model type</typeparam>
        void Truncate<T>() where T : class, new();

        /// <summary>
        /// Truncates custom source.
        /// </summary>
        /// <param name="source">The name of the custom source</param>
        void Truncate(string source);

        /// <summary>
        /// Opens the connection using <see cref="Credential"/>
        /// </summary>
        void Open();

        /// <summary>
        /// Opens the connection using new <see cref="IDatabaseCredential"/>
        /// </summary>
        /// <param name="credential"></param>
        void Open(IDatabaseCredential credential);

        /// <summary>
        /// Closes the connection
        /// </summary>
        void Close();

        /// <summary>
        /// Gets the data processing triggers
        /// </summary>
        List<DataProcessTrigger> DataProcessTriggers { get; }
    }
}
