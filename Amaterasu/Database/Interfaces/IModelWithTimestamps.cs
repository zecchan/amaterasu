﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IModelWithTimestamps
    {
        /// <summary>
        /// Gets or sets creation timestamp.
        /// </summary>
        DateTime CreatedAt { get; set; }

        /// <summary>
        /// Gets or sets last update timestamp.
        /// </summary>
        DateTime LastUpdatedAt { get; set;}
    }
}
