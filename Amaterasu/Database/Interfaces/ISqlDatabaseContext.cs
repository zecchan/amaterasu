﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Amaterasu.Database.Core
{
    public interface ISqlDatabaseContext: IDatabaseContext
    {

        /// <summary>
        /// Gets the expression parser
        /// </summary>
        ExpressionParser ExpressionParser { get; }

        /// <summary>
        /// Gets the expression to SQL converter
        /// </summary>
        ExpressionToSQL ExpressionToSQL { get; }

        /// <summary>
        /// Executes a query and map it to a model. Throws an exception when failed.
        /// </summary>
        /// <typeparam name="T">The model type</typeparam>
        /// <param name="query">The query to be executed</param>
        /// <param name="parameters">The parameter values of the query</param>
        /// <returns>An enumerable containing the query result</returns>
        IEnumerator<T> ExecuteQueryEnumerator<T>(string query, Dictionary<string, object> parameters = null) where T : class, new();

        /// <summary>
        /// Executes a query and map it to a model. Throws an exception when failed.
        /// </summary>
        /// <typeparam name="T">The model type</typeparam>
        /// <param name="query">The query to be executed</param>
        /// <param name="parameters">The parameter values of the query</param>
        /// <returns>An enumerable containing the query result</returns>
        IEnumerable<T> ExecuteQuery<T>(string query, Dictionary<string, object> parameters = null) where T : class, new();

        /// <summary>
        /// Executes a query and execute an action. Throws an exception when failed.
        /// </summary>
        /// <param name="query">The query to be executed</param>
        /// <param name="action">The action that will be executed after the data is read</param>
        /// <param name="parameters">The parameter values of the query</param>
        /// <returns>An enumerable containing the query result</returns>
        void ExecuteQuery(string query, Action<IDataReader> action, Dictionary<string, object> parameters = null);

        /// <summary>
        /// Executes a query and returns a data reader.
        /// </summary>
        /// <param name="query">The query to be executed</param>
        /// <param name="parameters">The parameter values of the query</param>
        /// <returns>IDataReader</returns>
        IDataReader ExecuteReader(string query, Dictionary<string, object> parameters = null);

        /// <summary>
        /// Executs a non-query. Throws an exception when failed.
        /// </summary>
        /// <param name="sql">The sql statement to be executed</param>
        /// <param name="parameters">The parameter values of the sql statement</param>
        /// <returns>Number of rows affected</returns>
        int ExecuteNonQuery(string sql, Dictionary<string, object> parameters = null);
    }
}
