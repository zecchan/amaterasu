﻿using Amaterasu.Database.Core;
using Amaterasu.Expressions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Database
{
    /// <summary>
    /// Contains method for querying a model.
    /// </summary>
    public interface IQueryBuilder<T> : IEnumerable<T>
    {

        /// <summary>
        /// Gets or sets the database context
        /// </summary>
        IDatabaseContext DatabaseContext { get; set; }

        string Source { get; set; }

        QueryLimit Limit { get; }

        List<QueryMember> Fields { get; }

        List<QuerySort> Sorts { get; }

        IQueryBuilder<T> Where(Expression<Func<T, bool>> expression);

        IQueryBuilder<T> OrWhere(Expression<Func<T, bool>> expression);

        IQueryBuilder<T> Skip(int skip);

        IQueryBuilder<T> Take(int take);

        IQueryBuilder<T> OrderBy(Expression<Func<T, object>> expression, QuerySortDirection direction = QuerySortDirection.Ascending);

        IQueryBuilder<T> Select(params Expression<Func<T, object>>[] expression);

        int Count(bool clear = true);

        T First();

        T FirstOrDefault();

        T Last();

        T LastOrDefault();

        IQueryBuilder<T> GroupStart();

        IQueryBuilder<T> OrGroupStart();

        IQueryBuilder<T> GroupEnd();

        SQLWithParameter BuildSQL(bool clear = true);

        void Clear();
    }


    public class ExpressionNodeStackRecord
    {
        public ExpressionNode Node { get; set; }

        public ExpressionType NodeType { get; set; } = ExpressionType.AndAlso;

        public ExpressionNodeStackRecord(ExpressionNode node, ExpressionType type = ExpressionType.AndAlso)
        {
            Node = node;
            NodeType = type;
        }
    }

    public class QueryLimit
    {
        public int Skip { get; set; } = 0;

        public int Take { get; set; } = 0;

        public void Clear()
        {
            Skip = 0;
            Take = 0;
        }
    }

    public class QuerySort
    {
        public string Name { get; set; }
        public QuerySortDirection SortDirection { get; set; }
    }

    public class QueryMember
    {
        public string Name { get; set; }

        public string As { get; set; }
    }

    public enum QuerySortDirection
    {
        Ascending,
        Descending
    }

    public class SQLWithParameter
    {
        public string SQL { get; set; }

        public Dictionary<string, object> Parameters { get; set; }
    }
}
