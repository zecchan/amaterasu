﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Amaterasu.Database.Interfaces
{
    public interface IDatabaseContextWithConnectionProperty
    {
        /// <summary>
        /// Gets the database connection
        /// </summary>
        IDbConnection Connection { get; }
    }
}
