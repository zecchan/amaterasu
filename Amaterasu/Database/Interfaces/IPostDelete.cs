﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{

    /// <summary>
    /// Exposes the OnPostDelete method to database context.
    /// </summary>
    public interface IPostDelete
    {
        /// <summary>
        /// This method will be executed after delete.
        /// </summary>
        /// <param name="db">The database context that execute the delete</param>
        void OnPostDelete(IDatabaseContext db);
    }
}
