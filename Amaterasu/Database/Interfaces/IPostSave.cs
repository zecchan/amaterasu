﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    /// <summary>
    /// Exposes the OnPostSave method to database context.
    /// </summary>
    public interface IPostSave
    {
        /// <summary>
        /// This method will be executed after save (insert or update)
        /// </summary>
        /// <param name="db">The database context that execute the save</param>
        /// <param name="isUpdate">Indicates this operation is an update operation or not</param>
        /// <param name="insertId">The insert id, if available. Null if not available or is an update.</param>
        void OnPostSave(IDatabaseContext db, bool isUpdate, object insertId);
    }
}
