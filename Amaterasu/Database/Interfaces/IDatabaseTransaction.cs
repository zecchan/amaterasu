﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseTransaction
    {
        /// <summary>
        /// Commits the current database transaction.
        /// </summary>
        void Commit();

        /// <summary>
        /// Rollbacks the current database transaction.
        /// </summary>
        void Rollback();
    }
}
