﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseCredential
    {
        /// <summary>
        /// Gets or sets the host or data source where the database resides
        /// </summary>
        string Host { get; set; }

        /// <summary>
        /// Gets or sets the port pf database service. When null, default port value is used
        /// </summary>
        int? Port { get; set; }

        /// <summary>
        /// Gets or sets the username to access the database
        /// </summary>
        string User { get; set; }

        /// <summary>
        /// Gets or sets the password to access the database
        /// </summary>
        string Password { get; set; }

        /// <summary>
        /// Gets or sets the database name to connect to
        /// </summary>
        string Database { get; set; }

        /// <summary>
        /// Literal connection string, if not null then this will be used instead of described credential
        /// </summary>
        string ConnectionString { get; set; }
    }
}
