﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseManager
    {
        /// <summary>
        /// Gets a list of databases
        /// </summary>
        Dictionary<string, IDatabaseContext> Databases { get; }

        /// <summary>
        /// Gets the default database
        /// </summary>
        IDatabaseContext Default { get; }

        IDatabaseContext First();

        T First<T>() where T : class, IDatabaseContext;

        T Get<T>(string key) where T : class, IDatabaseContext;

        IDatabaseContext Get(string key);

        void Add(string key, IDatabaseContext db);
    }
}
