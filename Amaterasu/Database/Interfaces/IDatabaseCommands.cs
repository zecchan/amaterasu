﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseCommands
    {
        /// <summary>
        /// Gets a query builder from a model's default source.
        /// </summary>
        /// <typeparam name="T">The model type</typeparam>
        /// <returns>Query builder</returns>
        IQueryBuilder<T> From<T>() where T : class, new();

        /// <summary>
        /// Gets a query builder from a custom source.
        /// </summary>
        /// <typeparam name="T">The model type</typeparam>
        /// <param name="source">The custom source where the data is fetched from</param>
        /// <returns>Query builder</returns>
        IQueryBuilder<T> From<T>(string source) where T : class, new();

        /// <summary>
        /// Checks whether data with the same id existed or not.
        /// </summary>
        bool Exists<T>(T data) where T : class, new();

        /// <summary>
        /// Save a data model. Automatically execute insert/update.
        /// </summary>
        /// <param name="data">The data model</param>
        void Save<T>(T data) where T : class, new();

        /// <summary>
        /// Insert a data model. Throws an exception if constraint failed.
        /// </summary>
        /// <param name="data">The data model</param>
        void Insert<T>(T data) where T : class, new();

        /// <summary>
        /// Update a data model. Throws an exception if constraint failed.
        /// </summary>
        /// <param name="data">The data model</param>
        void Update<T>(T data)where T : class, new();

        /// <summary>
        /// Updates data by setting certain fields. Throws an exception if constraint failed. This does not trigger pre/post save.
        /// </summary>
        /// <param name="data">The data model</param>
        void UpdateWhere<T>(Dictionary<string, object> set, Expression<Func<T, bool>> expression, string source = null) where T : class, new();

        /// <summary>
        /// Deletes a data model. Throws an exception if constraint failed.
        /// </summary>
        /// <param name="data"></param>
        void Delete<T>(T data)where T : class, new();

        /// <summary>
        /// Deletes a data model where the field matches the value. This does not trigger pre/post delete.
        /// </summary>
        /// <param name="fieldName">The field name to check</param>
        /// <param name="value">The value to check</param>
        /// <returns>The number or rows deleted.</returns>
        int DeleteWhere<T>(string fieldName, object value) where T : class, new();

        /// <summary>
        /// Deletes data from source matching the query. This does not trigger pre/post delete.
        /// </summary>
        /// <param name="expression">The filter expression</param>
        int DeleteWhere<T>(Expression<Func<T, bool>> expression) where T : class, new();

        /// <summary>
        /// Perform a database transaction. Throw an exception inside the action to rollback the whole transaction.
        /// </summary>
        /// <param name="transaction">The action to perform</param>
        void Transaction(Action<IDatabaseContext> transaction, IsolationLevel? isolationLevel = null);
    }
}
