﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{

    /// <summary>
    /// Exposes the OnPreDelete method to database context.
    /// </summary>
    public interface IPreDelete
    {
        /// <summary>
        /// This method will be executed before delete. Any exception thrown inside this method will halt the delete process.
        /// </summary>
        /// <param name="db">The database context that execute the delete</param>
        void OnPreDelete(IDatabaseContext db);
    }
}
