﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Amaterasu.Database
{
    public class ExpressionToSQL
    {
        public char MemberStartQuote = '`';

        public char MemberEndQuote = '`';

        public List<IExpressionToSQLConverter> Converters { get; } = new List<IExpressionToSQLConverter>();

        public void LoadAvailableIExpressionToSQLConverter()
        {
            var ch = typeof(IExpressionToSQLConverter);
            var typs = Assembly.GetExecutingAssembly().GetTypes().Where(x => ch.IsAssignableFrom(x)).ToList();
            foreach (var t in typs)
            {
                try
                {
                    var o = Activator.CreateInstance(t) as IExpressionToSQLConverter;
                    if (o != null)
                        Converters.Add(o);
                }
                catch { }
            }
        }
        public virtual SQLWhereClause ToWhereClause(ExpressionNode node)
        {
            var res = new SQLWhereClause();
            if (node?.Type == ExpressionOperationType.Lambda)
                node = node.Body;
            res.Where = node == null ? null : Convert(node, res);
            return res;
        }

        public string Quote(string name)
        {
            name = name?.Trim() ?? "";

            Regex regex = new Regex("^[a-zA-Z0-9]+$");
            if (!regex.IsMatch(name))
                return name;

            var res = "";
            if (MemberStartQuote != '\0')
                res += MemberStartQuote;
            res += name;
            if (MemberEndQuote != '\0')
                res += MemberEndQuote;
            return res;
        }

        public virtual string ToSelectField(QueryMember member)
        {
            if (member.Name != null)
            {
                return member.Name + (member.As != null ? " AS " + Quote(member.As) : "");
            }
            throw new NotImplementedException();
        }

        public virtual string Convert(ExpressionNode currentNode, SQLWhereClause clause)
        {
            var cs = Converters.Where(x => x.Accepts(currentNode)).ToList();
            cs.Reverse();
            foreach (var c in cs)
            {
                var re = c.Convert(currentNode, clause, this);
                if (re != null)
                    return re;
            }
            throw new Exception("Cannot parse ExpressionNode of type [" + currentNode.Type + "]");
        }
    }

    public interface IExpressionToSQLConverter
    {
        bool Accepts(ExpressionNode node);

        string Convert(ExpressionNode node, SQLWhereClause clause, ExpressionToSQL root);
    }

    /// <summary>
    /// Describes an SQL query where clause
    /// </summary>
    public class SQLWhereClause
    {
        /// <summary>
        /// Gets or sets the SQL Where clause
        /// </summary>
        public string Where { get; set; }

        /// <summary>
        /// Gets or sets a list of parameter to be passed
        /// </summary>
        public Dictionary<string, object> Parameters { get; } = new Dictionary<string, object>();

        private int Counter { get; set; } = 0;

        /// <summary>
        /// Creates a new parameter with specified value
        /// </summary>
        /// <param name="value">The parameter value</param>
        /// <returns>Parameter name prefixed with @</returns>
        public string NewParameter(object value)
        {
            var nm = "@par" + Counter.ToString().PadLeft(3, '0');
            Counter++;
            Parameters.Add(nm, value);
            return nm;
        }
    }
}
