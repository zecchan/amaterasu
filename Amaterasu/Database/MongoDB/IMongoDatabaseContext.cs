﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.MongoDB
{
    public interface IMongoDatabaseContext: IDatabaseContext
    {
        string GetCollectionNameFromType<T>(string defaultName = null) where T : class, new();
    }
}
