﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public class DatabaseTableConstructor
    {
        /// <summary>
        /// Gets the name of the table
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the database context
        /// </summary>
        public IDatabaseContext Database { get; }

        /// <summary>
        /// Gets the constructor mode
        /// </summary>
        DatabaseConstructorMode Mode { get; }

        public DatabaseTableConstructor(string name, IDatabaseContext database, DatabaseConstructorMode mode)
        {
            Name = name;
            Database = database;
            Mode = mode;
        }
    }
}
