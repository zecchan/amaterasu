﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public class MemberExpressionToSQLConverter : IExpressionToSQLConverter
    {
        public bool Accepts(ExpressionNode node)
        {
            return node.Type == ExpressionOperationType.Member;
        }

        public string Convert(ExpressionNode node, SQLWhereClause clause, ExpressionToSQL root)
        {
            string inner;
            if (node.Accessor.Type == ExpressionOperationType.Parameter)
            {
                inner = root.Quote(node.MemberName);
            }
            else
            {
                inner = root.Convert(node.Accessor, clause) + "." + root.Quote(node.MemberName);
            }
            return inner;
        }
    }
}
