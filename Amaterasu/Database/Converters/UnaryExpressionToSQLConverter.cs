﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public class UnaryExpressionToSQLConverter : IExpressionToSQLConverter
    {
        public bool Accepts(ExpressionNode node)
        {
            return node.Type == ExpressionOperationType.Unary;
        }

        public string Convert(ExpressionNode node, SQLWhereClause clause, ExpressionToSQL root)
        {
            if (node.NodeType == System.Linq.Expressions.ExpressionType.Not)
            {
                if (node.Body.Type == ExpressionOperationType.Constant && (node.Body.ReturnType == typeof(bool) || node.Body.ValueType == typeof(bool)))
                    return root.Convert(node.Body, clause) + " = 0";

                return "NOT (" + root.Convert(node.Body, clause) + ")";
            }
            if (node.NodeType == System.Linq.Expressions.ExpressionType.Convert)
            {
                return root.Convert(node.Body, clause);
            }
            throw new Exception("Unsupported unary operator [" + node.NodeType + "]");
        }
    }
}
