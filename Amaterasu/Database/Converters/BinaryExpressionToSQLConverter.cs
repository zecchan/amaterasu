﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Database
{
    public class BinaryExpressionToSQLConverter : IExpressionToSQLConverter
    {
        public bool Accepts(ExpressionNode node)
        {
            return node.Type == ExpressionOperationType.Binary;
        }

        public string Convert(ExpressionNode node, SQLWhereClause clause, ExpressionToSQL root)
        {
            var op = new Dictionary<ExpressionType, string>()
            {
                { ExpressionType.And, "AND" },
                { ExpressionType.AndAlso, "AND" },
                { ExpressionType.Or, "OR" },
                { ExpressionType.OrElse, "OR" },
                { ExpressionType.Equal, "=" },
                { ExpressionType.NotEqual, "<>" },
                { ExpressionType.GreaterThan, ">" },
                { ExpressionType.GreaterThanOrEqual, ">=" },
                { ExpressionType.LessThan, "<" },
                { ExpressionType.LessThanOrEqual, "<=" },
            };
            var opr = "AND";
            if (op.ContainsKey(node.NodeType))
                opr = op[node.NodeType];

            if (node.NodeType == ExpressionType.NotEqual)
            {
                if (node.Right.Type == ExpressionOperationType.Constant && node.Right.ConstantValue == null)
                    return "(" + root.Convert(node.Left, clause) + " IS NOT NULL)";
                if (node.Left.Type == ExpressionOperationType.Constant && node.Left.ConstantValue == null)
                    return "(" + root.Convert(node.Right, clause) + " IS NOT NULL)";
            }
            if (node.NodeType == ExpressionType.Equal)
            {
                if (node.Right.Type == ExpressionOperationType.Constant && node.Right.ConstantValue == null)
                    return "(" + root.Convert(node.Left, clause) + " IS NULL)";
                if (node.Left.Type == ExpressionOperationType.Constant && node.Left.ConstantValue == null)
                    return "(" + root.Convert(node.Right, clause) + " IS NULL)";
            }

            string left, right;
            if (node.Left.Type == ExpressionOperationType.Member && node.Left.ReturnType == typeof(bool))
            {                    
                left = root.Convert(node.Left, clause) + " = 1";
            }
            else
                left = root.Convert(node.Left, clause);
            if (node.Right.Type == ExpressionOperationType.Member && node.Right.ReturnType == typeof(bool))
            {
                right = root.Convert(node.Right, clause) + " = 1";
            }
            else right = root.Convert(node.Right, clause);

            return "(" + left + " " + opr + " " + right + ")";
        }
    }
}
