﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public class ConstantExpressionToSQLConverter : IExpressionToSQLConverter
    {
        public bool Accepts(ExpressionNode node)
        {
            return node.Type == ExpressionOperationType.Constant;
        }

        public string Convert(ExpressionNode node, SQLWhereClause clause, ExpressionToSQL root)
        {
            return clause.NewParameter(node.ConstantValue);
        }
    }
}
