﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Database
{
    public class MethodCallExpressionToSQLConverter : IExpressionToSQLConverter
    {
        public bool Accepts(ExpressionNode node)
        {
            var supported = new string[]
            {
                "trim",
                "tolower",
                "tolowerinvariant",
                "toupper",
                "toupperinvariant",
                "contains",
                "endswith",
                "startswith"
            };
            return node.Type == ExpressionOperationType.MethodCall && (
                node.Accessor.ValueType == typeof(string) || node.Accessor.ReturnType == typeof(string)
                ) && supported.Contains(node.MemberName?.ToLower());
        }

        public string Convert(ExpressionNode node, SQLWhereClause clause, ExpressionToSQL root)
        {
            var inner = root.Convert(node.Accessor, clause);

            var mname = node.MemberName?.ToLower();
            if (mname == "trim")
                return "TRIM(" + inner + ")";
            if (mname == "tolower" || mname == "tolowerinvariant")
                return "LOWER(" + inner + ")";
            if (mname == "toupper" || mname == "toupperinvariant")
                return "UPPER(" + inner + ")";
            if (mname == "contains")
            {
                var arg = node.Arguments[0];
                if (arg.Type == ExpressionOperationType.Constant && arg.ValueType == typeof(string))
                {
                    arg.ConstantValue = "%" + arg.ConstantValue + "%";
                    return inner + " LIKE " + root.Convert(arg, clause);
                } else
                {
                    return inner + " LIKE CONCAT('%'," + root.Convert(arg, clause) + ",'%')";
                }
            }

            return inner;
        }
    }
}
