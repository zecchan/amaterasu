﻿using Amaterasu.Database.Attributes;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Amaterasu.Database
{
    public class DatabaseManager : IDatabaseManager
    {
        private const string DatabaseConfigSectionKey = "Databases";

        public Dictionary<string, IDatabaseContext> Databases { get; } = new Dictionary<string, IDatabaseContext>();

        /// <summary>
        /// Gets the default database
        /// </summary>
        public IDatabaseContext Default
        {
            get
            {
                if (Databases.ContainsKey("default"))
                {
                    return Databases["default"];
                }
                if (Databases.Count > 0)
                    return Databases.First().Value;
                return null;
            }
        }

        /// <summary>
        /// Gets a specific database
        /// </summary>
        /// <param name="key">Database name</param>
        /// <returns>IDatabaseContext</returns>
        public IDatabaseContext this[string key]
        {
            get
            {
                return Databases[key];
            }
        }

        public T Get<T>(string key) where T : class, IDatabaseContext
        {
            return Databases[key] as T;
        }

        public IDatabaseContext Get(string key)
        {
            return Databases[key];
        }

        public void Add(string key, IDatabaseContext db)
        {
            Databases[key] = db;
        }

        public IDatabaseContext First()
        {
            return Databases.Values.FirstOrDefault();
        }

        public T First<T>() where T : class, IDatabaseContext
        {
            var tt = typeof(T);
            return Databases.Values.Where(x => tt.IsAssignableFrom(x?.GetType())).FirstOrDefault() as T;
        }

        public void LoadFromConfig(IConfiguration configuration, string sectionKey = DatabaseConfigSectionKey)
        {
            var section = configuration.GetSection(sectionKey);
            if (section == null)
            {
                throw new Exception("Database configuration section not found");
            }
            LoadFromConfig(section);
        }

        public void LoadFromConfig(IConfigurationSection databaseConfiguration)
        {
            foreach (var db in databaseConfiguration.GetChildren())
            {
                var id = db.Key;
                var dbc = databaseConfiguration.GetSection(id);
                var dic = new Dictionary<string, string>();
                foreach (var kv in dbc.GetChildren())
                {
                    if (!string.IsNullOrWhiteSpace(kv.Value))
                        dic.Add(kv.Key, kv.Value);
                }
                if (dic.Any(x => x.Key.ToLower() == "engine"))
                {
                    var engineId = dic.First(x => x.Key.ToLower() == "engine").Value;
                    var dbEngine = GetEngineTypeFromId(engineId);
                    var dbConfig = GetConfigurationTypeFromId(engineId);
                    if (dbEngine != null)
                    {
                        var cred = CreateDatabaseCredential(dbConfig, dic);
                        LoadDatabase(id, dbEngine, cred);
                    }
                }
            }
        }

        public void LoadDatabase(string id, Type databaseEngineType, IDatabaseCredential credential)
        {
            var instance = Activator.CreateInstance(databaseEngineType) as IDatabaseContext;
            if (instance != null)
            {
                instance.Open(credential);
                Add(id, instance);
            }
        }

        public IDatabaseCredential CreateDatabaseCredential(Type? databaseConfigType, Dictionary<string, string> values)
        {
            IDatabaseCredential? databaseCredential = null;
            if (databaseConfigType != null)
            {
                databaseCredential = Activator.CreateInstance(databaseConfigType) as IDatabaseCredential;
            }
            if (databaseCredential == null)
            {
                databaseCredential = new DatabaseCredential();
            }
            var dcredType = databaseCredential.GetType();
            var propertyAliases = new Dictionary<string, string[]>()
                    {
                        { "Host", new string[] { "DataSource", "Src", "Source" } },
                        { "Port", new string[] { "Prt" } },
                        { "User", new string[] { "Usr", "Username" } },
                        { "Password", new string[] { "Pwd", "Pass" } },
                        { "Database", new string[] { "Name", "DBName", "DB" } },
                        { "ConnectionString", new string[] { "ConStr", "CString", "ConString", "CS" } },
                    };

            foreach (var dctProp in dcredType.GetProperties())
            {
                if (!dctProp.CanWrite) continue;
                var aliases = propertyAliases.ContainsKey(dctProp.Name) ? propertyAliases[dctProp.Name] : new string[0];
                KeyValuePair<string, string>? sel =
                    values.Any(x => x.Key.ToLower() == dctProp.Name.ToLower()) ? values.Where(x => x.Key.ToLower() == dctProp.Name.ToLower()).FirstOrDefault()
                    : values.Any(x => aliases.Any(w => w.ToLower() == x.Key.ToLower())) ? values.Where(x => aliases.Any(w => w.ToLower() == x.Key.ToLower())).FirstOrDefault()
                    : null;
                if (sel != null)
                {
                    var val = sel.Value.Value;
                    if (dctProp.PropertyType == typeof(int))
                    {
                        int intval;
                        if (int.TryParse(val, out intval))
                            dctProp.SetValue(databaseCredential, intval);
                    }
                    else if (dctProp.PropertyType == typeof(string))
                    {
                        dctProp.SetValue(databaseCredential, val);
                    }
                }
            }
            return databaseCredential;
        }

        public Type? GetEngineTypeFromId(string id)
        {
            LoadEngines();
            if (RegisteredEngines.ContainsKey(id))
            {
                var engine = RegisteredEngines[id];
                return engine.Item1;
            }
            return null;
        }

        public Type? GetConfigurationTypeFromId(string id)
        {
            LoadEngines();
            if (RegisteredEngines.ContainsKey(id))
            {
                var engine = RegisteredEngines[id];
                return engine.Item2;
            }
            return null;
        }

        static bool _engineLoaded = false;
        private static void LoadEngines()
        {
            if (_engineLoaded) return;
            _engineLoaded = true;
            var dbcType = typeof(IDatabaseContext);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var dbcTypes = assemblies.SelectMany(x => x.GetTypes().Where(t => t.IsClass && t.IsVisible && !t.IsGenericType && !t.IsAbstract && dbcType.IsAssignableFrom(t))).ToList();

            foreach (var dbct in dbcTypes)
            {
                var attr = dbct.GetCustomAttribute<DatabaseCredentialClassAttribute>(false);
                if (attr != null && !string.IsNullOrWhiteSpace(attr.EngineId))
                {
                    if (!RegisteredEngines.ContainsKey(attr.EngineId))
                        RegisteredEngines.Add(attr.EngineId, new Tuple<Type, Type>(dbct, attr.ConfigurationClass));
                }
            }
        }

        public static void LoadEngine<TDatabaseContext>() where TDatabaseContext: class, IDatabaseContext
        {
            var dbct = typeof(TDatabaseContext);
            var attr = dbct.GetCustomAttribute<DatabaseCredentialClassAttribute>(false);
            if (attr != null && !string.IsNullOrWhiteSpace(attr.EngineId))
            {
                if (!RegisteredEngines.ContainsKey(attr.EngineId))
                    RegisteredEngines.Add(attr.EngineId, new Tuple<Type, Type>(dbct, attr.ConfigurationClass));
            }
        }

        private static Dictionary<string, Tuple<Type, Type>> RegisteredEngines { get; } = new Dictionary<string, Tuple<Type, Type>>();

        public static void RegisterEngine<T>() where T : class, IDatabaseContext
        {
            var dbct = typeof(T);
            var attr = dbct.GetCustomAttribute<DatabaseCredentialClassAttribute>(false);
            if (attr != null && !string.IsNullOrWhiteSpace(attr.EngineId))
            {
                if (!RegisteredEngines.ContainsKey(attr.EngineId))
                    RegisteredEngines.Add(attr.EngineId, new Tuple<Type, Type>(dbct, attr.ConfigurationClass));
            }
        }
    }
}
