﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.SQLServer
{
    public class SQLServerQueryBuilder<T> : QueryBuilder<T> where T : class, new()
    {
        public override SQLWithParameter BuildSQL(bool clear = true)
        {
            PrepareForBuild();
            var whr = LINQ2SQL.ToWhereClause(QueryExpressionNode);

            var sql = "SELECT " + (Limit.Skip <= 0 && Limit.Take > 0 ? BuildLimit() + " " : "") + BuildSelect() + " FROM " + LINQ2SQL.Quote(Source) +
                (string.IsNullOrEmpty(whr?.Where) ? "" : " WHERE " + whr.Where)
                 + BuildSort() + (Limit.Skip > 0 && Limit.Take > 0 ? " " + BuildLimit() : "");

            var res = new SQLWithParameter();
            res.SQL = sql;
            if (whr?.Parameters?.Count > 0)
            {
                res.Parameters = whr.Parameters;
            }
            if (clear) Clear();

            return res;
        }

        public override string BuildLimit()
        {
            if (Limit.Take > 0)
            {
                if (Limit.Skip > 0)
                {
                    return "OFFSET " + Limit.Skip + " ROWS FETCH NEXT " + Limit.Take + " ROWS ONLY";
                }
                else
                    return "TOP " + Limit.Take;
            }
            return "";
        }
    }
}
