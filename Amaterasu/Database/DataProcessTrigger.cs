﻿using Amaterasu.Database.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public class DataProcessTrigger
    {
        public DataTrigger Trigger { get; set; }

        public Action<object, IDatabaseContext> Action { get; set; }
    }
}
