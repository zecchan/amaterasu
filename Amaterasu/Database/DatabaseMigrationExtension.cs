﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public static class DatabaseMigrationExtension
    {
        public static DatabaseTableConstructor CreateTable(this IDatabaseMigration migration, string tableName, IDatabaseContext databaseContext)
        {
            return new DatabaseTableConstructor(tableName, databaseContext, DatabaseConstructorMode.Create);
        }
        public static DatabaseTableConstructor AlterTable(this IDatabaseMigration migration, string tableName, IDatabaseContext databaseContext)
        {
            return new DatabaseTableConstructor(tableName, databaseContext, DatabaseConstructorMode.Alter);
        }
    }
}
