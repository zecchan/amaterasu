﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Amaterasu.Database.Validation
{
    /// <summary>
    /// Static class to validate a model.
    /// </summary>
    public static class ModelValidation
    {
        /// <summary>
        /// Validates a model.
        /// </summary>
        /// <param name="data">The model instance to check.</param>
        public static ValidationResult Validate<T>(T data)
        {
            return Validate(typeof(T), data);
        }

        /// <summary>
        /// Validates a model.
        /// </summary>
        /// <param name="data">The model instance to check.</param>
        /// <param name="type">The type of the model.</param>
        public static ValidationResult Validate(Type type, object data)
        {
            return _validate(data, type);
        }

        private static ValidationResult _validate(object data, Type type, string node = "")
        {
            if (data == null) return new ValidationResult(null);

            var props = type.GetProperties().Where(x => x.CanRead);
            var errors = new List<FieldValidationResult>();

            foreach (var p in props)
            {
                var hasError = false;
                var validations = p.GetCustomAttributes<ValidateAttribute>(true);
                foreach (var v in validations)
                {
                    var validator = Activator.CreateInstance(v.ValidatorType, v.Parameters) as IFieldValidator;
                    if (validator != null)
                    {
                        var res = validator.Validate(p.GetValue(data));
                        if (!res.Valid)
                        {
                            res.FieldName = p.Name;
                            res.Node = node;
                            errors.Add(res);
                            hasError = true;
                            break;
                        }
                    }
                }

                var vInner = p.GetCustomAttribute<ValidateInnerAttribute>();
                if (vInner != null && !hasError)
                {
                    var o = p.GetValue(data);
                    if (o != null)
                    {
                        if (o is IEnumerable enumerable)
                        {
                            var index = 0;
                            foreach(var mem in enumerable)
                            {
                                var typ = mem.GetType();
                                var r = _validate(mem, typ, node + p.Name + "[" + index + "].");
                                errors.AddRange(r.Errors);
                            }
                        }
                        else
                        {
                            var r = _validate(o, p.PropertyType, node + p.Name + ".");
                            errors.AddRange(r.Errors);
                        }
                    }
                }
            }

            return new ValidationResult(errors);
        }
    }
}
