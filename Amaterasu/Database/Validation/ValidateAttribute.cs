﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Validation
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class ValidateAttribute: Attribute
    {
        public Type ValidatorType { get; set; }

        public object[] Parameters { get; set; }

        public ValidateAttribute(Type validatorType, params object[] parameters)
        {
            ValidatorType = validatorType;
            Parameters = parameters;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ValidateInnerAttribute : Attribute
    {

    }
}
