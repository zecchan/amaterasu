﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public class RequiredValidator : IFieldValidator<object>
    {
        public FieldValidationResult Validate(object data)
        {
            if (data == null)
                return FieldValidationResult.Fail("validation.required.fail", "{field} cannot be null");
            if (data is string str && string.IsNullOrWhiteSpace(str)) 
                return FieldValidationResult.Fail("validation.required.fail", "{field} cannot be empty");
            return FieldValidationResult.Success();
        }
    }
}
