﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public class NotNullValidator : IFieldValidator<object>
    {
        public FieldValidationResult Validate(object data)
        {
            if (data == null)
                return FieldValidationResult.Fail("validation.notnull.fail", "{field} must not null");
            return FieldValidationResult.Success();
        }
    }
}
