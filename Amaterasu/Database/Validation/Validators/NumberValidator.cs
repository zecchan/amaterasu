﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public class Int32Validator : IFieldValidator<int>
    {
        /// <summary>
        /// Minimum value of the number.
        /// </summary>
        public int? MinValue { get; }
        /// <summary>
        /// Maximum value of the number.
        /// </summary>
        public int? MaxValue { get; }
        public FieldValidationResult Validate(int data)
        {
            if (data < MinValue) return FieldValidationResult.Fail("validation.number.fail", "{field} must be greater than or equal to " + MinValue);
            if (data > MaxValue) return FieldValidationResult.Fail("validation.number.fail", "{field} must be less than or equal to " + MaxValue);
            return FieldValidationResult.Success();
        }

        public FieldValidationResult Validate(object data)
        {
            if (data is int)
                return Validate((int)data);
            return FieldValidationResult.Fail("validation.number.notint", "{field} must be an integer");
        }
    }
}
