﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public class StringLengthValidator : IFieldValidator<string>
    {
        /// <summary>
        /// Minimum length of the string.
        /// </summary>
        public int? MinLength { get; }
        /// <summary>
        /// Maximum length of the string.
        /// </summary>
        public int? MaxLength { get; }

        public StringLengthValidator(int? min = null, int? max = null)
        {
            MinLength = min;
            MaxLength = max;
        }

        public FieldValidationResult Validate(string data)
        {
            var len = data?.Length ?? 0;
            if (MinLength != null && len < MinLength) return FieldValidationResult.Fail("validation.stringlength.fail", "{field} must be at least " + MinLength + " characters");
            if (MaxLength != null && len > MaxLength) return FieldValidationResult.Fail("validation.stringlength.fail", "{field} must be less than " + MaxLength + " characters");
            return FieldValidationResult.Success();
        }

        public FieldValidationResult Validate(object data)
        {
            if (data is string || data == null)
                return Validate(data as string);
            return FieldValidationResult.Fail("validation.stringlength.notstring", "{field} must be a string");
        }
    }
}
