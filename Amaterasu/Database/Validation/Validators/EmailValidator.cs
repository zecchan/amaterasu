﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public class EmailValidator : IFieldValidator<string>
    {
        public FieldValidationResult Validate(string data)
        {
            try
            {
                new MailAddress(data);
            }
            catch
            {
                return FieldValidationResult.Fail("validation.email.fail", "{field} must be a valid email address");
            }
            return FieldValidationResult.Success();
        }

        public FieldValidationResult Validate(object data)
        {
            if (data is string || data == null)
                return Validate(data as string);
            return FieldValidationResult.Fail("validation.email.notstring", "{field} must be a string");
        }
    }
}
