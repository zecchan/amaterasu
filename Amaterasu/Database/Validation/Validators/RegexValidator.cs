﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Amaterasu.Database.Validation
{
    public class RegexValidator : IFieldValidator<string>
    {
        public Regex Regex { get; }

        public FieldValidationResult Validate(string data)
        {
            if (data == null || !Regex.IsMatch(data))
                return FieldValidationResult.Fail("validation.regex.fail", "{field} is not in a correct format.");
            return FieldValidationResult.Success();
        }

        public FieldValidationResult Validate(object data)
        {
            if (data is string || data == null)
                return Validate(data as string);
            return FieldValidationResult.Fail("validation.regex.notstring", "{field} must be a string.");
        }

        public RegexValidator(string regex)
        {
            Regex = new Regex(regex);
        }
        public RegexValidator(Regex regex)
        {
            Regex = regex;
        }
    }
}
