﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public interface IFieldValidator
    {
        /// <summary>
        /// Validates a given data.
        /// </summary>
        /// <param name="data">The data to validate.</param>
        /// <returns>Validation result.</returns>
        FieldValidationResult Validate(object data);
    }

    public interface IFieldValidator<T> : IFieldValidator
    {
        /// <summary>
        /// Validates a given data.
        /// </summary>
        /// <param name="data">The data to validate.</param>
        /// <returns>Validation result.</returns>
        FieldValidationResult Validate(T data);
    }

    public interface IFieldValidatorAttribute
    {
        IFieldValidator Validator { get; }
    }

    public class FieldValidationResult
    {
        /// <summary>
        /// Gets whether the field is valid or not.
        /// </summary>
        public bool Valid { get; }

        /// <summary>
        /// Gets the validation error message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets the error code.
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets the field node.
        /// </summary>
        public string Node { get; set; }

        public static FieldValidationResult Success()
        {
            return new FieldValidationResult(true, null);
        }

        public static FieldValidationResult Fail(string errorCode, string message = null)
        {
            return new FieldValidationResult(false, errorCode, message);
        }

        private FieldValidationResult(bool result, string errorCode, string message = null)
        {
            ErrorCode = errorCode;
            Valid = result;
            Message = message;
        }
    }
}
