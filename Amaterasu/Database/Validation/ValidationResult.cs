﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Database.Validation
{
    public class ValidationResult
    {
        /// <summary>
        /// Gets whether the model is valid or not.
        /// </summary>
        public bool Valid { get => !Errors.Any(x => !x.Valid); }

        /// <summary>
        /// Gets a list of validation errors.
        /// </summary>
        public List<FieldValidationResult> Errors { get; } = new List<FieldValidationResult>();

        public ValidationResult(IEnumerable<FieldValidationResult> results)
        {
            if (results != null)
                Errors = results.Where(x => !x.Valid).ToList();
            else
                Errors.Add(FieldValidationResult.Fail("validation.model.null", "Model instance is null"));
        }
    }
}
