﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Amaterasu.Database
{
    /// <summary>
    /// Contains static methods that allows you to get information of a model.
    /// </summary>
    public static class ModelHelper
    {
        public static string GetSourceName<T>() where T : class, new()
        {
            return GetSourceName(typeof(T));
        }
        public static string GetSourceName(Type type)
        {
            var ca = type.GetCustomAttribute<ModelAttribute>(false);
            if (ca == null) return null;
            return ca.SourceName;
        }

        public static string GetSchemaName<T>() where T : class, new()
        {
            return GetSchemaName(typeof(T));
        }
        public static string GetSchemaName(Type type)
        {
            var ca = type.GetCustomAttribute<ModelAttribute>(false);
            if (ca == null) return null;
            return ca.SchemaName;
        }

        public static string GetKeyName<T>() where T : class, new()
        {
            return GetKeyName(typeof(T));
        }
        public static string GetKeyName(Type type)
        {
            var attrs = type.GetMembers().Where(x => 
                (x.GetCustomAttribute<KeyAttribute>(true) != null 
                || x.GetCustomAttribute<PrimaryKeyAttribute>(true) != null)
            ).ToList();
            if (attrs.Count != 1)
                return null;

            return attrs.First().Name;
        }

        public static object GetKeyValue<T>(T obj) where T : class, new()
        {
            return GetKeyValue(typeof(T), obj);
        }
        public static object GetKeyValue(Type type, object obj)
        {
            return GetValue(type, obj, GetKeyName(type));
        }

        public static object GetValue<T>(T obj, string field) where T : class, new()
        {
            return GetValue(typeof(T), obj, field);
        }
        public static object GetValue(Type type, object obj, string field)
        {
            var attr = type.GetMembers().Where(x =>
                x.Name.ToLower() == field.Trim().ToLower()
                ).FirstOrDefault();
            if (attr == null)
                throw new Exception("There is no member named '" + field + "'");
            return attr.Readable() ? attr.GetValue(obj) : null;
        }

        public static void SetValue<T>(T obj, string field, object value) where T : class, new()
        {
            SetValue(typeof(T), obj, field, value);
        }
        public static void SetValue(Type type, object obj, string field, object value)
        {
            var attr = type.GetMembers().Where(x =>
                x.Name.ToLower() == field.Trim().ToLower()
                ).FirstOrDefault();
            if (attr == null)
                throw new Exception("There is no member named '" + field + "'");
            if (attr.Writeable())
                attr.SetValue(obj, value);
            else
                throw new Exception("Cannot write to member '" + field + "'");
        }

        public static T Clone<T>(T data, bool deep = false) where T: class, new()
        {
            return (T)Clone(typeof(T), data, deep);
        }
        public static object Clone(Type type, object data, bool deep = false)
        {
            var instance = Activator.CreateInstance(type);
            Copy(type, data, instance, deep);
            return instance;
        }

        public static void Copy<T>(T source, T destination, bool deep = false)
        {
            Copy(typeof(T), source, destination, deep);
        }
        public static void Copy(Type type, object source, object destination, bool deep = false)
        {
            var members = GetMembers(type);
            foreach (var mem in members)
            {
                var memType = mem.GetValueType();
                if (mem.Readable() && mem.Writeable())
                {
                    if (deep)
                    {
                        throw new NotSupportedException("Deep clone is currently not supported");
                    }
                    else
                    {
                        var value = mem.GetValue(source);
                        mem.SetValue(destination, value);
                    }
                }
                else
                {
                    if (!mem.Readable())
                        throw new Exception($"Member '{mem.Name}' is not readable");
                    if (!mem.Writeable())
                        throw new Exception($"Member '{mem.Name}' is not writeable");
                }
            }
        }

        public static IEnumerable<MemberInfo> GetMembers<T>(bool includeIgnored = false)
        {
            return GetMembers(typeof(T), includeIgnored);
        }
        public static IEnumerable<MemberInfo> GetMembers(Type type, bool includeIgnored = false)
        {
            return type.GetMembers().Where(x => 
                (x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property) 
                && (includeIgnored || (x.GetCustomAttribute<NotMappedAttribute>(false) == null 
                && x.GetCustomAttribute<IgnoreMemberAttribute>(false) == null))
            );
        }
        public static IEnumerable<MemberInfo> GetForeignMembers<T>()
        {
            return GetForeignMembers(typeof(T));
        }
        public static IEnumerable<MemberInfo> GetForeignMembers(Type type)
        {
            return type.GetMembers().Where(x =>
                (x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property)
                && x.GetCustomAttribute<ForeignModelAttribute>(true) != null
            );
        }

        public static T GetRow<T>(IDataReader reader) where T : class, new()
        {
            try
            {
                var res = Activator.CreateInstance(typeof(T));
                var pi = GetMembers<T>();
                for (var i = 0; i < reader.FieldCount; i++)
                {
                    var p = pi.Where(x => x.Name?.ToLower() == reader.GetName(i)?.ToLower()).FirstOrDefault();
                    if (p != null && p.Writeable())
                    {
                        var value = reader.GetValue(i);
                        if (value is DBNull) value = null;
                        p.SetValue(res, value);
                    }
                }
                return res as T;
            }
            catch {
                return null;
            }
        }
    }
}
