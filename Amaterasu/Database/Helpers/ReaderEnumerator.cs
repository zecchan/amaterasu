﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Amaterasu.Database
{
    public class ReaderEnumerator<T> : IEnumerator<T> where T: class, new()
    {
        public IDataReader DataReader { get; }

        public ReaderEnumerator(IDataReader dr)
        {
            DataReader = dr;
        }

        public T Current { get; private set; }

        object IEnumerator.Current => Current;

        public void Dispose()
        {
            try
            {
                DataReader.Dispose();
            }
            catch { }
        }

        public bool MoveNext()
        {
            var can= DataReader.Read();
            if (!can) return false;
            Current = ModelHelper.GetRow<T>(DataReader);
            return true;
        }

        public void Reset()
        {
            throw new InvalidOperationException("Cannot reset a DataReader, convert it to List to be able to do so.");
        }
    }
}
