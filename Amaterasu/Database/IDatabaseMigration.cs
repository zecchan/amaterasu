﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public interface IDatabaseMigration
    {
        /// <summary>
        /// Migrate the database
        /// </summary>
        /// <param name="databaseContext">Database context to use</param>
        void Migrate(IDatabaseContext databaseContext);

        /// <summary>
        /// Rollback this migration
        /// </summary>
        /// <param name="databaseContext">Database context to use</param>
        void Rollback(IDatabaseContext databaseContext);
    }
}
