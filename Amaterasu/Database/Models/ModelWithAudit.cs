﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database
{
    public abstract class ModelWithAudit : IPostDelete, IPostSave
    {
        /// <summary>
        /// When overriden in inheriting class, it should save the audit log.
        /// </summary>
        protected abstract void OnAudit(AuditType auditType);

        public virtual void OnPostDelete(IDatabaseContext db)
        {
            OnAudit(AuditType.Delete);
        }

        public virtual void OnPostSave(IDatabaseContext db, bool isUpdate, object insertId)
        {
            OnAudit(isUpdate ? AuditType.Update : AuditType.Insert);
        }
    }
}
