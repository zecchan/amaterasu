﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Database.Models
{
    public class ModelWithTimestamps : IModelWithTimestamps, IPreSave
    {
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdatedAt { get; set; }

        public virtual void OnPreSave(IDatabaseContext db, bool isUpdate)
        {
            if (!isUpdate)
                CreatedAt = DateTime.Now;
            LastUpdatedAt = DateTime.Now;
        }
    }
}
