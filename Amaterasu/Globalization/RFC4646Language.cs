﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.Globalization
{
    public struct RFC4646Language
    {
        public string Language { get; }

        public string Subtag { get; }

        public string Region { get; }

        public RFC4646Language(string lang)
        {
            var spl = lang.Split('-');
            Language = spl[0].ToLower();
            if (spl.Length > 2)
                Subtag = spl[1];
            else 
                Subtag = null;
            if (spl.Length > 1)
                Region = spl[spl.Length - 1].ToUpper();
            else
                Region = null;
        }

        public override bool Equals(object obj)
        {
            if (obj is RFC4646Language lang)
            {
                return lang.Language == Language && lang.Subtag == Subtag && lang.Region == Region;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            var part = Language;
            if (Subtag != null)
                part += "-" + Subtag;
            if (Region != null)
                part += "-" + Region;
            return part;
        }

        public int LooselyEqual(RFC4646Language language)
        {
            var sim = 0;
            if (language.Language == Language)
                sim++;
            else
                return 0;
            if (language.Region == Region)
                sim++;
            if (language.Subtag == Subtag)
                sim++;

            return sim;
        }
    }
}
