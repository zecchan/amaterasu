﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Amaterasu.Mapping
{
    public static class Mapper
    {
        public static TDest Map<TDest>(object source, TDest destination, bool ignoreCase = true) where TDest : class, new()
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (destination == null) throw new ArgumentNullException(nameof(destination));

            var ts = source.GetType();
            var td = typeof(TDest);

            var ps = ts.GetProperties();
            var pd = td.GetProperties();

            var res = destination;

            foreach (var sp in ps)
            {
                if (!sp.CanRead) continue;
                var spName = sp.Name;
                if (ignoreCase) spName = spName.ToLower();

                var dp = pd.Where(x =>

                x.Name == spName || (ignoreCase && x.Name.ToLower() == spName)

                ).FirstOrDefault();
                if (dp != null && dp.CanWrite)
                {
                    try
                    {
                        dp.SetValue(res, sp.GetValue(source));
                    }
                    catch { }
                }
            }

            return res;
        }

        public static TDest MapTo<TDest>(object obj, bool ignoreCase = true) where TDest: class, new()
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));
            var res = Activator.CreateInstance(typeof(TDest)) as TDest;

            return Map(obj, res);
        }
    }

    public class MappingMethod
    {
        public Type SourceType { get; }
        public Type DestinationType { get; }

        public Action<object, object> Converter { get; set; }

        public object Map(object source, object destination)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (destination == null) destination = Activator.CreateInstance(DestinationType);
            var sType = source.GetType();
            var dType = destination.GetType();

            if (sType != SourceType) throw new TypeMismatchException(SourceType, sType);
            if (dType != DestinationType) throw new TypeMismatchException(DestinationType, dType);

            if (Converter != null)
            {
                Converter.Invoke(source, destination);
                return destination;
            }

            return destination;
        }
    }

    public class TypeMismatchException: Exception
    {
        public Type ExpectedType { get; }
        public Type GotType { get; }

        public TypeMismatchException(Type expected, Type got): base("Expecting object of type '" + expected.FullName + "' got '" + got.FullName + "' instead")
        {
            ExpectedType = expected;
            GotType = got;
        }
    }
}
