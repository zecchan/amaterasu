﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu
{
    public static class RandomGenerator
    {
        public const string LowercaseAlpha = "abcdefghijklmnopqrstuvwxyz";
        public const string UppercaseAlpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string Number = "0123456789";
        public const string AlphaNumeric = LowercaseAlpha + UppercaseAlpha + Number;

        private static Random Randomizer { get; } = new Random();

        public static string Id()
        {
            return DateTime.Now.ToString("yyyy.MM.dd.HH.mm.ss") + "_" + Guid.NewGuid().ToString("n");
        }

        public static string ShortId()
        {
            return Guid.NewGuid().ToString("n").Substring(0, 8);
        }

        /// <summary>
        /// Generates a string with specified <paramref name="length"/> and <paramref name="charset"/>
        /// </summary>
        public static string String(int length, string charset = AlphaNumeric)
        {
            var res = "";
            for (var i = 0; i < length; i++)
            {
                res += charset[Int(charset.Length)];
            }
            return res;
        }

        /// <summary>
        /// Generates a random integer that is greater than or equal to 0 and less than <paramref name="maxValue"/>.
        /// </summary>
        public static int Int(int maxValue)
        {
            return Randomizer.Next(maxValue);
        }

        /// <summary>
        /// Generates a random integer that is greater than or equal to <paramref name="minValue"/> and less than <paramref name="maxValue"/>.
        /// </summary>
        public static int Int(int minValue, int maxValue)
        {
            return Randomizer.Next(minValue, maxValue);
        }
    }
}
