﻿using Amaterasu.Database.Attributes;
using Amaterasu.Database.MySql;
using Amaterasu.Database.SQLServer;
using Amaterasu.Expressions;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Text;

namespace Amaterasu.Database.MySQL
{
    [DatabaseCredentialClass(typeof(DatabaseCredential), "MySQL")]
    public class MySQLDatabaseContext : SQLBaseDatabaseContext<MySqlConnection>, IDatabaseAutoReconnect
    {
        public bool AutoReconnect { get; set; }

        protected void DoAutoReconnect()
        {
            if (!AutoReconnect) return;
            if (Connection.State == ConnectionState.Closed || Connection.State == ConnectionState.Broken)
            {
                try
                {
                    Close();

                    Open();
                }
                catch { }
            }
        }

        public MySQLDatabaseContext()
        {
            Connection = new MySqlConnection();
            ExpressionToSQL = new MySqlExpressionToSQL();
        }

        public override IQueryBuilder<T> From<T>()
        {
            var qb = new MySqlQueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            return qb;
        }

        public override IQueryBuilder<T> From<T>(string source)
        {
            var qb = new MySqlQueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            qb.Source = source;
            return qb;
        }

        public override void Open()
        {
            if (string.IsNullOrWhiteSpace(Credential.ConnectionString))
            {
                var host = Credential.Host;
                var database = Credential.Database;
                var username = Credential.User;
                var password = Credential.Password;

                var cs = $"server={host};database={database};uid={username};";
                if (!string.IsNullOrEmpty(password))
                    cs += "pwd=" + password;
                Connection.ConnectionString = cs;
            }
            else Connection.ConnectionString = Credential.ConnectionString;
            base.Open();
        }

        protected override object GetLastInsertID<T>(T data)
        {
            DoAutoReconnect();
            return null;
        }

        public override int ExecuteNonQuery(string sql, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<MySqlCommand>())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(sql, parameters);
                var ret = cmd.ExecuteNonQuery();
                EndReport(true, ret);
                return ret;
            }
        }

        public override IEnumerable<T> ExecuteQuery<T>(string query, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            var result = new List<T>();
            using (var cmd = CreateCommand<MySqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                using (var rdr = cmd.ExecuteReader())
                {
                    EndReport(true);
                    try
                    {
                        while (rdr.Read())
                        {
                            var row = ModelHelper.GetRow<T>(rdr);
                            result.Add(row);
                        }
                    }
                    catch { }
                }
            }
            return result;
        }

        public override void ExecuteQuery(string query, Action<IDataReader> action, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<MySqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                using (var rdr = cmd.ExecuteReader())
                {
                    EndReport(true);
                    if (action != null)
                        action.Invoke(rdr);
                }
            }
        }

        public override IEnumerator<T> ExecuteQueryEnumerator<T>(string query, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<MySqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                var rdr = cmd.ExecuteReader();
                EndReport(true);
                return new ReaderEnumerator<T>(rdr);
            }
        }

        public override IDataReader ExecuteReader(string query, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<MySqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                var rdr = cmd.ExecuteReader();
                EndReport(true);
                return rdr;
            }
        }
    }
}
