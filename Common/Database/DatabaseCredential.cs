﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Database
{
    public class DatabaseCredential : IDatabaseCredential
    {
        public virtual string Host { get; set; } = string.Empty;

        public virtual int? Port { get; set; }

        public virtual string User { get; set; } = string.Empty;

        public virtual string Password { get; set; } = string.Empty;

        public virtual string Database { get; set; } = string.Empty;

        public virtual string ConnectionString { get; set; } = string.Empty;
    }
}
