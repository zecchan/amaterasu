﻿using Amaterasu.Database;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amaterasu.Database.MongoDB;

namespace Amaterasu.Database.MongoDB
{
    public class MongoForeignWrapper<T> : IEnumerable<T>
    {
        IFindFluent<T, T> Data { get; }
        MongoDatabaseContext Database { get; }

        public MongoForeignWrapper(IFindFluent<T, T> data, MongoDatabaseContext databaseContext)
        {
            Data = data;
            Database = databaseContext;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new MongoForeignWrapperEnumerator<T>(Data, Database);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new MongoForeignWrapperEnumerator<T>(Data, Database);
        }
    }

    public class MongoForeignWrapperEnumerator<T> : IEnumerator<T>
    {
        MongoDatabaseContext Database { get; }
        IFindFluent<T, T> Data { get; }
        IEnumerable<T> Enum { get; set; }
        int Position { get; set; }
        public MongoForeignWrapperEnumerator(IFindFluent<T, T> data, MongoDatabaseContext database)
        {
            Data = data;
            Enum = Data.ToEnumerable();
            Position = -1;
            Reset();
            Database = database;
        }

        public T Current
        {
            get
            {
                try
                {
                    var ele = ProcessForeign(Enum.ElementAt(Position));
                    if (ele is T data) return data;
#pragma warning disable CS8603 // Possible null reference return.
                    return default;
#pragma warning restore CS8603 // Possible null reference return.
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

#pragma warning disable CS8603 // Possible null reference return.
        object IEnumerator.Current => Current;
#pragma warning restore CS8603 // Possible null reference return.

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            Position++;
            return Position < Enum.Count();
        }

        public void Reset()
        {
            Enum = Data.ToEnumerable();
            Position = -1;
        }

        private object ProcessForeign(object data)
        {
            if (data == null) return null;
            var members = ModelHelper.GetForeignMembers(data.GetType());
            foreach (var foreignMember in members)
            {
                if (foreignMember.Writeable())
                {
                    var foreignAttribute = foreignMember.GetCustomAttribute<ForeignModelAttribute>(true);
                    if (foreignAttribute == null)
                    {
                        throw new Exception($"Cannot get the ForeignModelAttribute");
                    }

                    var originBsonData = data.ToBsonDocument();
                    var foreignMemberType = foreignMember.GetValueType();
                    var foreignCollectionName = ModelHelper.GetSourceName(foreignMemberType);
                    var foreignCollection = Database.Database.GetCollection<BsonDocument>(foreignCollectionName);

                    var foreignKeyMember = ModelHelper.GetMembers<T>().Where(x => x.Name.ToLower() == foreignAttribute.ForeignKey.Trim().ToLower()).FirstOrDefault();
                    var trueForeignKeyName = foreignKeyMember?.Name;
                    var localKeyMember = ModelHelper.GetMembers(foreignMemberType).Where(x => x.Name.ToLower() == foreignAttribute.LocalKey.Trim().ToLower()).FirstOrDefault();
                    var trueLocalKeyName = localKeyMember?.Name;

                    if (foreignKeyMember.GetCustomAttribute<BsonIdAttribute>(true) != null)
                        trueForeignKeyName = "_id";
                    if (localKeyMember.GetCustomAttribute<BsonIdAttribute>(true) != null)
                        trueLocalKeyName = "_id";

                    if (trueForeignKeyName == null)
                    {
                        throw new Exception($"Cannot get formal foreign key name, it must not be ignored");
                    }
                    if (trueLocalKeyName == null)
                    {
                        throw new Exception($"Cannot get formal local key name, it must not be ignored");
                    }

                    var filter = new BsonDocument();
                    filter.Add(trueLocalKeyName, originBsonData.GetValue(trueForeignKeyName));
                    var res = foreignCollection.Find(filter).FirstOrDefault();
                    if (res != null)
                    {
                        var value = BsonSerializer.Deserialize(res, foreignMemberType);
                        foreignMember.SetValue(data, value);
                    }
                    else
                    {
                        foreignMember.SetValue(data, null);
                    }
                }
                else
                {
                    throw new Exception($"Member '{foreignMember.Name}' of {typeof(T).FullName} is marked as foreign, but not writeable");
                }
            }

            // find instance and enum member
            members = data.GetType().GetMembers().Where(x =>
            (x.MemberType == System.Reflection.MemberTypes.Property || x.MemberType == System.Reflection.MemberTypes.Field) &&
            (x.GetValueType().IsClass || typeof(IEnumerable).IsAssignableFrom(x.GetValueType())) && 
            !x.GetValueType().IsAbstract && 
            x.GetValueType().IsPublic &&
            !x.GetValueType().IsValueType &&
            x.GetValueType() != typeof(string));
            foreach(var instanceMember in members)
            {

            }
            return data;
        }

    }
}
