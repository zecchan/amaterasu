
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;
using System;
using System.Linq;

namespace Amaterasu.Database.MongoDB
{
    public class MongoHelper
    {
#if NET5_0_OR_GREATER
        public static string? GetKeyName<T>() where T : class, new()
#else
        public static string GetKeyName<T>() where T : class, new()
#endif
        {
            return GetKeyName(typeof(T));
        }

#if NET5_0_OR_GREATER
        public static string? GetKeyName(Type type)
#else
        public static string GetKeyName(Type type)
#endif
        {
            var attrs = type.GetMembers().Where(x =>
                (x.GetCustomAttribute<BsonIdAttribute>(true) != null
                 || x.GetCustomAttribute<PrimaryKeyAttribute>(true) != null
                 || x.GetCustomAttribute<KeyAttribute>(true) != null)
            ).ToList();
            if (attrs.Count > 1)
                throw new Exception("Model has multiple BsonId attribute: " + type.Name);
            if (attrs.Count != 1)
                return null;

            return attrs.First().Name;
        }

        public static object GetKeyValue<T>(T obj) where T : class, new()
        {
            return GetKeyValue(typeof(T), obj);
        }
        public static object GetKeyValue(Type type, object obj)
        {
            return ModelHelper.GetValue(type, obj, GetKeyName(type));
        }
    }
}