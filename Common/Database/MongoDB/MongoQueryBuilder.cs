﻿using Amaterasu.Expressions;
using Amaterasu.Database.MongoDB;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Amaterasu.Database.MongoDB
{
    public class MongoQueryBuilder<T> : IQueryBuilder<T> where T : class, new()
    {
#if NET5_0_OR_GREATER
        public MongoDatabaseContext? DatabaseContext { get; set; }
#else
        public MongoDatabaseContext DatabaseContext { get; set; }
#endif

#if NET5_0_OR_GREATER
        IDatabaseContext? IQueryBuilder<T>.DatabaseContext
#else
        IDatabaseContext IQueryBuilder<T>.DatabaseContext
#endif
        {
            get => DatabaseContext; set
            {
                if (value is MongoDatabaseContext c)
                {
                    DatabaseContext = c;
                }
                else throw new Exception("Value must be a MongoDatabaseContext");
            }
        }

        public string Source { get; set; }

        public QueryLimit Limit { get; } = new QueryLimit();

        public List<QueryMember> Fields { get; } = new List<QueryMember>();

        public List<QuerySort> Sorts { get; } = new List<QuerySort>();

        public List<SortDefinition<T>> SortDefinitions { get; } = new List<SortDefinition<T>>();

        public List<MongoFilterDefinitionWithLogic<T>> FilterDefinitions { get; } = new List<MongoFilterDefinitionWithLogic<T>>();

        public MongoQueryBuilder()
        {
            Source = ModelHelper.GetSourceName<T>();
        }

        public MongoDBQuery<T> BuildFilterDefinition()
        {
            var re = new MongoDBQuery<T>();
            foreach (var f in FilterDefinitions)
            {
                if (re.Filter == null)
                {
                    re.Filter = f.Filter;
                }
                else
                {
                    if (f.Logic == ExpressionType.OrElse)
                    {
                        re.Filter = Builders<T>.Filter.Or(re.Filter, f.Filter);
                    }
                    else
                    {
                        re.Filter = Builders<T>.Filter.And(re.Filter, f.Filter);
                    }
                }
            }
            if (FilterDefinitions.Count == 0)
                re.Filter = FilterDefinition<T>.Empty;
            re.Sorts.AddRange(SortDefinitions);
            return re;
        }

        private IEnumerable<T> Find(bool sort = true, bool limit = true, bool clear = true)
        {
            if (string.IsNullOrWhiteSpace(Source))
                throw new Exception($"'{typeof(T).FullName}' does not have ModelAttribute and no source was specified");
            var fd = BuildFilterDefinition();
            var ff = DatabaseContext.Database.GetCollection<T>(Source).Find(fd.Filter);
            if (sort)
            {
                foreach (var s in fd.Sorts)
                {
                    ff = ff.Sort(s);
                }
            }
            if (limit)
            {
                if (Limit.Skip > 0)
                    ff = ff.Skip(Limit.Skip);
                if (Limit.Take > 0)
                    ff = ff.Limit(Limit.Take);
            }

            if (clear) Clear();
            return new MongoForeignWrapper<T>(ff, DatabaseContext);
        }

        public SQLWithParameter BuildSQL(bool clear = true)
        {
            throw new NotSupportedException("Cannot build SQL from Mongo filter");
        }

        public void Clear()
        {
            FilterDefinitions.Clear();
            SortDefinitions.Clear();
            Limit.Clear();
        }

        public int Count(bool clear = true)
        {
            return (int)Find(true, false, clear).Count();
        }

        public T First()
        {
            return Find().First();
        }

        public T FirstOrDefault()
        {
            return Find().FirstOrDefault();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var res = Find();
            return res.GetEnumerator();
        }

        public IQueryBuilder<T> GroupEnd()
        {
            throw new NotSupportedException();
        }

        public IQueryBuilder<T> GroupStart()
        {
            throw new NotSupportedException();
        }

        public T Last()
        {
            return Find().Last();
        }

        public T LastOrDefault()
        {
            return Find().LastOrDefault();
        }

        public IQueryBuilder<T> OrderBy(Expression<Func<T, object>> expression, QuerySortDirection direction = QuerySortDirection.Ascending)
        {
            if (direction == QuerySortDirection.Ascending)
                SortDefinitions.Add(Builders<T>.Sort.Ascending(expression));
            else
                SortDefinitions.Add(Builders<T>.Sort.Descending(expression));
            return this;
        }

        public IQueryBuilder<T> OrGroupStart()
        {
            throw new NotSupportedException();
        }

        public IQueryBuilder<T> Select(params Expression<Func<T, object>>[] expression)
        {
            throw new NotSupportedException();
        }

        public IQueryBuilder<T> Skip(int skip)
        {
            Limit.Skip = Math.Max(0, skip);
            return this;
        }

        public IQueryBuilder<T> Take(int take)
        {
            Limit.Take = Math.Max(0, take);
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IQueryBuilder<T> Where(Expression<Func<T, bool>> expression)
        {
            FilterDefinitions.Add(MongoFilterDefinitionWithLogic<T>.FromFilterDefinition(Builders<T>.Filter.Where(expression)));
            return this;
        }

        public IQueryBuilder<T> OrWhere(Expression<Func<T, bool>> expression)
        {
            FilterDefinitions.Add(MongoFilterDefinitionWithLogic<T>.FromFilterDefinition(Builders<T>.Filter.Where(expression), ExpressionType.OrElse));
            return this;
        }
    }

    public class MongoDBQuery<T>
    {
        public FilterDefinition<T> Filter { get; set; }

        public List<SortDefinition<T>> Sorts { get; } = new List<SortDefinition<T>>();
    }

    public class MongoFilterDefinitionWithLogic<T>
    {
        public FilterDefinition<T> Filter { get; set; }

        public ExpressionType Logic { get; set; } = ExpressionType.AndAlso;

        public static MongoFilterDefinitionWithLogic<T> FromFilterDefinition(FilterDefinition<T> def, ExpressionType logic = ExpressionType.AndAlso)
        {
            return new MongoFilterDefinitionWithLogic<T>()
            {
                Filter = def,
                Logic = logic
            };
        }
    }
}
