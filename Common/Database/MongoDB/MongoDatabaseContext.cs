﻿using Amaterasu.Database;
using Amaterasu.Database.Attributes;
using Amaterasu.Database.Enums;
using Amaterasu.Expressions;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Amaterasu.Database.MongoDB
{
    [DatabaseCredentialClass(typeof(DatabaseCredential), "Mongo")]
    public class MongoDatabaseContext : IMongoDatabaseContext
    {
        public IDatabaseCredential Credential { get; private set; } = new DatabaseCredential();

        public IMongoClient Client { get; protected set; }

        public IMongoDatabase Database { get; protected set; }

        public IDbConnection Connection => throw new NotSupportedException();

#if NET5_0_OR_GREATER
        protected IClientSessionHandle? CurrentSession { get; set; }
#else
        protected IClientSessionHandle CurrentSession { get; set; }
#endif
        public List<DataProcessTrigger> DataProcessTriggers { get; } = new List<DataProcessTrigger>();

        public void Close()
        {
        }

#if NET5_0_OR_GREATER
        public string GetCollectionNameFromType<T>(string? defaultName = null) where T : class, new()
#else
        public string GetCollectionNameFromType<T>(string defaultName = null) where T : class, new()
#endif
        {
            var src = ModelHelper.GetSourceName<T>();
            if (src == null)
            {
                if (defaultName == null)
                    throw new Exception("Type [" + typeof(T).Name + "] does not have ModelAttribute, cannot define collection name");
                return defaultName;
            }
            return src;
        }

        private void ProcessTrigger(DataTrigger trigger, object data)
        {
            var triggers = DataProcessTriggers.Where(x => x.Trigger == trigger);
            foreach (var trig in triggers)
            {
                trig.Action?.Invoke(data, this);
            }
        }

        public void Delete<T>(T data) where T : class, new()
        {
            var idmem = MongoHelper.GetKeyName<T>();
            var idval = MongoHelper.GetKeyValue<T>(data);
            if (data is IPreDelete pd)
                pd.OnPreDelete(this);
            ProcessTrigger(DataTrigger.PreDelete, data);
            DeleteWhere<T>(idmem, idval);
            if (data is IPostDelete td)
                td.OnPostDelete(this);
            ProcessTrigger(DataTrigger.PostDelete, data);
        }

        public int DeleteWhere<T>(string fieldName, object value) where T : class, new()
        {
            var col = GetCollectionNameFromType<T>();
            var res = CurrentSession == null ?
                Database.GetCollection<T>(col).DeleteOne(Builders<T>.Filter.Eq(fieldName, value)) :
                Database.GetCollection<T>(col).DeleteOne(CurrentSession, Builders<T>.Filter.Eq(fieldName, value));
            return (int)res.DeletedCount;
        }

        public int DeleteWhere<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            var col = GetCollectionNameFromType<T>();
            var res = CurrentSession == null ?
                Database.GetCollection<T>(col).DeleteMany(expression) :
                Database.GetCollection<T>(col).DeleteMany(CurrentSession, expression);
            return (int)res.DeletedCount;
        }

        public bool Exists<T>(T data) where T : class, new()
        {
            var col = GetCollectionNameFromType<T>();
            var idmem = MongoHelper.GetKeyName<T>();
            var idval = MongoHelper.GetKeyValue<T>(data);
            if (string.IsNullOrWhiteSpace(idmem))
            {
                throw new Exception("Model does not have PrimaryKey: " + typeof(T).Name);
            }
            var res = Database.GetCollection<T>(col).Find(Builders<T>.Filter.Eq(idmem, idval)).FirstOrDefault();
            return res != null;
        }

        public IQueryBuilder<T> From<T>() where T : class, new()
        {
            return new MongoQueryBuilder<T>()
            {
                DatabaseContext = this
            };
        }

        public IQueryBuilder<T> From<T>(string source) where T : class, new()
        {
            return new MongoQueryBuilder<T>()
            {
                DatabaseContext = this,
                Source = source
            };
        }

        public void Insert<T>(T data) where T : class, new()
        {
            var col = GetCollectionNameFromType<T>();
            if (data is IPreSave pd)
                pd.OnPreSave(this, false);
            ProcessTrigger(DataTrigger.PreSave, data);

            if (CurrentSession == null)
                Database.GetCollection<T>(col).InsertOne(data);
            else
                Database.GetCollection<T>(col).InsertOne(CurrentSession, data);

            if (data is IPostSave td)
                td.OnPostSave(this, false, null);
            ProcessTrigger(DataTrigger.PostSave, data);
        }

        public void Open()
        {
            Client = new MongoClient(MakeConnectionString());
            Database = Client.GetDatabase(Credential.Database);
        }

        private string MakeConnectionString()
        {
            if (!string.IsNullOrWhiteSpace(Credential.ConnectionString))
                return Credential.ConnectionString;
            var connString = "mongodb://";
            if (!string.IsNullOrWhiteSpace(Credential.User))
            {
                connString += Credential.User;
                if (!string.IsNullOrWhiteSpace(Credential.Password))
                {
                    connString += ":" + Credential.Password;
                }
                connString += "@";
            }
            connString += Credential.Host ?? "127.0.0.1";    
            connString += ":" + (Credential.Port ?? 27017);
            
            if (!string.IsNullOrWhiteSpace(Credential.User))
            {
                connString += "/?authSource=admin";
            }
            return connString;
        }

        public void Save<T>(T data) where T : class, new()
        {
            var update = Exists(data);
            var col = GetCollectionNameFromType<T>();
            if (data is IPreSave pd)
                pd.OnPreSave(this, update);
            ProcessTrigger(DataTrigger.PreSave, data);
            if (update)
            {
                var idmem = MongoHelper.GetKeyName<T>();
                var idval = MongoHelper.GetKeyValue<T>(data);
                if (CurrentSession == null)
                    Database.GetCollection<T>(col).FindOneAndReplace(Builders<T>.Filter.Eq(idmem, idval), data);
                else
                    Database.GetCollection<T>(col).FindOneAndReplace(CurrentSession, Builders<T>.Filter.Eq(idmem, idval), data);
            }
            else
            {
                if (CurrentSession == null)
                    Database.GetCollection<T>(col).InsertOne(data);
                else
                    Database.GetCollection<T>(col).InsertOne(CurrentSession, data);
            }
            if (data is IPostSave td)
                td.OnPostSave(this, update, null);
            ProcessTrigger(DataTrigger.PostSave, data);
        }

        public void Truncate<T>() where T : class, new()
        {
            var col = GetCollectionNameFromType<T>();
            Database.DropCollection(col);
        }

        public void Truncate(string source)
        {
            Database.DropCollection(source);
        }

        public void Update<T>(T data) where T : class, new()
        {
            var col = GetCollectionNameFromType<T>();
            if (data is IPreSave pd)
                pd.OnPreSave(this, false);
            ProcessTrigger(DataTrigger.PreSave, data);
            var idmem = MongoHelper.GetKeyName<T>();
            var idval = MongoHelper.GetKeyValue<T>(data);

            if (CurrentSession == null)
                Database.GetCollection<T>(col).FindOneAndReplace(Builders<T>.Filter.Eq(idmem, idval), data);
            else
                Database.GetCollection<T>(col).FindOneAndReplace(CurrentSession, Builders<T>.Filter.Eq(idmem, idval), data);

            if (data is IPostSave td)
                td.OnPostSave(this, true, null);
            ProcessTrigger(DataTrigger.PostSave, data);
        }

#if NET5_0_OR_GREATER
        public void UpdateWhere<T>(Dictionary<string, object> set, Expression<Func<T, bool>> expression, string? source = null) where T : class, new()
#else
        public void UpdateWhere<T>(Dictionary<string, object> set, Expression<Func<T, bool>> expression, string source = null) where T : class, new()
#endif
        {
            var col = GetCollectionNameFromType<T>();
            var dataset = (source != null ? From<T>(source) : From<T>()).Where(expression).ToList();
            var members = ModelHelper.GetMembers(typeof(T));
            foreach (var data in dataset)
            {
                foreach (var field in set)
                {
                    var member = members.FirstOrDefault(x => x.Name.ToLower() == field.Key.Trim().ToLower() && x.MemberType == MemberTypes.Property);
                    if (member != null)
                    {
                        if (member is PropertyInfo pi && pi.CanWrite)
                        {
                            pi.SetValue(data, field.Value);
                        }
                        else
                        {
                            throw new Exception($"Cannot write to property '{member.Name}'");
                        }
                    }
                    else
                    {
                        throw new Exception("Field not found: " + field);
                    }
                }

                var idmem = MongoHelper.GetKeyName<T>();
                var idval = MongoHelper.GetKeyValue<T>(data);

                if (CurrentSession == null)
                    Database.GetCollection<T>(col).FindOneAndReplace(Builders<T>.Filter.Eq(idmem, idval), data);
                else
                    Database.GetCollection<T>(col).FindOneAndReplace(CurrentSession, Builders<T>.Filter.Eq(idmem, idval), data);
            }
        }

        public void Transaction(Action<IDatabaseContext> transaction, IsolationLevel? isolationLevel = null)
        {
            using (var ses = Database.Client.StartSession())
            {
                ses.StartTransaction();
                try
                {
                    CurrentSession = ses;
                    transaction.Invoke(this);

                    ses.CommitTransaction();
                }
                catch (Exception ex)
                {
                    ses.AbortTransaction();
                    throw new Exception("Database transaction failed, see inner exception for detail.", ex);
                }
                finally
                {
                    CurrentSession = null;
                }
            }
        }

        public void Open(IDatabaseCredential credential)
        {
            Credential = credential;
            Open();
        }
    }
}
