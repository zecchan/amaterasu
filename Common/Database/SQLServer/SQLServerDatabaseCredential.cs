﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace Amaterasu.Database.SQLServer
{
    public class SQLServerDatabaseCredential
    {
        [JsonPropertyName("authenticationMode")]
        public SQLServerAuthenticationMode AuthenticationMode { get; set; }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum SQLServerAuthenticationMode
    {
        Default = 0,
        Windows = 1,
        SQL = 2,
        Mixed = 3,
    }
}
