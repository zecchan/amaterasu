﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Linq;
using System.Reflection;
using System.Text;
using Amaterasu.Database.Attributes;

namespace Amaterasu.Database.SQLServer
{
    [DatabaseCredentialClass(typeof(SQLServerDatabaseCredential), "SQLServer")]
    public class SQLServerDatabaseContext : SQLBaseDatabaseContext<SqlConnection>, IDatabaseAutoReconnect
    {
        public bool AutoReconnect { get; set; }

        protected void DoAutoReconnect()
        {
            if (!AutoReconnect) return;
            if (Connection.State == ConnectionState.Closed || Connection.State == ConnectionState.Broken)
            {
                try
                {
                    Close();

                    Open();
                }
                catch { }
            }
        }

        public SQLServerDatabaseContext()
        {
            Connection = new SqlConnection();
            ExpressionToSQL = new SQLServerExpressionToSQL();
        }

        public override IQueryBuilder<T> From<T>()
        {
            var qb = new SQLServerQueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            return qb;
        }

        public override IQueryBuilder<T> From<T>(string source)
        {
            var qb = new SQLServerQueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            qb.Source = source;
            return qb;
        }

        public override void Open()
        {
            Connection.ConnectionString = BuildConnectionString();
            base.Open();
        }

        private string BuildConnectionString()
        {
            string connectionString;

            if (string.IsNullOrWhiteSpace(Credential.ConnectionString))
            {
                var host = Credential.Host;
                var database = Credential.Database;
                var username = Credential.User;
                var password = Credential.Password;
                if (Credential is SQLServerDatabaseCredential)
                {
                    // NYI
                    var cs = $"Server={host};Database={database};User Id={username};";
                    if (!string.IsNullOrEmpty(password))
                        cs += "Password=" + password;
                    cs += ";MultipleActiveResultSets=True;";
                    connectionString = cs;
                }
                else
                {
                    var cs = $"Server={host};Database={database};User Id={username};";
                    if (!string.IsNullOrEmpty(password))
                        cs += "Password=" + password;
                    cs += ";MultipleActiveResultSets=True;";
                    connectionString = cs;
                }
            }
            else connectionString = Credential.ConnectionString;

            return connectionString;
        }

        protected override object GetLastInsertID<T>(T data)
        {
            DoAutoReconnect();
            try
            {
                using (var cmd = CreateCommand())
                {
                    cmd.CommandText = "SELECT IDENT_CURRENT('" + GetSourceName<T>() + "')";
                    var ret = cmd.ExecuteScalar();
                    return ret;
                }
            }
            catch
            {
                return null;
            }
        }

        public override int ExecuteNonQuery(string sql, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<SqlCommand>())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(sql, parameters);
                var ret = cmd.ExecuteNonQuery();
                EndReport(true, ret);
                return ret;
            }
        }

        public override IEnumerable<T> ExecuteQuery<T>(string query, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            var result = new List<T>();
            using (var cmd = CreateCommand<SqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                using (var rdr = cmd.ExecuteReader())
                {
                    EndReport(true);
                    try
                    {
                        while (rdr.Read())
                        {
                            var row = ModelHelper.GetRow<T>(rdr);
                            result.Add(row);
                        }
                    }
                    catch { }
                }
            }
            return result;
        }

        public override void ExecuteQuery(string query, Action<IDataReader> action, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<SqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                using (var rdr = cmd.ExecuteReader())
                {
                    EndReport(true);
                    if (action != null)
                        action.Invoke(rdr);
                }
            }
        }

        public override IEnumerator<T> ExecuteQueryEnumerator<T>(string query, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<SqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                var rdr = cmd.ExecuteReader();
                EndReport(true);
                return new ReaderEnumerator<T>(rdr);
            }
        }

        public override IDataReader ExecuteReader(string query, Dictionary<string, object> parameters = null)
        {
            DoAutoReconnect();
            using (var cmd = CreateCommand<SqlCommand>())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, kv.Value);
                    }
                BeginReport(query, parameters);
                var rdr = cmd.ExecuteReader();
                EndReport(true);
                return rdr;
            }
        }
    }
}
