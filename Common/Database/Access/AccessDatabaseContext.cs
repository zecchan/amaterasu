﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Data;
using System.Linq;
using Amaterasu.Database.Attributes;

namespace Amaterasu.Database.Access
{
    [DatabaseCredentialClass(typeof(DatabaseCredential), "Access")]
    public class AccessDatabaseContext : SQLBaseDatabaseContext<OleDbConnection>
    {
        public AccessDatabaseContext()
        {
            Connection = new OleDbConnection();
            ExpressionToSQL = new AccessExpressionToSQL();

            var typ = typeof(IAccessExpressionToSQLConverter);
            var compat = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes().Where(x => typ.IsAssignableFrom(x) && !x.IsAbstract)).ToList();
            foreach (var c in compat)
            {
                try
                {
                    var o = Activator.CreateInstance(c) as IAccessExpressionToSQLConverter;
                    ExpressionToSQL.Converters.Add(o);
                }
                catch { }
            }
        }

        public override IQueryBuilder<T> From<T>()
        {
            var qb = new AccessQueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            return qb;
        }

        public override IQueryBuilder<T> From<T>(string source)
        {
            var qb = new AccessQueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            qb.Source = source;
            return qb;
        }

        public override void Open()
        {
            if (!string.IsNullOrWhiteSpace(Credential.ConnectionString))
            {
                Connection.ConnectionString = Credential.ConnectionString;
            }
            else
            {
                var host = Credential.Host;
                var password = Credential.Password;

                var cs = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={host};Persist Security Info=False;";
                if (!string.IsNullOrEmpty(password))
                    cs = $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={host};Jet OLEDB:Database Password={password};";
                //cs += ";MultipleActiveResultSets=True;";
                Connection.ConnectionString = cs;
            }
            base.Open();
        }

        public virtual object FormatParameterValue(object value)
        {
            if (value is DateTime dt)
            {
                return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, dt.Second);
            }
            return value;
        }

        public override int ExecuteNonQuery(string sql, Dictionary<string, object> parameters = null)
        {
            using (var cmd = Connection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, FormatParameterValue(kv.Value));
                    }
                return cmd.ExecuteNonQuery();
            }
        }

        public override IEnumerable<T> ExecuteQuery<T>(string query, Dictionary<string, object> parameters = null)
        {
            var result = new List<T>();
            using (var cmd = Connection.CreateCommand())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, FormatParameterValue(kv.Value)); 
                    }
                using (var rdr = cmd.ExecuteReader())
                {
                    try
                    {
                        while (rdr.Read())
                        {
                            var row = ModelHelper.GetRow<T>(rdr);
                            result.Add(row);
                        }
                    }
                    catch { }
                }
            }
            return result;
        }

        public override void ExecuteQuery(string query, Action<IDataReader> action, Dictionary<string, object> parameters = null)
        {
            using (var cmd = Connection.CreateCommand())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, FormatParameterValue(kv.Value));
                    }
                using (var rdr = cmd.ExecuteReader())
                {
                    if (action != null)
                        action.Invoke(rdr);
                }
            }
        }

        public override IEnumerator<T> ExecuteQueryEnumerator<T>(string query, Dictionary<string, object> parameters = null)
        {
            using (var cmd = Connection.CreateCommand())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, FormatParameterValue(kv.Value));
                    }
                var rdr = cmd.ExecuteReader();
                return new ReaderEnumerator<T>(rdr);
            }
        }

        public override IDataReader ExecuteReader(string query, Dictionary<string, object> parameters = null)
        {
            using (var cmd = Connection.CreateCommand())
            {
                cmd.CommandText = query;
                if (parameters != null)
                    foreach (var kv in parameters)
                    {
                        cmd.Parameters.AddWithValue(kv.Key, FormatParameterValue(kv.Value));
                    }
                return cmd.ExecuteReader();
            }
        }

        protected override object GetLastInsertID<T>(T data)
        {
            using(var cmd  = Connection.CreateCommand())
            {
                cmd.CommandText = "select @@IDENTITY from " + GetSourceName<T>();
                return cmd.ExecuteScalar();
            }
        }
    }
}
