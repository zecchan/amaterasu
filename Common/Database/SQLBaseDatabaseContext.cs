﻿using Amaterasu.Expressions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
using Amaterasu.Database.Core;
using Amaterasu.Database.Interfaces;
using Amaterasu.Database.Enums;
using Amaterasu.Database.Attributes;

namespace Amaterasu.Database
{
    public abstract class SQLBaseDatabaseContext<TConnection> : IDatabaseContext, ISqlDatabaseContext, IDatabaseContextWithConnectionProperty where TConnection : DbConnection
    {
        public ExpressionParser ExpressionParser { get; protected set; }

        public ExpressionToSQL ExpressionToSQL { get; protected set; }

        public TConnection Connection { get; protected set; }

        IDbConnection IDatabaseContextWithConnectionProperty.Connection => Connection;

        public IDatabaseCredential Credential { get; private set; } = new DatabaseCredential();

#if NET5_0_OR_GREATER
        public abstract int ExecuteNonQuery(string sql, Dictionary<string, object>? parameters = null);
#else
        public abstract int ExecuteNonQuery(string sql, Dictionary<string, object> parameters = null);
#endif

#if NET5_0_OR_GREATER
        public abstract IEnumerable<T> ExecuteQuery<T>(string query, Dictionary<string, object>? parameters = null) where T : class, new();
#else
        public abstract IEnumerable<T> ExecuteQuery<T>(string query, Dictionary<string, object> parameters = null) where T : class, new();
#endif

#if NET5_0_OR_GREATER
        public abstract void ExecuteQuery(string query, Action<IDataReader> action, Dictionary<string, object>? parameters = null);
#else
        public abstract void ExecuteQuery(string query, Action<IDataReader> action, Dictionary<string, object> parameters = null);
#endif

#if NET5_0_OR_GREATER
        public abstract IEnumerator<T> ExecuteQueryEnumerator<T>(string query, Dictionary<string, object>? parameters = null) where T : class, new();
#else
        public abstract IEnumerator<T> ExecuteQueryEnumerator<T>(string query, Dictionary<string, object> parameters = null) where T : class, new();
#endif
        public List<DataProcessTrigger> DataProcessTriggers { get; } = new List<DataProcessTrigger>();
        public SQLBaseDatabaseContext()
        {
            ExpressionParser = new ExpressionParser();
            ExpressionParser.LoadAvailableIExpressionParsers();
            ExpressionToSQL = new ExpressionToSQL();
        }

        protected void ProcessTrigger(DataTrigger trigger, object data)
        {
            var triggers = DataProcessTriggers.Where(x => x.Trigger == trigger);
            foreach (var trig in triggers)
            {
                trig.Action?.Invoke(data, this);
            }
        }

        public virtual IQueryBuilder<T> From<T>() where T : class, new()
        {
            var qb = new QueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            return qb;
        }

        public virtual IQueryBuilder<T> From<T>(string source) where T : class, new()
        {
            var qb = new QueryBuilder<T>();
            qb.DatabaseContext = this;
            qb.LINQ2SQL = ExpressionToSQL;
            qb.Parser = ExpressionParser;
            qb.Source = source;
            return qb;
        }

        protected virtual string GetSourceName<T>() where T : class, new()
        {
            var source = ModelHelper.GetSourceName<T>();
            if (source == null) throw new Exception("Model does not have Model attribute");
            return source;
        }

        public virtual void Truncate<T>() where T: class, new()
        {
            Truncate(GetSourceName<T>());
        }

        public virtual void Truncate(string source)
        {
            var sql = "TRUNCATE TABLE " + ExpressionToSQL.Quote(source);
            using (var cmd = CreateCommand())
            {
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
            }
        }

        public DbCommand CreateCommand()
        {
            var cmd = Connection.CreateCommand();
            if (CurrentTransaction != null)
                cmd.Transaction = CurrentTransaction;
            return cmd;
        }
        public TCommand CreateCommand<TCommand>() where TCommand : DbCommand
        {
            var cmd = Connection.CreateCommand();
            if (CurrentTransaction != null)
                cmd.Transaction = CurrentTransaction;
            return cmd as TCommand;
        }

        public virtual void Open()
        {
            Connection.Open();
        }

        public virtual void Close()
        {
            Connection.Close();
        }

        public void Save<T>(T data) where T: class, new()
        {
            if (Exists(data))
                Update(data);
            else
                Insert(data);
        }

#if NET5_0_OR_GREATER
        protected virtual object? GetLastInsertID<T>(T data) where T : class, new()
#else
        protected virtual object GetLastInsertID<T>(T data) where T : class, new()
#endif
        {
            return ModelHelper.GetKeyValue<T>(data) ?? null;
        }

        private void CheckDataValid<T>(T data)
        {
            var typ = typeof(T);
            var mdl = typ.GetCustomAttribute<ModelAttribute>(false);
            if (mdl != null && mdl.Validate && data != null)
            {
                var valResult = new List<ValidationResult>();
                if (!Validator.TryValidateObject(data, new ValidationContext(data), valResult))
                {
                    string reason = null;
                    if (valResult != null && valResult.Count > 0)
                    {
                        reason = valResult.First().ErrorMessage;
                    }
                    throw new Exception("Data validation failed" + (!string.IsNullOrWhiteSpace(reason) ? ": " + reason : "!"));
                }
            }
        }

        public virtual void Insert<T>(T data) where T : class, new()
        {
            CheckDataValid(data);
            var sql = "INSERT INTO " + ExpressionToSQL.Quote(GetSourceName<T>()) + " ";
            var pars = new Dictionary<string, object>();

            var flds = new List<string>();
            var vls = new List<string>();
            var mis = ModelHelper.GetMembers<T>();
            var cnt = 0;
            if (data is IPreSave pd)
                pd.OnPreSave(this, false);
            ProcessTrigger(DataTrigger.PreSave, data);
            foreach (var mi in mis)
            {
                var fld = "@f" + cnt.ToString().PadLeft(4, '0');
                var val = mi.GetValue(data);
                if (val != null || (mi.GetCustomAttribute<KeyAttribute>(false) == null && mi.GetCustomAttribute<PrimaryKeyAttribute>(false) == null))
                {
                    var mem = mi.GetCustomAttribute<MemberAttribute>(false);
                    pars.Add(fld, val ?? DBNull.Value);
                    flds.Add(ExpressionToSQL.Quote(mem?.Name ?? mi.Name));
                    vls.Add(fld);
                }
                cnt++;
            }
            sql += "(" + string.Join(", ", flds) + ") VALUES (" + string.Join(", ", vls) + ")";
            BeginReport(sql, pars);
            var ret = ExecuteNonQuery(sql, pars);
            EndReport(true, ret);
            if (data is IPostSave td)
            {
                object idval = GetLastInsertID(data);
                // try to set the key
                if (idval != null)
                {
                    try
                    {
                        var keymem = mis.Where(x => x.GetCustomAttribute<KeyAttribute>(false) != null || x.GetCustomAttribute<PrimaryKeyAttribute>(false) != null).First();
                        keymem.SetValue(data, idval);
                    }
                    catch { }
                }
                td.OnPostSave(this, false, idval);
            }
            ProcessTrigger(DataTrigger.PostSave, data);
        }
        public virtual void Update<T>(T data) where T : class, new()
        {
            CheckDataValid(data);
            var sql = "UPDATE " + ExpressionToSQL.Quote(GetSourceName<T>()) + " SET ";
            var pars = new Dictionary<string, object>();

            var flds = new List<string>();
            var mis = ModelHelper.GetMembers<T>();
            var cnt = 0;
            var key = ModelHelper.GetKeyName<T>();
            if (data is IPreSave pd)
                pd.OnPreSave(this, false);
            ProcessTrigger(DataTrigger.PreSave, data);
            foreach (var mi in mis)
            {
                if (mi.Name == key) continue;
                var fld = "@f" + cnt.ToString().PadLeft(4, '0');
                pars.Add(fld, mi.GetValue(data) ?? DBNull.Value);
                flds.Add(ExpressionToSQL.Quote(mi.Name) + " = " + fld);
                cnt++;
            }
            sql += string.Join(", ", flds);

            pars.Add("@id", ModelHelper.GetKeyValue<T>(data));
            sql += " WHERE " + ExpressionToSQL.Quote(key) + " = @id";
            BeginReport(sql, pars);
            var ret = ExecuteNonQuery(sql, pars);
            EndReport(true, ret);
            if (data is IPostSave td)
                td.OnPostSave(this, true, null);
            ProcessTrigger(DataTrigger.PostSave, data);
        }
        public virtual void Delete<T>(T data) where T : class, new()
        {
            var idname = ModelHelper.GetKeyName<T>();
            if (idname == null) throw new Exception("Model does not have member with Key attribute");
            var idval = ModelHelper.GetKeyValue<T>(data);
            if (data is IPreDelete pd)
                pd.OnPreDelete(this);
            ProcessTrigger(DataTrigger.PreDelete, data);
            DeleteWhere<T>(idname, idval);
            if (data is IPostDelete ps)
                ps.OnPostDelete(this);
            ProcessTrigger(DataTrigger.PostDelete, data);
        }

#if NET5_0_OR_GREATER
        public virtual void UpdateWhere<T>(Dictionary<string, object> set, Expression<Func<T, bool>> expression, string? source = null) where T : class, new()
#else
        public virtual void UpdateWhere<T>(Dictionary<string, object> set, Expression<Func<T, bool>> expression, string source = null) where T : class, new()
#endif
        {
            var nd = ExpressionParser.Parse(expression);
            var tree = ExpressionToSQL.ToWhereClause(nd);
            var sql = "UPDATE " + ExpressionToSQL.Quote(source ?? GetSourceName<T>()) + " SET ";

            var typ = set.GetType();
            var mis = typ.GetMembers().Where(x => (x.MemberType == System.Reflection.MemberTypes.Property || x.MemberType == System.Reflection.MemberTypes.Field) && x.Readable());
            var flds = new List<string>();
            var pars = new Dictionary<string, object>();
            foreach (var mi in mis)
            {
                var val = mi.GetValue(set);
                flds.Add(ExpressionToSQL.Quote(mi.Name) + " = @" + mi.Name);
                pars.Add("@" + mi.Name, val ?? DBNull.Value);
            }

            sql += string.Join(", ", flds);

            if (tree?.Parameters != null)
            {
                foreach (var kv in tree.Parameters)
                {
                    pars.Add(kv.Key, kv.Value);
                }
            }
            if (!string.IsNullOrWhiteSpace(tree?.Where))
            {
                sql += " WHERE " + tree.Where;
            }
            BeginReport(sql, pars);
            var ret = ExecuteNonQuery(sql, pars);
            EndReport(true, ret);
        }

        public virtual int DeleteWhere<T>(Expression<Func<T, bool>> expression) where T : class, new()
        {
            var nd = ExpressionParser.Parse(expression);
            var tree = ExpressionToSQL.ToWhereClause(nd);
            var sql = "DELETE FROM " + ExpressionToSQL.Quote(GetSourceName<T>());
            if (!string.IsNullOrWhiteSpace(tree?.Where))
            {
                sql += " WHERE " + tree.Where;
            }
            BeginReport(sql, tree?.Parameters);
            var ret = ExecuteNonQuery(sql, tree?.Parameters);
            EndReport(true, ret);
            return ret;
        }

        public virtual int DeleteWhere<T>(string fieldName, object value) where T : class, new()
        {
            var sql = "DELETE FROM " + ExpressionToSQL.Quote(ModelHelper.GetSourceName<T>()) + " WHERE " + ExpressionToSQL.Quote(fieldName) + " = @par";
            var par = new Dictionary<string, object>() { { "@par", value } };
            BeginReport(sql, par);
            var ret = ExecuteNonQuery(sql, par);
            EndReport(true, ret);
            return ret;
        }

        public bool Exists<T>(T data) where T : class, new()
        {
            var keyName = ModelHelper.GetKeyName<T>();
            if (string.IsNullOrWhiteSpace(keyName)) throw new Exception("PrimaryKey attribute is not set for '" + typeof(T).FullName + "'");
            var sql = "SELECT COUNT(*) AS cnt FROM " + ExpressionToSQL.Quote(GetSourceName<T>()) + " ";
            sql += " WHERE " + ExpressionToSQL.Quote(keyName) + " = @id";
            var pars = new Dictionary<string, object>();
            pars.Add("@id", ModelHelper.GetKeyValue<T>(data) ?? DBNull.Value);
            var exists = false;
            BeginReport(sql, pars);
            ExecuteQuery(sql, (rdr) =>
            {
                if (rdr.Read())
                    exists = rdr.GetInt32(0) > 0;
            }, pars);
            EndReport(true, exists);
            return exists;
        }

        public abstract IDataReader ExecuteReader(string query, Dictionary<string, object> parameters = null);

        public SQLCommandReport LastCommand { get; protected set; }

        protected void BeginReport(string query = null, Dictionary<string, object> parameters = null)
        {
            if (query == null)
            {
                LastCommand = null;
                return;
            }
            LastCommand = new SQLCommandReport()
            {
                Query = query,
                Parameters = parameters
            };
        }

        protected void EndReport(bool success, object returnValue = null)
        {
            if (LastCommand != null)
            {
                LastCommand.IsExecuted = true;
                LastCommand.IsSuccess = success;
                LastCommand.ReturnValue = returnValue;
            }
        }

        protected DbTransaction CurrentTransaction { get; set; }
        public void Transaction(Action<IDatabaseContext> transaction, IsolationLevel? isolationLevel = null)
        {
            using(var trx = isolationLevel != null ? Connection.BeginTransaction(isolationLevel.Value) : Connection.BeginTransaction())
            {
                try
                {
                    CurrentTransaction = trx;
                    transaction.Invoke(this);

                    trx.Commit();
                }
                catch(Exception ex)
                {
                    trx.Rollback();
                    throw new Exception("Database transaction failed, see inner exception for detail.", ex);
                }
                finally
                {
                    CurrentTransaction = null;
                }
            }
        }

        public void Open(IDatabaseCredential credential)
        {
            Credential = credential;
            Open();
        }
    }

    public class SQLCommandReport
    {
        public bool IsExecuted { get; set; }

        public bool IsSuccess { get; set; }

        public string Query { get; set; }

        public Dictionary<string, object> Parameters { get; set; }

        public object ReturnValue { get; set; }
    }
}
