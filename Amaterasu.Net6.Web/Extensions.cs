﻿using Amaterasu.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;

namespace Amaterasu.Web
{
    public static class Extensions
    {
        private static List<Type> _databaseContextTypes { get; } = new List<Type>();

        /// <summary>
        /// Register <see cref="IDatabaseContext"/> to <see cref="DatabaseManager"/>
        /// </summary>
        /// <typeparam name="TDatabaseContext">Must inherit <see cref="IDatabaseContext"/> and must be a <see cref="class"/></typeparam>
        public static IServiceCollection RegisterDatabaseContext<TDatabaseContext>(this IServiceCollection services) where TDatabaseContext : class, IDatabaseContext
        {
            _databaseContextTypes.Add(typeof(TDatabaseContext));
            return services;
        }

        /// <summary>
        /// Adds <see cref="DatabaseManager"/> and configure it using custom function
        /// </summary>
        /// <param name="configure">Configuration function</param>
        public static IServiceCollection AddDatabaseManager(this IServiceCollection services, Action<IDatabaseManager> configure)
        {
            var dbManager = new DatabaseManager();
            configure(dbManager);
            services.AddSingleton(dbManager);
            return services;
        }

        /// <summary>
        /// Adds <see cref="DatabaseManager"/> and configure it using default configuration
        /// </summary>
        /// <param name="configuration">Configuration manager</param>
        /// <param name="configurationPath">Configuration path</param>
        /// <exception cref="Exception"></exception>
        public static IServiceCollection AddDatabaseManager(this IServiceCollection services, ConfigurationManager configuration, string configurationPath = "Databases")
        {
            var dbManager = new DatabaseManager();
            var cfgRoot = configuration.GetSection(configurationPath);

            foreach(var config in cfgRoot.GetChildren())
            {
                var name = config.Key;
                var credentials = config.GetChildren();

                var engine = config["Engine"];
                if (string.IsNullOrWhiteSpace(engine))
                {
                    throw new Exception($"Database configuration invalid: '{config.Path}' must have Engine property");
                }
                engine = engine.ToLowerInvariant().Trim();

                var matchingEngine = _databaseContextTypes
                    .FirstOrDefault(x => 
                        x.Name.ToLower().Replace("databasecontext", "").Contains(engine)
                    );
                if (matchingEngine == null)
                {
                    throw new Exception($"Database engine is not loaded or invalid: '{engine}'");
                }

                var dbContext = Activator.CreateInstance(matchingEngine) as IDatabaseContext;
                if (dbContext == null)
                {
                    throw new Exception($"Database engine failed to instantiate: '{matchingEngine.Name}'");
                }
                var cstring = config["ConnectionString"] ?? config["ConnString"] ?? config["Cstring"] ?? null;
                if (!string.IsNullOrWhiteSpace(cstring))
                {
                    dbContext.Credential.ConnectionString = cstring;
                }
                else
                {
                    dbContext.Credential.Host = config["Host"] ?? config["DataSource"] ?? config["Server"];
                    dbContext.Credential.User = config["User"] ?? config["Username"] ?? config["Usr"];
                    dbContext.Credential.Password = config["Password"] ?? config["Pwd"];
                }
                dbContext.Credential.Database = config["Database"] ?? config["Name"];

                dbContext.Open();
                dbManager.Add(name, dbContext);
            }
            services.AddSingleton<IDatabaseManager>(dbManager);
            return services;
        }
    }
}