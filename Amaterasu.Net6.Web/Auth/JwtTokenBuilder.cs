﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Web.Auth
{
    public static class JwtTokenBuilder
    {
        private static ConfigurationManager? configuration { get; set; }

        public static void SetConfigurationManager(ConfigurationManager? configurationManager)
        {
            configuration = configurationManager;
        }

        public static string? CreateToken(Dictionary<string,object> claims)
        {
            var tokenDescriptor = new SecurityTokenDescriptor();

            tokenDescriptor.Claims = claims;

            var expire = configuration?["Jwt:Expire"] ?? "259200";
            tokenDescriptor.Expires = DateTime.UtcNow.AddSeconds(int.Parse(expire));
            tokenDescriptor.Issuer = configuration?["Jwt:Issuer"];
            tokenDescriptor.Audience = configuration?["Jwt:Audience"];

            if (!string.IsNullOrWhiteSpace(configuration?["Jwt:SigningKey"]))
            {
                tokenDescriptor.SigningCredentials = new SigningCredentials(
                    new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(configuration["Jwt:SigningKey"])
                        ), 
                    SecurityAlgorithms.HmacSha512Signature
                    );
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var jwtToken = tokenHandler.WriteToken(token);
            return jwtToken;
        }
    }
}
