﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace Amaterasu.Web.Auth
{
    public static class AuthExtensions
    {
        public static IServiceCollection AddAmaterasuJWTAuthentication(this IServiceCollection services, ConfigurationManager configuration, string configurationPath = "Jwt")
        {
            var config = configuration.GetSection(configurationPath);
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(o =>
            {
                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = config["Issuer"],
                    ValidateIssuer = !string.IsNullOrWhiteSpace(config["Issuer"]),

                    IssuerSigningKey = !string.IsNullOrWhiteSpace(config["SigningKey"]) ? new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["SigningKey"])) : null,
                    ValidateIssuerSigningKey = !string.IsNullOrWhiteSpace(config["SigningKey"]),

                    ValidAudience = config["Audience"],
                    ValidateAudience = !string.IsNullOrWhiteSpace(config["Audience"]),

                    ValidateLifetime = config["ValidateLifetime"] == "true",
                };
            });
            JwtTokenBuilder.SetConfigurationManager(configuration);
            return services;
        }
    }
}
