﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Amaterasu.Web.API
{

    public class APIResult
    {
        public virtual APIResultMeta Meta { get; } = new APIResultMeta();

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public APIMessages? Errors { get; set; }

        public static APIResult Success(string? message = null)
        {
            var res = new APIResult();
            res.Meta.Message = message;
            return res;
        }

        public static APIResult<T> Success<T>(T data, string? message = null)
        {
            var res = new APIResult<T>();
            res.Meta.Message = message;
            res.Data = data;
            return res;
        }

        public static APIResult Error(HttpStatusCode statusCode, string? message = null)
        {
            return Error((int)statusCode, message);
        }

        public static APIResult Error(HttpStatusCode statusCode, APIMessages errors, string? message = null)
        {
            return Error((int)statusCode, errors, message);
        }

        public static APIResult Error(int statusCode, string? message = null)
        {
            var res = new APIResult();
            res.Meta.Code = statusCode;
            res.Meta.Message = message;
            return res;
        }

        public static APIResult Error(int statusCode, APIMessages errors, string? message = null)
        {
            var res = new APIResult();
            res.Meta.Code = statusCode;
            res.Meta.Message = message;
            res.Errors = errors;
            return res;
        }

        public static APIResult<T> Dataset<T>(T dataset, int? total = null, int? page = null, int? take = null) where T : IEnumerable
        {
            var res = new APIResult<T>();
            res.Data = dataset;
            res.Meta.Total = total;
            res.Meta.Page = page;
            res.Meta.Take = take;
            return res;
        }
    }

    public class APIResultMeta
    {
        public int Code { get; set; } = 200;

        public string? Message { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public int? Total { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public int? Page { get; set; }

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public int? Take { get; set; }
    }

    public class APIResult<TData> : APIResult
    {
        public TData? Data { get; set; }
    }

    public class APIMessages : Dictionary<string, string>
    {

    }
}
