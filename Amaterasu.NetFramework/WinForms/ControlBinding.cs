﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amaterasu.NetFramework.WinForms
{
    /// <summary>
    /// Provides a loose binding for windows form
    /// </summary>
    public static class ControlBinding
    {
        /// <summary>
        /// Reads values from model and apply it to control. Binding tag is $<property_name>
        /// </summary>
        public static void ReadValue<T>(Control control, T model) where T : class
        {
            var tag = ControlBindingTag.ReadFrom(control);
            if (tag.Ignore) return;

            if (!string.IsNullOrWhiteSpace(tag.ModelPropertyName))
            {
                var modelType = typeof(T);
                var modelProperty = modelType.GetProperty(tag.ModelPropertyName);
                if (modelProperty != null && modelProperty.CanRead)
                {
                    var customAttr = modelProperty.GetCustomAttribute<ControlBindingWriteOnlyAttribute>(false);
                    if (customAttr == null)
                    {
                        var val = modelProperty.GetValue(model);
                        ControlValueManipulator.SetValue(control, val);
                    }
                }
            }

            if (control.HasChildren)
            {
                foreach (Control c in control.Controls)
                {
                    ReadValue(c, model);
                }
            }
        }

        /// <summary>
        /// Read values from a control and write it to model. Binding tag is $<property_name>
        /// </summary>
        public static void WriteValue<T>(Control control, T model) where T : class
        {
            var tag = ControlBindingTag.ReadFrom(control);
            if (tag.Ignore) return;

            if (!string.IsNullOrWhiteSpace(tag.ModelPropertyName))
            {
                var modelType = typeof(T);
                var modelProperty = modelType.GetProperty(tag.ModelPropertyName);
                if (modelProperty != null && modelProperty.CanWrite)
                {
                    var customAttr = modelProperty.GetCustomAttribute<ControlBindingReadOnlyAttribute>(false);
                    if (customAttr == null)
                    {
                        var val = ControlValueManipulator.GetValue(control, modelProperty.PropertyType);
                        modelProperty.SetValue(model, val);
                    }
                }
            }

            if (control.HasChildren)
            {
                foreach (Control c in control.Controls)
                {
                    WriteValue(c, model);
                }
            }
        }

        /// <summary>
        /// Reads values from model and apply it this control. Binding tag is $<property_name>
        /// </summary>
        public static void ReadValueFrom<T>(this Control control, T model) where T : class
        {
            ReadValue(control, model);
        }

        /// <summary>
        /// Read values from this control and write it to model. Binding tag is $<property_name>
        /// </summary>
        public static void WriteValueTo<T>(this Control control, T model) where T : class
        {
            WriteValue(control, model);
        }
    }

    public static class ControlValueManipulator
    {
        /// <summary>
        /// Set value of a control.
        /// </summary>
        public static void SetValue<T>(Control control, T value)
        {
            RegisterInternalReadersAndWriters();

            var supported = Writers.Where(x => x.CanProcess(control.GetType(), value?.GetType() ?? typeof(T))).ToList();
            supported.Reverse();
            var success = false;
            foreach(var s in supported)
            {
                try
                {
                    s.Write(control, value);
                    success = true;
                    break;
                }
                catch { }
            }
            if (!success)
                throw new Exception("No ControlValueReader that support writing '" + (typeof(T)?.Name ?? "object") + "' to '" + (control?.GetType()?.Name ?? "null") + "'");
        }

        /// <summary>
        /// Get value from a control.
        /// </summary>
        public static object GetValue(Control control, Type expectedType = null)
        {
            RegisterInternalReadersAndWriters();

            var supported = Readers.Where(x => x.CanProcess(control.GetType(), expectedType)).ToList();
            supported.Reverse();
            foreach (var s in supported)
            {
                try
                {
                    return s.Read(control, expectedType);
                }
                catch { }
            }
            throw new Exception("No ControlValueReader that support reading '" + (control?.GetType()?.Name ?? "null" ) + "' as '" + (expectedType?.Name ?? "object") + "'");
        }

        public static List<ControlValueWriter> Writers { get; } = new List<ControlValueWriter>();
        public static List<ControlValueReader> Readers { get; } = new List<ControlValueReader>();

        private static bool _internalRegistered = false;
        private static void RegisterInternalReadersAndWriters()
        {
            if (_internalRegistered) return;
            _internalRegistered = true;

            Writers.Add(new GenericControlWriter());
            Readers.Add(new GenericControlReader());

            Writers.Add(new NumericUpDownWriter());
            Readers.Add(new NumericUpDownReader());

            Writers.Add(new ComboBoxWriter());
            Readers.Add(new ComboBoxReader());

            Writers.Add(new CheckRadioWriter());
            Readers.Add(new CheckRadioReader());
        }
    }

    public abstract class ControlValueReadWriteBase
    {
        public abstract bool CanProcess(Type controlType, Type valueType = null);
    }

    public abstract class ControlValueWriter: ControlValueReadWriteBase
    {
        public abstract void Write<T>(Control control, T value);
    }

    public abstract class ControlValueReader: ControlValueReadWriteBase
    {
        public abstract object Read(Control control, Type expectedType = null);
    }

    public class ControlBindingTag
    {
        /// <summary>
        /// Gets or sets the name of model propery that bounds to this control.
        /// </summary>
        public string ModelPropertyName { get; set; }

        /// <summary>
        /// Gets or sets a boolean value that indicates this control should be ignored or not by the control binding.
        /// </summary>
        public bool Ignore { get; set; }

        public static ControlBindingTag ReadFrom(Control control)
        {
            var res = new ControlBindingTag();

            if (control != null && control.Tag is string tag)
            {
                var spl = tag.Split(new char[] { ' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                var binding = spl.FirstOrDefault(x => x.StartsWith("$"))?.Substring(1).Trim();
                if (!string.IsNullOrWhiteSpace(binding))
                    res.ModelPropertyName = binding;

                res.Ignore = spl.Any(x => x?.Trim().ToLower() == "ignore");
            }

            return res;
        }
    }

    /// <summary>
    /// When applied to a property, ignores WriteValue<T> operation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ControlBindingReadOnlyAttribute: Attribute
    {

    }

    /// <summary>
    /// When applied to a property, ignores ReadValue<T> operation.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ControlBindingWriteOnlyAttribute : Attribute
    {

    }
}
