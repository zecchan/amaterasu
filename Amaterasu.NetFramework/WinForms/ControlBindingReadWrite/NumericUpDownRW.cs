﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amaterasu.NetFramework.WinForms
{
    public class NumericUpDownReader : ControlValueReader
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return typeof(NumericUpDown).IsAssignableFrom(controlType);
        }

        public override object Read(Control control, Type expectedType = null)
        {
            var cControl = control as NumericUpDown;
            if (cControl == null) throw new Exception();

            if (expectedType == null || expectedType.IsAssignableFrom(typeof(decimal)))
                return cControl.Value;

            if (expectedType.IsAssignableFrom(typeof(string)))
                return cControl.Value.ToString();

            if (expectedType.IsAssignableFrom(typeof(double)))
                return (double)cControl.Value;

            if (expectedType.IsAssignableFrom(typeof(float)))
                return (float)cControl.Value;

            if (expectedType.IsAssignableFrom(typeof(long)))
                return (long)cControl.Value;

            if (expectedType.IsAssignableFrom(typeof(int)))
                return (int)cControl.Value;

            if (expectedType.IsAssignableFrom(typeof(short)))
                return (short)cControl.Value;

            if (expectedType.IsAssignableFrom(typeof(byte)))
                return (byte)cControl.Value;

            throw new Exception();
        }
    }

    public class NumericUpDownWriter : ControlValueWriter
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return typeof(NumericUpDown).IsAssignableFrom(controlType);
        }

        public override void Write<T>(Control control, T value)
        {
            var cControl = control as NumericUpDown;
            if (cControl == null) throw new Exception();
            cControl.Value = Convert.ToDecimal(value);
        }
    }
}
