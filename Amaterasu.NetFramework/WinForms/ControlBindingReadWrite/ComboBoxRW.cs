﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amaterasu.NetFramework.WinForms
{

    public class ComboBoxReader : ControlValueReader
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return typeof(ComboBox).IsAssignableFrom(controlType);
        }

        public override object Read(Control control, Type expectedType = null)
        {
            var cControl = control as ComboBox;
            if (cControl == null) throw new Exception();

            if (expectedType == null || typeof(object) == expectedType)
                return cControl.SelectedItem;

            if (expectedType.IsAssignableFrom(typeof(string)))
                return cControl.Text;

            if (expectedType.IsAssignableFrom(typeof(int)))
                return cControl.SelectedIndex;

            if (expectedType.IsEnum)
            {
                var vals = Enum.GetValues(expectedType);
                foreach (var v in vals)
                {
                    try
                    {
                        if ((int)v == cControl.SelectedIndex || v.ToString() == cControl.Text)
                            return v;
                    }
                    catch { }
                }
            }

            throw new Exception();
        }
    }

    public class ComboBoxWriter : ControlValueWriter
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return typeof(ComboBox).IsAssignableFrom(controlType);
        }

        public override void Write<T>(Control control, T value)
        {
            var cControl = control as ComboBox;
            if (cControl == null) throw new Exception();

            var vType = value?.GetType() ?? typeof(T);
            if (vType.IsEnum)
            {
                cControl.Text = value.ToString();
                if (cControl.SelectedIndex < 0)
                {
                    var val = Enum.GetValues(vType);
                    foreach (var v in val)
                        if (v.Equals(value))
                            cControl.SelectedIndex = (int)v;
                }
            }
            if (value is int i)
                cControl.SelectedIndex = i;
            else if (value is string s)
                cControl.Text = s;
            else
                cControl.SelectedItem = value;
        }
    }
}
