﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amaterasu.NetFramework.WinForms
{
    public class CheckRadioReader : ControlValueReader
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return (typeof(CheckBox).IsAssignableFrom(controlType) || typeof(RadioButton).IsAssignableFrom(controlType)) && valueType == typeof(bool);
        }

        public override object Read(Control control, Type expectedType = null)
        {
            if (expectedType == typeof(bool))
            {
                if (control is CheckBox cb)
                {
                    return cb.Checked;
                }
                if (control is RadioButton rb)
                {
                    return rb.Checked;
                }
            }

            throw new Exception();
        }
    }

    public class CheckRadioWriter : ControlValueWriter
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return (typeof(CheckBox).IsAssignableFrom(controlType) || typeof(RadioButton).IsAssignableFrom(controlType)) && valueType == typeof(bool);
        }

        public override void Write<T>(Control control, T value)
        {
            if (value is bool b)
            {
                if (control is CheckBox cb)
                {
                    cb.Checked = b;
                    return;
                }
                if (control is RadioButton rb)
                {
                    rb.Checked = b;
                    return;
                }
            }
            throw new Exception();
        }
    }
}
