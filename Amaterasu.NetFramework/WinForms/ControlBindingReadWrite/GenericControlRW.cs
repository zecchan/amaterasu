﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amaterasu.NetFramework.WinForms
{
    public class GenericControlReader : ControlValueReader
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return typeof(Control).IsAssignableFrom(controlType) 
                && (valueType == null || valueType.IsAssignableFrom(typeof(string)));
        }

        public override object Read(Control control, Type expectedType = null)
        {
            if (expectedType == null || expectedType.IsAssignableFrom(typeof(string)))
                return control.Text;

            if (expectedType.IsAssignableFrom(typeof(decimal)))
                return decimal.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(double)))
                return double.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(float)))
                return float.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(long)))
                return long.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(int)))
                return int.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(short)))
                return short.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(byte)))
                return byte.Parse(control.Text);

            if (expectedType.IsAssignableFrom(typeof(DateTime)))
                return DateTime.Parse(control.Text);

            throw new Exception();
        }
    }

    public class GenericControlWriter : ControlValueWriter
    {
        public override bool CanProcess(Type controlType, Type valueType = null)
        {
            return typeof(Control).IsAssignableFrom(controlType);
        }

        public override void Write<T>(Control control, T value)
        {
            control.Text = value?.ToString();
        }
    }
}
