﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Amaterasu.Database
{
    public class DatabaseConfig : IDatabaseConfig
    {
        [JsonPropertyName("engine")]
        public string Engine { get; set; } = string.Empty;

        [JsonPropertyName("credential")]
        public virtual JSONDatabaseCredential Credential { get; set; } = new JSONDatabaseCredential();
        IDatabaseCredential IDatabaseConfig.Credential { 
            get => Credential; 
            set => Credential = (JSONDatabaseCredential)value;
        }
    }
    public class JSONDatabaseCredential : DatabaseCredential
    {
        [JsonPropertyName("host")]
        public override string Host { get; set; } = string.Empty;

        [JsonPropertyName("port")]
        public override int? Port { get; set; }

        [JsonPropertyName("user")]
        public override string User { get; set; } = string.Empty;

        [JsonPropertyName("password")]
        public override string Password { get; set; } = string.Empty;

        [JsonPropertyName("database")]
        public override string Database { get; set; } = string.Empty;

        [JsonPropertyName("connectionString")]
        public override string ConnectionString { get; set; } = string.Empty;
    }

    public static class DatabaseManagerDatabaseConfigExtension
    {
        public static void LoadFromConfig(this DatabaseManager databaseManager, string id, DatabaseConfig config)
        {
            var engineId = config.Engine;
            var dbEngine = databaseManager.GetEngineTypeFromId(engineId);

            if (dbEngine != null)
            {
                databaseManager.LoadDatabase(id, dbEngine, config.Credential);
            }
        }
    }
}
