﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami
{
    /// <summary>
    /// Base interface for any Izanami App class
    /// </summary>
    public interface IApplication
    {
        /// <summary>
        /// Gets the id of this app
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Gets the name of this app
        /// </summary>
        string Name { get; }  

        /// <summary>
        /// Gets the version of this app
        /// </summary>
        Version Version { get; }

        /// <summary>
        /// Gets the public key of this app
        /// </summary>
        string PublicKey { get; }

        /// <summary>
        /// Get the shared secret for this app
        /// </summary>
        ApplicationSecret Secret { get; }

        /// <summary>
        /// Initialize application for running
        /// </summary>
        void Initialize(IConfiguration configuration, ApplicationBootstrapper bootstrapper);
    }
}
