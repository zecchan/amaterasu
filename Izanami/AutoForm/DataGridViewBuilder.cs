﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public class DataGridViewBuilder
    {
        public IReadOnlyList<IDataGridViewColumnBuilder> Columns { get => columns; }
        List<IDataGridViewColumnBuilder> columns = new List<IDataGridViewColumnBuilder>();

        public bool ReadOnly { get; set; }
        public bool Sizeable { get; set; }
        public bool Multiselect { get; set; }

        public DataGridViewColumnBuilder AddColumn(string header, int width = -1, string? format = null)
        {
            var col = new DataGridViewColumnBuilder()
            {
                HeaderText = header,
                Width = width,
                Format = format,
            };
            columns.Add(col);
            return col;
        }

        public DataGridViewColumnBuilder AddColumn<T>(string header, Func<T, object?> getter, int width = -1, string? format = null)
        {
            var col = new DataGridViewColumnBuilder<T>()
            {
                HeaderText = header,
                Width = width,
                Format = format,
                ValueGetter = getter,
            };
            columns.Add(col);
            return col;
        }

        public DataGridViewBuilder AddColumn(IDataGridViewColumnBuilder columnBuilder)
        {
            columns.Add(columnBuilder);
            return this;
        }

        public void Apply(DataGridView dataGridView)
        {
            dataGridView.Columns.Clear();
            dataGridView.AllowUserToResizeColumns =
            dataGridView.AllowUserToResizeRows = Sizeable;

            dataGridView.MultiSelect = Multiselect;
            dataGridView.RowHeadersVisible = false;
            dataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dataGridView.ReadOnly = ReadOnly;
            dataGridView.AllowUserToDeleteRows =
            dataGridView.AllowUserToAddRows = !ReadOnly;

            foreach (var col in Columns)
            {
                var dgvc = new DataGridViewColumn()
                {
                    HeaderText = col.HeaderText,
                    ReadOnly = ReadOnly,
                    ValueType = col.ValueType,
                };
                if (col.Format != null)
                {
                    dgvc.DefaultCellStyle.Format = col.Format;
                } 
                if (col.Width > 0)
                {
                    dgvc.Width = col.Width;
                }
                if (col.Sortable)
                {
                    dgvc.SortMode = DataGridViewColumnSortMode.Automatic;
                }
                dgvc.Tag = col;

                // NYI
                dgvc.CellTemplate = new DataGridViewTextBoxCell();

                dataGridView.Columns.Add(dgvc);
            }
        }
    }
}
