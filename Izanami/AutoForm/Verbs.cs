﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public static class ActionVerbs
    {
        public const string Create = "create";
        public const string Save = "save";
        public const string Search = "search";
        public const string SearchText = "text";
    }

    public static class TagKeys
    {
        public const string Verb = "af:verb";
        public const string SearchFilter = "af:filter";
        public const string Grid = "af:grid";
        public const string Input = "af:input";
        public const string Validation = "v:";
    }
}
