﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public class FormController<TForm> : IFormController<TForm> where TForm : Form
    {
        public TForm Form { get => _form ?? throw new Exception("Form is not bound"); set => _form = value; }
        TForm? _form;

        public ReadOnlyDictionary<Control, Tags> Refs { get => new ReadOnlyDictionary<Control, Tags>(refs); }

        Dictionary<Control, Tags> refs = new Dictionary<Control, Tags>();

        public static FormController<T> Create<T>(T form) where T : Form
        {
            var controller = new FormController<T>();
            controller.Bind(form);
            return controller;
        }

        public ErrorProvider ErrorProvider { get; } = new ErrorProvider();

        bool bound = false;
        public void Bind(TForm form)
        {
            if (bound)
            {
                throw new Exception("Cannot bind a controller twice!");
            }
            bound = true;
            Form = form;
            refs = new Dictionary<Control, Tags>();
            BuildReferences(form);

            ControlBinder.BindActions(this);
            ControlBinder.BindDataGridView(this);
        }

        public IEnumerable<Control> Find(Func<Tags, bool> filter)
        {
            return Refs.Where(x => filter(x.Value)).Select(x => x.Key);
        }

        public IEnumerable<T> Find<T>(Func<Tags, bool> filter) where T : Control
        {
            return Refs.Where(x => x.Key is T && filter(x.Value)).Select(x => x.Key).OfType<T>();
        }

        public Control? FindFirst(Func<Tags, bool> filter)
        {
            return Find(filter).FirstOrDefault();
        }

        public T? FindFirst<T>(Func<Tags, bool> filter) where T : Control
        {
            return Find<T>(filter).FirstOrDefault();
        }

        private void BuildReferences(Control control)
        {
            Tags? tags = null;
            if (control.Tag is string tag)
            {
                tags = new Tags(tag);
            }
            else if (!(control.Tag is BindingContext))
            {
                tags = new Tags();
            }

            if (tags != null)
            {
                refs.Add(control, tags);
                var binding = new BindingContext(this, tags);
                if (tags.ContainsKey(TagKeys.Input))
                {
                    binding.ErrorProvider = ErrorProvider;
                }
                control.Tag = binding;
            }

            foreach (var child in control.Controls.OfType<Control>())
            {
                if (child != null)
                {
                    BuildReferences(child);
                }
            }
        }

        public DataFilter? BuildSearchFilter()
        {
            return null;
        }
    }
}
