﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public struct BindingContext
    {
        public IFormController Controller { get; }

        public Tags Tags { get; }

        public ErrorProvider? ErrorProvider { get; set; }

        public BindingContext(IFormController controller, Tags tags)
        {
            Controller = controller; 
            Tags = tags;
            ErrorProvider = null;
        }
    }
}
