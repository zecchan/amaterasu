﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public interface IFormWithController
    {
        IFormController Controller { get; }
    }

    public interface IFormWithController<TController> : IFormWithController where TController : IFormController
    {
        new TController Controller { get; }
    }
}
