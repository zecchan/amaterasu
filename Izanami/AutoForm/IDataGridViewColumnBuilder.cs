﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public interface IDataGridViewColumnBuilder
    {
        string HeaderText { get; set; }

        string? Format { get; set; }

        Type? ValueType { get; set; }

        int Width { get; set; }

        bool Sortable { get;set; }

        object? GetValue(object data);
    }
}
