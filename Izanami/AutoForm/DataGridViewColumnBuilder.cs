﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public class DataGridViewColumnBuilder: IDataGridViewColumnBuilder
    {
        public string HeaderText { get; set; } = string.Empty;
        
        public string? Format { get; set; }

        public Type? ValueType { get; set; }

        public int Width { get; set; } = -1;

        public bool Sortable { get; set; } = true;

        public virtual object? GetValue(object data)
        {
            return null;
        }
    }

    public class DataGridViewColumnBuilder<TData>: DataGridViewColumnBuilder
    {
        public Func<TData, object?>? ValueGetter { get; set; }

        public override object? GetValue(object data)
        {
            if (data is TData td)
            {
                return ValueGetter?.Invoke(td);
            }
            return null;
        }
    }
}
