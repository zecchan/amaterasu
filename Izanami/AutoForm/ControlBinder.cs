﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Izanami.AutoForm
{
    public static class ControlBinder
    {
        public static void BindActions(IFormController controller)
        {
            var form = controller.FindFirst<Form>(x => true);
            if (form != null)
            {
                form.Load += Form_Load;
            }

            foreach (var btnSearch in controller.Find<Button>(x => x.Has(TagKeys.Verb, ActionVerbs.Search)))
            {
                btnSearch.Click += BtnSearch_Click;
            }
            foreach (var btnCreate in controller.Find<Button>(x => x.Has(TagKeys.Verb, ActionVerbs.Create)))
            {
                btnCreate.Click += BtnCreate_Click;
            }
            foreach (var btnSave in controller.Find<Button>(x => x.Has(TagKeys.Verb, ActionVerbs.Save)))
            {
                btnSave.Click += BtnSave_Click;
            }
            foreach (var cSearchFilter in controller.Find(x => x.ContainsKey(TagKeys.SearchFilter)))
            {
                cSearchFilter.KeyPress += CSearchFilter_KeyPress;
            }
        }

        private static void CSearchFilter_KeyPress(object? sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' && sender is Control c && c.Tag is BindingContext context)
            {
                if (PerformSearch(context.Controller))
                {
                    e.Handled = true;
                }
            }
        }

        private static void Form_Load(object? sender, EventArgs e)
        {
            if (sender is Form form && form.Tag is BindingContext context)
            {
                PerformSearch(context.Controller);
            }
        }

        public static object? GetValue(Control control)
        {
            if (control is TextBox textBox)
            {
                return textBox.Text;
            }
            if (control is DateTimePicker timePicker)
            {
                return timePicker.Value;
            }
            return null;
        }

        public static void BindDataGridView(IFormController controller)
        {
            var dgv = controller.FindFirst<DataGridView>(x => x.ContainsKey(TagKeys.Grid));
            if (dgv == null)
            {
                return;
            }

            string tagValue = controller.Refs[dgv][TagKeys.Grid] ?? "";
            var flags = tagValue.Split(',', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries).Select(x => x.ToLower()).ToArray();

            if (controller is IDataGridViewController dataGridViewController)
            {
                DataGridViewBuilder dgvb = new DataGridViewBuilder();

                dgvb.ReadOnly = flags.Contains("readonly");
                dgvb.Sizeable = flags.Contains("sizeable");
                dgvb.Multiselect = flags.Contains("multiselect");

                dataGridViewController.OnBuildDataGridView(dgvb);
                dgvb.Apply(dgv);

                if (flags.Contains("editform"))
                {
                    dgv.CellDoubleClick += HandleCellDoubleClickToEdit;
                    dgv.KeyDown += HandleCellF2ToEdit;
                }
            }
        }

        public static void ReadData<T>(T data, IFormController controller) where T : class
        {
            var type = typeof(T);
            foreach (var ctlField in controller.Find(x => x.ContainsKey(TagKeys.Input)))
            {
                if (ctlField.Tag is BindingContext context)
                {
                    var propName = context.Tags[TagKeys.Input];
                    var prop = type.GetProperty(propName);
                    if (prop != null && prop.CanWrite)
                    {
                        var value = GetValue(ctlField);
                        try
                        {
                            var convertedValue = Convert.ChangeType(value, prop.PropertyType);
                            prop.SetValue(data, convertedValue);
                        }
                        catch
                        {
                            if (Amaterasu.Conversions.AutoConvert.IsNumericType(prop.PropertyType)
                                && value is string str && string.IsNullOrWhiteSpace(str)
                                )
                            {
                                prop.SetValue(data, Convert.ChangeType(0, prop.PropertyType));
                            }
                            else
                                throw;
                        }
                    }
                }
            }
        }

        public static void FillData<T>(T data, IFormController controller) where T : class
        {
            var type = typeof(T);
            foreach (var ctlField in controller.Find(x => x.ContainsKey(TagKeys.Input)))
            {
                if (ctlField.Tag is BindingContext context)
                {
                    var propName = context.Tags[TagKeys.Input];
                    var prop = type.GetProperty(propName);
                    if (prop != null && prop.CanRead)
                    {
                        SetValue(ctlField, prop.GetValue(data));
                    }
                }
            }
        }

        public static void SetValue(Control control, object? value)
        {
            if (control is TextBox textBox)
            {
                textBox.Text = value?.ToString() ?? "";
            }
            if (control is DateTimePicker timePicker && value is DateTime dateTime)
            {
                timePicker.Value = dateTime;
            }
        }

        private static void HandleCellF2ToEdit(object? sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F2)
            {
                if (sender is DataGridView dgv && dgv.SelectedRows.Count == 1)
                {
                    HandleDataGridEdit(dgv);
                    e.Handled = true;
                }
            }
        }

        private static void HandleCellDoubleClickToEdit(object? sender, DataGridViewCellEventArgs e)
        {
            if (sender is DataGridView dgv && dgv.SelectedRows.Count == 1 && e.RowIndex == dgv.SelectedRows[0].Index)
            {
                HandleDataGridEdit(dgv);
            }
        }

        private static void HandleDataGridEdit(DataGridView dataGridView)
        {
            if (dataGridView.Tag is BindingContext context && context.Controller is IDataBrowser browser
                && dataGridView.SelectedRows.Count == 1)
            {
                var row = dataGridView.SelectedRows[0];
                if (row.Tag != null)
                {
                    browser.OnEditData(row.Tag);
                    PerformSearch(context.Controller);
                }
            }
        }

        private static void BtnCreate_Click(object? sender, EventArgs e)
        {
            if (sender is Control ctl && ctl.Tag is BindingContext context
                && context.Controller is IDataBrowser browser)
            {
                browser.OnNewData();
                PerformSearch(context.Controller);
            }
        }

        private static void BtnSearch_Click(object? sender, EventArgs e)
        {
            if (sender is Control ctl && ctl.Tag is BindingContext context)
            {
                PerformSearch(context.Controller);
            }
        }

        private static void BtnSave_Click(object? sender, EventArgs e)
        {
            if (sender is Control ctl && ctl.Tag is BindingContext context
                && context.Controller is IDataEditor editor)
            {
                SaveData(context.Controller, editor);
            }
        }

        public static void SaveData(IFormController controller, IDataEditor editor)
        {
            try
            {
                ClearValidationErrors(controller);
                if (!PerformValidation(controller)) return;
                editor.SaveData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to save data: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static void ClearValidationErrors(IFormController controller)
        {
            foreach (var ctl in controller.Find(x => x.Keys.Any(w => w.StartsWith(TagKeys.Validation))))
            {
                if (ctl.Tag is BindingContext context && context.ErrorProvider != null)
                {
                    context.ErrorProvider.SetError(ctl, string.Empty);
                }
            }
        }

        public static bool PerformValidation(IFormController controller)
        {
            var allValid = true;
            Control? firstInvalid = null;
            foreach (var ctl in controller.Find(x => x.Keys.Any(w => w.StartsWith(TagKeys.Validation))))
            {
                if (ctl.Tag is BindingContext context)
                {
                    var validations = context.Tags.Where(x => x.Key.StartsWith(TagKeys.Validation)).ToList();
                    foreach (var validation in validations)
                    {
                        bool hasInvalid = false;
                        switch (validation.Key)
                        {
                            case "v:required":
                                hasInvalid = !ValidateRequired(ctl, context);
                                allValid &= !hasInvalid;
                                break;
                            case "v:min":
                                hasInvalid = !ValidateMin(ctl, validation.Value, context);
                                allValid &= !hasInvalid;
                                break;
                            case "v:minlength":
                                hasInvalid = !ValidateMinLength(ctl, validation.Value, context);
                                allValid &= !hasInvalid;
                                break;
                        }
                        if (hasInvalid)
                        {
                            if (firstInvalid == null)
                                firstInvalid = ctl;
                            break;
                        }
                    }
                }
            }
            if (firstInvalid != null)
            {
                firstInvalid.Focus();
                if (firstInvalid is TextBoxBase tb)
                {
                    tb.SelectAll();
                }
            }
            return allValid;
        }

        private static bool ValidateRequired(Control ctl, BindingContext context)
        {
            if (ctl is ComboBox cb)
            {
                return cb.SelectedIndex >= 0;
            }
            var val = GetValue(ctl);
            var strVal = val?.ToString();
            if (string.IsNullOrWhiteSpace(strVal))
            {
                if (context.ErrorProvider != null)
                {
                    context.ErrorProvider.SetError(ctl, "Value is required");
                }
                return false;
            }
            return true;
        }

        private static bool ValidateMinLength(Control ctl, string param, BindingContext context)
        {
            var val = GetValue(ctl);
            var strVal = val?.ToString();

            int len;
            if (!int.TryParse(param, out len))
            {
                len = 1;
            }

            if (strVal != null && strVal.Trim().Length < len)
            {
                if (context.ErrorProvider != null)
                {
                    context.ErrorProvider.SetError(ctl, $"Text must be at least {len} characters");
                }
                return false;
            }
            return strVal != null;
        }

        private static bool ValidateMin(Control ctl, string param, BindingContext context)
        {
            var val = GetValue(ctl);
            var strVal = val?.ToString();
            if (strVal == null) return true;
            double d;
            if (double.TryParse(param, out d))
            {
                double v;
                if (double.TryParse(strVal, out v))
                {
                    if (d > v && context.ErrorProvider != null)
                    {
                        context.ErrorProvider.SetError(ctl, "Value must be greater than or equal to " + Math.Round(d, 2));
                    }
                    return d <= v;
                }
                else
                {
                    if (context.ErrorProvider != null)
                    {
                        context.ErrorProvider.SetError(ctl, "Value must be a valid number");
                    }
                    return false;
                }
            }
            return true;
        }

        private static bool PerformSearch(IFormController controller)
        {
            if (controller is IDataBrowser browser)
            {
                var filter = controller.BuildSearchFilter() ?? BuildSearchFilter(controller);
                var data = browser.OnSearch(filter);
                if (data != null && controller is IDataGridViewController dgvc)
                {
                    var dgv = controller.FindFirst<DataGridView>(x => x.ContainsKey(TagKeys.Grid));
                    if (dgv != null)
                    {
                        FillDataGridView(dgv, data);
                    }
                }
                return true;
            }
            return false;
        }

        private static void FillDataGridView(DataGridView dgv, IEnumerable data)
        {
            dgv.Rows.Clear();
            foreach (var row in data)
            {
                var dgvrow = dgv.Rows[dgv.Rows.Add()];
                dgvrow.Tag = row;
                foreach (var col in dgv.Columns.OfType<DataGridViewColumn>())
                {
                    if (col.Tag is IDataGridViewColumnBuilder dgvcb)
                    {
                        dgvrow.Cells[col.Index].Value = dgvcb.GetValue(row);
                    }
                }
            }
            dgv.ClearSelection();
            if (dgv.Rows.Count > 0)
                dgv.Rows[0].Selected = true;
        }

        private static DataFilter BuildSearchFilter(IFormController controller)
        {
            var res = new DataFilter();
            foreach (var ctl in controller.Find(x => x.ContainsKey(TagKeys.SearchFilter)))
            {
                if (ctl.Tag is BindingContext context)
                {
                    var val = GetValue(ctl);
                    var field = context.Tags[TagKeys.SearchFilter];
                    if (!string.IsNullOrWhiteSpace(field))
                    {
                        if (field == ActionVerbs.SearchText && val is string)
                        {
                            res.Text = val.ToString();
                        }
                        else
                        {
                            res.Where(field, val);
                        }
                    }
                }
            }

            return res;
        }
    }
}