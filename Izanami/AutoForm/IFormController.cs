﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public interface IFormController
    {
        ErrorProvider ErrorProvider { get; }

        ReadOnlyDictionary<Control, Tags> Refs { get; }

        IEnumerable<Control> Find(Func<Tags, bool> filter);

        IEnumerable<T> Find<T>(Func<Tags, bool> filter) where T : Control;

        Control? FindFirst(Func<Tags, bool> filter);

        T? FindFirst<T>(Func<Tags, bool> filter) where T : Control;

        DataFilter? BuildSearchFilter();
    }

    public interface IFormController<TForm> : IFormController where TForm: Form
    {
        void Bind(TForm form);
    }
}
