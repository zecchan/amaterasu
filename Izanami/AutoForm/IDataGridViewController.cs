﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.AutoForm
{
    public interface IDataGridViewController
    {
        void OnBuildDataGridView(DataGridViewBuilder configure);
    }
}
