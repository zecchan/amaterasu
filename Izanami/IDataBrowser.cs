﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami
{
    public interface IDataBrowser
    {
        IEnumerable OnSearch(DataFilter filter);

        void OnViewData(object data);

        void OnEditData(object data);

        void OnSelectData(object data);

        void OnNewData();
    }

    public interface IDataBrowser<TData>: IDataBrowser
    {
        void OnViewData(TData data);

        void OnEditData(TData data);

        void OnSelectData(TData data);
    }
}
