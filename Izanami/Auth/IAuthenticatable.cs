﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izanami.Auth
{
    public interface IAuthenticatable
    {
        bool Can(params string[] actions);

        bool VerifyPassword(string password);

        string Id { get; }

        string Name { get; }

        string Password { set; }
    }
}
