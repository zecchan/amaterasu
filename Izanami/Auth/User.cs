﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Amaterasu;
using Amaterasu.Cryptography;

namespace Izanami.Auth
{
    public class User : IAuthenticatable
    {
        public string Id { get; set; } = string.Empty;

        public string Name { get; set; } = string.Empty;

        string _password = string.Empty;
        public string Password { get => _password; set => _password = EncryptPassword(value); }

        public string Secret { get; set; } = RandomGenerator.String(64);

        public virtual bool Can(params string[] actions)
        {
            return false;
        }

        public virtual bool VerifyPassword(string password)
        {
            return Password == EncryptPassword(password);
        }

        protected virtual string EncryptPassword(string password)
        {
            var cvt = App.Current.Secret.UserSecretKey + Secret + password;
            return Hasher.ComputeHash(Encoding.UTF8.GetBytes(cvt), CommonHashAlgorithm.SHA256).Hash;
        }
    }
}
