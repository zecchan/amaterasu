﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace Izanami
{
    public class DataFilter
    {
        public string? Text { get; set; }

        public int Offset { get; set; } = 0;

        public int Limit { get; set; } = -1;

        public List<DataBrowserFilterParameter> Conditions { get; } = new List<DataBrowserFilterParameter>();

        public DataFilter Search(string text)
        {
            Text = text;
            return this;
        }

        public DataFilter Skip(int skip)
        {
            Offset = skip;
            return this;
        }

        public DataFilter Take(int take)
        {
            Limit = take;
            return this;
        }

        public DataFilter TakeAll()
        {
            Limit = -1;
            return this;
        }

        public DataFilter Where(string field, DataBrowserFilterOperator op, object? value = null)
        {
            Conditions.Add(new DataBrowserFilterParameter(field, op, value));
            return this;
        }

        public DataFilter Where(string field, object? value = null)
        {
            Conditions.Add(new DataBrowserFilterParameter(field, value));
            return this;
        }

        public static implicit operator DataFilter(string? text)
        {
            return new DataFilter() { Text = text };
        }
    }

    public class DataBrowserFilterParameter
    {
        public string Field { get; }

        public DataBrowserFilterOperator Operator { get; set; } = DataBrowserFilterOperator.Equal;

        public object? Value { get; set; }

        public DataBrowserFilterParameter(string field, DataBrowserFilterOperator @operator, object? value = null)
        {
            Field = field;
            Operator = @operator;
            Value = value;
        }

        public DataBrowserFilterParameter(string field, object? value = null)
        {
            Field = field;
            Value = value;
        }
    }

    public enum DataBrowserFilterOperator
    {
        Equal,
        NotEqual,

        LessThan,
        MoreThan,

        LessOrEqualThan,
        MoreOrEqualThan,
    }
}