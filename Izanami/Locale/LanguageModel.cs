﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Izanami.Locale
{
    public class LanguageModel
    {
        [JsonPropertyName("id")]
        public string Id { get; set; } = string.Empty;

        [JsonPropertyName("dictionaries")]
        public Dictionary<string, Dictionary<string, string>> Dictionaries { get; } = new Dictionary<string, Dictionary<string, string>>();
    }
}
