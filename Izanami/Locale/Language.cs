﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Izanami.Locale
{
    public class Language
    {
        public Language(string id)
        {
            Id = id;
        }

        public string Id { get; }

        public Dictionary<string, string> Dictionary { get; } = new Dictionary<string, string>();

        /// <summary>
        /// Normalize dictionary keys
        /// </summary>
        public void Normalize()
        {
            var keys = Dictionary.Keys.ToList();
            foreach (var key in keys)
            {
                var lcKey = key.Trim().ToLower();
                if (!Dictionary.ContainsKey(lcKey))
                {
                    var val = Dictionary[key];
                    Dictionary.Remove(key);
                    Dictionary.Add(lcKey, val);
                }
            }
        }

        /// <summary>
        /// Merge with a language model
        /// </summary>
        /// <param name="model"></param>
        public void MergeWith(LanguageModel model)
        {
            if (Id != model.Id)
                throw new Exception($"Cannot merge, language id does not match");
            foreach(var ns in model.Dictionaries)
            {
                MergeWith(ns.Value, ns.Key);
            }
        }

        /// <summary>
        /// Merge with other language object
        /// </summary>
        public void MergeWith(Language other, string? parentNamespace = null, bool overwrite = true)
        {
            if (Id != other.Id)
                throw new Exception($"Cannot merge, language id does not match");
            MergeWith(other.Dictionary, parentNamespace, overwrite);
        }

        /// <summary>
        /// Merge with other dictionary
        /// </summary>
        public void MergeWith(Dictionary<string, string> language, string? parentNamespace = null, bool overwrite = true)
        {
            foreach (var kv in language)
            {
                var key = kv.Key.Trim().ToLower();
                if (!string.IsNullOrWhiteSpace(parentNamespace))
                {
                    key = parentNamespace.Trim().ToLower() + "." + key;
                }
                if (!overwrite && Dictionary.ContainsKey(key)) continue;
                Dictionary[key] = kv.Value;
            }
            Normalize();
        }

        /// <summary>
        /// Translate a string
        /// </summary>
        public string? Translate(string text, params object[] parameters)
        {
            if (Dictionary.ContainsKey(text))
            {
                var translated = Dictionary[text];
                return string.Format(translated, parameters);
            }
            return null;
        }

        /// <summary>
        /// Translate a string
        /// </summary>
        public string? Translate(string text, Dictionary<string, string> values)
        {
            if (Dictionary.ContainsKey(text))
            {
                var translated = Dictionary[text];
                var sortedValues = values.OrderBy(x => x.Key).Reverse().ToList();
                foreach (var val in sortedValues)
                {
                    translated = translated.Replace("{" + val.Key + "}", Translate(val.Value) ?? val.Value);
                }
                return translated;
            }
            return null;
        }
    }
}
