﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Izanami
{
    /// <summary>
    /// Provides a way to parse string to tags
    /// </summary>
    public class Tags : Dictionary<string, string>
    {
        public Tags()
        {
        }

        public Tags(string tags)
        {
            var splKV = tags.Split(';', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            foreach (var kv in splKV)
            {
                var spl = kv.Split(new[] { '=' }, 2, StringSplitOptions.TrimEntries);
                if (!string.IsNullOrWhiteSpace(spl[0]))
                {
                    if (spl.Length == 1)
                    {
                        this[spl[0]] = "true";
                    }
                    else if (spl.Length == 2)
                    {
                        this[spl[0]] = spl[1];
                    }
                }
            }
        }

        public bool Has(string key, string value)
        {
            if (!ContainsKey(key)) return false;
            return this[key] == value;
        }

        public T? ValueAs<T>(string key)
        {
            if (!ContainsKey(key)) return default;

            var value = this[key];
            if (typeof(T) == typeof(string))
            {
                return JsonSerializer.Deserialize<T>(JsonSerializer.Serialize(value));
            }
            return JsonSerializer.Deserialize<T>(value);
        }

        public static Tags Parse(string tags)
        {
            return new Tags(tags);
        }
    }
}
