﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Xml.Linq;

namespace Izanami
{
    public interface IDataEditor
    {
        void LoadData(object data, bool readOnly = false);

        void SaveData(object? data = default);

        void DeleteData(object data);

        void OnNewData();
    }

    public interface IDataEditor<TData>: IDataEditor
    {
        void LoadData(TData data);

        void SaveData(TData? data = default);

        void DeleteData(TData data);
    }
}
