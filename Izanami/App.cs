﻿using Microsoft.Extensions.Configuration;

namespace Izanami
{
    public class App
    {
        /// <summary>
        /// Gets the current application
        /// </summary>
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        public static IApplication Current { get; private set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

        /// <summary>
        /// Run the application
        /// </summary>
        public static void Run(IApplication application)
        {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddEnvironmentVariables();
            configurationBuilder.AddJsonFile("config.json", true);
            configurationBuilder.AddJsonFile(application.Id + ".config.json", true);
            var config = configurationBuilder.Build();

            var bootstrapper = new ApplicationBootstrapper();
            application.Initialize(config, bootstrapper);
            Current = application;
        }
    }
}