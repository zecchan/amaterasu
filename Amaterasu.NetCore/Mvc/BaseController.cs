﻿using System;
using System.Collections.Generic;
using System.Text;
using Amaterasu.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Amaterasu.Mvc
{
    public abstract class BaseController : Controller
    {
        public IDatabaseManager DB { get; }
        public IConfiguration Configuration { get; }

        public BaseController(IDatabaseManager db, IConfiguration config)
        {
            DB = db;
            Configuration = config;
        }
    }

    public abstract class ModelBaseController<TModel> : BaseController where TModel : class, new()
    {
        protected abstract string DBContextKey { get; }
        protected IDatabaseContext DBC { get => string.IsNullOrWhiteSpace(DBContextKey) ? DB.Default : DB.Databases[DBContextKey]; }

        protected ModelBaseController(IDatabaseManager db, IConfiguration config) : base(db, config)
        {
        }

        [HttpGet]
        public virtual IActionResult GetAll([FromQuery]int? take, [FromQuery] int? skip, [FromQuery] int? page, [FromQuery] string search)
        {
            try
            {
                var fil = new ModelFilterBase()
                {
                    Take = take,
                    Skip = skip,
                    Page = page,
                    Search = search
                };
                var list = DBC.From<TModel>();
                var data = fil.FinalizeQueryBuilder(list).ToList();
                return new ApiDatasetResultWithTotal<TModel>()
                {
                    Data = data,
                    StatusCode = 200,
                    Total = data.Count
                };
            }
            catch {
                return ApiResult.Fail(HttpStatusCode.InternalServerError, "An error has occured when getting the data.");
            }
        }

        [HttpGet("{id}")]
        public virtual IActionResult Get([FromRoute] string id)
        {
            return ApiResult.Fail(HttpStatusCode.NotImplemented, "This feature is not implemented");
        }

        [HttpPost]
        public virtual IActionResult Post([FromBody] TModel data)
        {
            try
            {
                DBC.Save(data);
                return ApiResult.Success(data, "Data saved successfully.");
            }
            catch(Exception ex)
            {
                return ApiResult.Fail(HttpStatusCode.InternalServerError, "An error has occured when saving the data: " + ex.Message);
            }
        }

        [HttpPut]
        public virtual IActionResult Put([FromBody] TModel data)
        {
            try
            {
                DBC.Insert(data);
                return ApiResult.Success(data, "Data inserted successfully.");
            }
            catch(Exception ex)
            {
                return ApiResult.Fail(HttpStatusCode.InternalServerError, "An error has occured when saving the data: " + ex.Message);
            }
        }

        [HttpPatch("{id}")]
        public virtual IActionResult Patch([FromQuery] string id, [FromBody] TModel data)
        {
            return ApiResult.Fail(HttpStatusCode.NotImplemented, "This feature is not implemented");
        }

        [HttpDelete]
        public virtual IActionResult Delete([FromBody] TModel data)
        {
            try
            {
                DBC.Delete(data);
                return ApiResult.Success("Data deleted successfully.");
            }
            catch(Exception ex)
            {
                return ApiResult.Fail(HttpStatusCode.InternalServerError, "An error has occured when deleting the data: " + ex.Message);
            }
        }
    }

    public class ModelFilterBase
    {
        /// <summary>
        /// Gets how many data to take, if this is unset then all data will be taken.
        /// </summary>
        public int? Take { get; set; }
        /// <summary>
        /// Gets what page should be shown, take must be specified.
        /// </summary>
        public int? Page { get; set; }
        /// <summary>
        /// Gets how many data should be skipped, take must be specified. Page starts from 1.
        /// </summary>
        public int? Skip { get; set; }

        public IQueryBuilder<T> FinalizeQueryBuilder<T>(IQueryBuilder<T> q)
        {
            if (Take != null && Take > 0)
            {
                var take = Take.Value;
                var skip = 0;
                if (Skip != null && Skip >= 0)
                {
                    skip = Skip.Value;
                }
                else if (Page != null && Page > 0)
                {
                    skip = (Page.Value - 1) * take;
                }

                return q.Skip(skip).Take(take);
            }
            return q;
        }

        /// <summary>
        /// Gets the simple search keyword
        /// </summary>
        public string Search { get; set; }
    }
}
