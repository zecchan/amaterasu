﻿using Amaterasu.Database;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Amaterasu.NetCore.Mvc.BLL
{
    public abstract class BLLAbstractBase<TDB>
    {
        /// <summary>
        /// Gets the database interface
        /// </summary>
        protected TDB DB { get; }

        /// <summary>
        /// Gets the application configuration
        /// </summary>
        protected IConfiguration Config { get; }

        public BLLAbstractBase(TDB db, IConfiguration config = null)
        {
            DB = db;
            Config = config;
        }
    }

    public abstract class BLLBase : BLLAbstractBase<IDatabaseManager> { 
        public BLLBase(IDatabaseManager db, IConfiguration config = null) : base(db, config)
        {

        }
    }

}
