﻿using Amaterasu.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Amaterasu.NetCore.Mvc.BLL
{
    /// <summary>
    /// Basic BLLResult class
    /// </summary>
    public class BLLResult
    {
        /// <summary>
        /// Gets whether the result is a success or not. This will be set to true only when StatusCode is OK.
        /// </summary>
        public bool IsSuccess { get => StatusCode == HttpStatusCode.OK; }

        /// <summary>
        /// Gets the status code of the result.
        /// </summary>
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// Gets the message of the result. Might be null.
        /// </summary>
        public string Message { get; private set; }

        public ExceptionLog ExceptionLog { get; private set; }

        /// <summary>
        /// Create a success result
        /// </summary>
        /// <param name="message">The message</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Success(string message = null)
        {
            return new BLLResult()
            {
                Message = message,
                StatusCode = HttpStatusCode.OK
            };
        }

        /// <summary>
        /// Create a success result
        /// </summary>
        /// <param name="data">The data to attach</param>
        /// <param name="message">The message</param>
        /// <returns>BLLResult</returns>
        public static BLLResult<T> Success<T>(T data, string message = null)
        {
            return new BLLResult<T>()
            {
                Message = message,
                Data = data,
                StatusCode = HttpStatusCode.OK
            };
        }

        /// <summary>
        /// Create a fail result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Fail(string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new BLLResult()
            {
                Message = message,
                StatusCode = statusCode
            };
        }

        /// <summary>
        /// Create a fail result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <param name="ex">Exception to be logged</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Fail(Exception ex, string message = null, HttpStatusCode statusCode = HttpStatusCode.InternalServerError)
        {
            return new BLLResult()
            {
                Message = message ?? ex?.Message ?? statusCode.ToString(),
                StatusCode = statusCode,
                ExceptionLog = ex != null ? new ExceptionLog(ex) : null
            };
        }

        /// <summary>
        /// Create a fail result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <param name="data">The data to attach</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Fail<T>(T data, string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new BLLResult<T>()
            {
                Message = message,
                Data = data,
                StatusCode = statusCode
            };
        }

        /// <summary>
        /// Create a fail result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <param name="data">The data to attach</param>
        /// <param name="ex">Exception to be logged</param>
        /// <returns>BLLResult</returns>
        public static BLLResult<T> Fail<T>(Exception ex, T data = default(T), string message = null, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new BLLResult<T>()
            {
                Message = message ?? ex?.Message ?? statusCode.ToString(),
                Data = data,
                StatusCode = statusCode,
                ExceptionLog = ex != null ? new ExceptionLog(ex) : null
            };
        }

        /// <summary>
        /// Create a custom result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Result(HttpStatusCode statusCode, string message = null)
        {
            return new BLLResult()
            {
                StatusCode = statusCode,
                Message = message
            };
        }

        /// <summary>
        /// Create a custom result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <param name="data">The data to attach</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Result<T>(HttpStatusCode statusCode, T data, string message = null)
        {
            return new BLLResult<T>()
            {
                StatusCode = statusCode,
                Message = message,
                Data = data
            };
        }

        /// <summary>
        /// Create a custom result
        /// </summary>
        /// <param name="statusCode">The status code of the result</param>
        /// <param name="message">The message</param>
        /// <param name="data">The data to attach</param>
        /// <param name="ex">Exception to be logged</param>
        /// <returns>BLLResult</returns>
        public static BLLResult Result<T>(HttpStatusCode statusCode, T data, Exception ex, string message = null)
        {
            return new BLLResult<T>()
            {
                StatusCode = statusCode,
                Message = message ?? ex?.Message ?? statusCode.ToString(),
                Data = data,
                ExceptionLog = ex != null ? new ExceptionLog(ex) : null
            };
        }

        /// <summary>
        /// Converts to ApiResult
        /// </summary>
        /// <returns>ApiResult</returns>
        public virtual ApiResult ToApiResult()
        {
            return IsSuccess ? ApiResult.Success(Message) : ApiResult.Fail(StatusCode, Message);
        }
    }

    /// <summary>
    /// BLL Result with data
    /// </summary>
    /// <typeparam name="T">Data type</typeparam>
    public class BLLResult<T> : BLLResult
    {
        /// <summary>
        /// Gets the attached data
        /// </summary>
        public T Data { get; internal set; }

        public override ApiResult ToApiResult()
        {
            return IsSuccess ? ApiResult.Success(Data, Message) : ApiResult.Fail(StatusCode, Data, Message);
        }
    }

    public class BLLDatasetResult<T>: BLLResult
    {
        /// <summary>
        /// Gets the attached data
        /// </summary>
        public IEnumerable<T> Data { get; internal set; }

        /// <summary>
        /// Gets the total number of the current dataset
        /// </summary>
        public int? Total { get; internal set; }

        public override ApiResult ToApiResult()
        {
            if (Total != null)
            return new ApiDatasetResultWithTotal<T>()
            {
                Data = Data,
                Total = Total.Value,
                StatusCode = (int)StatusCode
            };
            return new ApiDatasetResult<T>()
            {
                Data = Data,
                StatusCode = (int)StatusCode
            };
        }
    }

    /// <summary>
    /// A class to convert exception into a simple class
    /// </summary>
    public class ExceptionLog
    {
        /// <summary>
        /// Gets the exception message
        /// </summary>
        public string Message { get; }

        /// <summary>
        /// Gets the stack trace
        /// </summary>
        public string StackTrace { get; }

        /// <summary>
        /// Gets the inner exception. Might be null.
        /// </summary>
        public ExceptionLog InnerException { get; }

        public ExceptionLog(string message, string stackTrace = null, ExceptionLog innerException = null)
        {
            Message = message;
            StackTrace = stackTrace;
            InnerException = innerException;
        }

        public ExceptionLog(Exception ex)
        {
            Message = ex.Message;
            StackTrace = ex.StackTrace;
            InnerException = ex.InnerException != null ? new ExceptionLog(ex.InnerException) : null;
        }
    }
}
