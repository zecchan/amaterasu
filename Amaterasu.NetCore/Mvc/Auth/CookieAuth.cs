﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Amaterasu.NetCore.Mvc.Auth
{
    /// <summary>
    /// Cookie based auth. It will pass the value of the cookie "token" to IUserIdentityProvider.
    /// </summary>
    public static class CookieAuth
    {
        public const string AuthenticationScheme = "Cookie";
        public const string AuthenticationType = "Web";
    }

    public class CookieAuthenticationHandler : AuthenticationHandler<CookieAuthenticationHandlerOptions>
    {
        protected IConfiguration Config { get; set; }

        public CookieAuthenticationHandler(IOptionsMonitor<CookieAuthenticationHandlerOptions> options,
            ILoggerFactory logger,
               UrlEncoder encoder,
               ISystemClock clock,
               IConfiguration configuration) : base(options, logger, encoder, clock)
        {
            Config = configuration;
        }


        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Cookies.ContainsKey("token"))
            {
                return AuthenticateResult.Fail("Missing Token");
            }

            var token = Request.Cookies["token"];
            try
            {
                var claims = Options.UserIdentityProvider.GetClaimsFromCookie(token, Context);
                if (claims == null)
                    return AuthenticateResult.Fail("Invalid Token");
                var identity = new ClaimsIdentity(claims, CookieAuth.AuthenticationType);
                var principal = new ClaimsPrincipal(identity);
                var ticket = new AuthenticationTicket(principal, CookieAuth.AuthenticationScheme);

                return AuthenticateResult.Success(ticket);
            }
            catch
            {
                return AuthenticateResult.Fail("Unauthorized");
            }
        }

        protected override async Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            var authResult = await HandleAuthenticateOnceSafeAsync();

            if (!authResult.Succeeded)
            {
                Response.StatusCode = 302;
                Response.Headers.Add(HeaderNames.Location, new Microsoft.Extensions.Primitives.StringValues(Options.LoginPage ?? "/web/auth"));
                Response.Cookies.Delete("token");
            }
        }
    }
    public class CookieAuthenticationHandlerOptions : AuthenticationSchemeOptions
    {
        public IUserIdentityProvider UserIdentityProvider { get; set; }

        public string LoginPage { get; set; }
    }
}
