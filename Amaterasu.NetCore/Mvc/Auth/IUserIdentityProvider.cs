﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace Amaterasu.NetCore.Mvc.Auth
{
    public interface IUserIdentityProvider
    {
        public IEnumerable<Claim> GetClaimsFromCookie(string token, HttpContext context);

        public IEnumerable<Claim> GetClaimsFromBasic(string username, string password);
    }
}
