﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Amaterasu.Mvc
{
    public class ApiResult : IActionResult
    {
        public int StatusCode { get; set; } = 200;

        public virtual Task ExecuteResultAsync(ActionContext context)
        {
            var js = new JsonResult(this);
            js.StatusCode = StatusCode;
            return js.ExecuteResultAsync(context);
        }

        public static ApiDatasetResult<T> Dataset<T>(IEnumerable<T> data, int? total = null)
        {
            return total == null ? new ApiDatasetResult<T> { Data = data } : new ApiDatasetResultWithTotal<T> { Data = data, Total = total.Value };
        }

        public static ApiResult Success()
        {
            return new ApiResult();
        }

        public static ApiResultWithMessage Success(string message)
        {
            return new ApiResultWithMessage() { Message = message };
        }

        public static ApiResultWithData Success(object data, string message = null)
        {
            return new ApiResultWithData() { Message = message, Data = data };
        }

        public static ApiResultWithMessage Fail(HttpStatusCode statusCode, string message = null)
        {
            return new ApiResultWithMessage() { StatusCode = (int)statusCode, Message = message };
        }

        public static ApiResultWithData Fail(HttpStatusCode statusCode, object data, string message = null)
        {
            return new ApiResultWithData() { StatusCode = (int)statusCode, Message = message, Data = data };
        }

        public static ApiResultWithData Fail(HttpStatusCode statusCode, Exception ex, string message = null, bool includeStackTrace = false)
        {
            return new ApiResultWithData() { StatusCode = (int)statusCode, Message = message ?? "A fatal error has occured", Data = new ApiExceptionData(ex, includeStackTrace) };
        }
    }

    public class ApiResultWithMessage: ApiResult
    {
        public string Message { get; set; }
    }
    public class ApiResultWithData : ApiResultWithMessage
    {
        public object Data { get; set; }
    }

    public class ApiDatasetResult<T>: ApiResult
    {
        public IEnumerable<T> Data { get; set; }
    }

    public class ApiDatasetResultWithTotal<T>: ApiDatasetResult<T>
    {
        public int Total { get; set; }
    }

    public class ApiExceptionData
    {
        public string Message { get; set; }

        public string StackTrace { get; set; }

        public ApiExceptionData InnerException { get; set; }

        public ApiExceptionData(Exception ex, bool keepStackTrace = false)
        {
            Message = ex.Message;
            if (keepStackTrace)
                StackTrace = ex.StackTrace;
            if (ex.InnerException != null)
                InnerException = new ApiExceptionData(ex.InnerException, keepStackTrace);
        }
    }
}
