﻿using Amaterasu.Database;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Amaterasu.Extensions
{
    public static class DatabaseManagerExtension
    {
        public static IServiceCollection AddDatabaseManager(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddSingleton(typeof(IDatabaseManager), typeof(WebDatabaseManager));

            return services;
        }
    }

    public class WebDatabaseManager : DatabaseManager
    {
        public WebDatabaseManager(IConfiguration config)
        {
            var dct = typeof(IDatabaseContext);
            var dcs = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes().Where(t => dct.IsAssignableFrom(t) && !t.IsAbstract && t.IsClass));
            var dic = new Dictionary<string, Type>();
            foreach (var d in dcs)
            {
                var pname = d.Name.EndsWith("DatabaseContext") ? d.Name[0..^15] : d.Name;
                dic[pname.ToLower()] = d;
            }

            var dbsection = config.GetSection("Databases");
            if (dbsection != null)
            {
                foreach (var db in dbsection.GetChildren())
                {
                    var eng = db["Engine"];
                    if (!dic.ContainsKey(eng?.ToLower())) throw new Exception("The database engine '" + eng + "' is not found");
                    var dbi = Activator.CreateInstance(dic[eng.ToLower()]) as IDatabaseContext;
                    dbi.Credential.Database = db["Name"] ?? db["Database"];
                    dbi.Credential.Host = db["Host"] ?? db["DataSource"];
                    dbi.Credential.Password = db["Password"];
                    dbi.Credential.User = db["User"] ?? db["Username"];
                    dbi.Credential.ConnectionString = db["ConnectionString"];
                    if (dbi != null)
                    {
                        dbi.Open();
                        this.Add(db.Key, dbi);
                    }
                }
            }
        }
    }
}
